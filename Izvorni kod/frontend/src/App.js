import './App.css';
import React, { useEffect, useState } from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Header from './components/Header';
import Calendar from './components/Calendar';
import Competition from './components/Competition';
import CompetitionInfo from './components/CompetitionInfo';
import CompetitionResults from './components/CompetitionResults';
import CompetitionDataEdit from './components/CompetitionDataEdit';
import NewCompetition from "./components/NewCompetition";
import NewTask from "./components/NewTask";
import Task from './components/Task';
import TaskDataEdit from './components/TaskDataEdit';
import TaskResult from './components/TaskResult';
import Tasks from './components/Tasks';
import Profile from './components/Profile';
import Footer from './components/Footer';
import Login from './components/Login'
import Register from "./components/Register";
import HomePage from "./components/HomePage";
import Admin from "./components/Admin";
import Users from "./components/Users";
import DataEdit from "./components/DataEdit";
import easyFetch from './components/util/fetchUtility';
import backend from './components/util/constants';
import NewRandomVirtual from './components/NewRandomVirtual';
import TaskPractice from './components/TaskPractice';
import CompetitionResultsVirtual from './components/CompetitionResultsVirtual';

function App() {

  const[username, setUsername] = useState("");
  const[user, setUser] = useState({});

  //Prilikom promjene username, provjeri jel vec postoji username u ovom seasionu i ako postoji njega postavi u username
  //inace spremi username u seasionStorage, u kombinaciji sa useEffectom ispod bi trebao osigurati azurnost podataka o useru
  useEffect(() => {
    if((sessionStorage.getItem("username") === "" && username !== "")
        || (sessionStorage.getItem("username") !== "" && username !== "")
        || sessionStorage.getItem("username") === null){
      sessionStorage.setItem("username", username);
    } else {
      setUsername(sessionStorage.getItem("username"));
    }
  }, [username]);

  //Prilikom promjene username, dohvati sve podatke o tom useru i spremi ih u variablu "user"
  useEffect(() => {
    if(username !== "")
      easyFetch(`${backend}/userinfo/${username}/v2`)
        .then(res => {
          setUser(res);
      });
    else {
      setUser({});
    }
  }, [username]);

  //console.log("tu se ispise user "+user.appUser.username);

  return (
    <BrowserRouter>

      <Header user = {user} setUsername={setUsername}/>

      <div className="App">
        <Routes>
          <Route exact path='/' element={<HomePage/>}/>
          <Route path='/admin'  exact element={<Admin/>}/>
          <Route path='/kalendar'  exact element={<Calendar/>}/>
          <Route path='/profil' exact element={<Profile user = {user}/>}/>
          <Route path='/profil/:id' exact element={<Profile/>}/>
          <Route path='/zadaci'  element={<Tasks user= {user}/>}/>
          <Route path='/zadaci/:id' exact element={<TaskPractice/>}/>
          <Route path='/natjecanje/:competition_id' exact element={<CompetitionInfo user  = {user}/>}/>
          <Route path='/natjecanje/:competition_id/info' exact element={<CompetitionResults/>}/>
          <Route path='/natjecanje/:competition_id/virtualInfo' exact element={<CompetitionResultsVirtual/>}/>
          <Route path='/natjecanje/:competition_id/zadaci/:task_no' exact element={<Competition user = {user}/>}/>
          <Route path='/natjecanje/:competition_id/zadaci/:task_no/info' exact element={<TaskResult/>}/>
          <Route path='/novoNatjecanje/:username' exact element={<NewCompetition user ={user} />}/>
          <Route path='/novoVirtNatjecanje' exact element={<NewRandomVirtual/>}/>
          <Route path='/noviZadatak/:username' exact element={<NewTask/>}/>
          <Route path='/korisnici' exact element={<Users user = {user}/>}/>
          <Route path='/login' exact element={<Login setUsername = {setUsername} user = {user}/>}/>
          <Route path='/registration' exact element={<Register user = {user}/>}/>
          <Route path='/uredivanjePodataka/:username'  exact element={<DataEdit user = {user}/>}/>
          <Route path='/uredivanjeZadatka/:id' exact element={<TaskDataEdit/>}/>
          <Route path='/uredivanjeNatjecanja/:id' exact element={<CompetitionDataEdit user = {user}/>}/>
        </Routes>
      </div>

      <Footer/>

    </BrowserRouter>
  );
}

export default App;
