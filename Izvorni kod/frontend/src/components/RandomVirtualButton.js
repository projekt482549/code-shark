import React, { useEffect} from "react";
import { useNavigate } from "react-router";

function RandomVirtualButton() {
    const navigate = useNavigate()
    
    useEffect(() => {
        if(!sessionStorage.getItem("username")){
            navigate("/login");
        }
    }, []);

    function handleClick(e) {
        e.preventDefault();
        return (
            navigate(`/novoVirtNatjecanje`)
        )
    }

    return(
        <div>
            <button className = "virtualCompetitionButton" onClick = {handleClick}>Virtualno natjecanje</button>
        </div>
    );
}

export default RandomVirtualButton;