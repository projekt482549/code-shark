import React, { useEffect, useState } from "react"
import {useParams, useNavigate} from 'react-router-dom';
import Task from './Task';
import Timer from './Timer';
import TasksNavigation from './TasksNavigation';
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import upload from './resursi/upload.png';
import "./css/competition.css";

function Competition(props) {

    const user = props.user;
    const {competition_id, task_no} = useParams();
    const [taskData, setTaskData] = useState({});
    const [competitionData, setCompetitionData] = useState({});
    const [file, setFile] = useState();
    const navigate = useNavigate()
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    // exam/:examid/task/:taskno vraca: taskid, title, description, mscore, datestart, duration, totaltasks (nije stupac)
    // taskid zadatka se odredi iz inexam tablice, trebat ce mi za post
    // totaltasks je broj zadataka za natjecanje examid - treba mi za tasksnavigation
    // sve ostalo saljem u task komponentu
    useEffect(() => {
        easyFetch(`${backend}/exam/${competition_id}/task/${task_no}`, "GET", null, headers)
            .then(res => {
                if(res.status === 400) {
                    console.log("error")
                }
                if(res.datestart === undefined){
                  navigate(`/natjecanje/${competition_id}/zadaci/${task_no}/info`)
                }
                setTaskData(res);
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);

    useEffect(() => {
      easyFetch(`${backend}/exam/${competition_id}`, "GET", null, headers)
          .then(res => {
              if(res.status === 400) {
                  console.log("error")
              }
              setCompetitionData(res);
          })
          .catch(err => {console.log("No comunication with server")});
  }, []);

    function readFile(e){
      let input = e.target.files;
      let reader = new FileReader();

      reader.readAsArrayBuffer(input[0]);
      reader.onload = (e) => {
          let bytes = new Uint8Array(e.target.result);
          setFile(bytes);
      }
    }

    function handleClick(e) {
      e.preventDefault();
      if(competitionData.virtualFlag === null) return;
      var url = competitionData.virtualFlag ? `/natjecanje/${competition_id}/virtualInfo` : `/natjecanje/${competition_id}`;
      return (
        navigate(url)
      )
    }

    function onSubmit(e) {
      e.preventDefault();
      const data = {
        'file': file,
      }
      const headers = {
        'Content-Type': 'application/json'
      }
      var data2 = JSON.stringify(data, function(k, v) {
        if (v instanceof  Uint8Array) {
          return JSON.stringify(Array.apply([], v));
        }
        return v;
      });

      return (
        easyFetch(`${backend}/exam/${competition_id}/task/${task_no}`, "POST", data2, headers)
            .then(res => {
                if(res.status === 401) {
                    
                } 
            })
            .catch(err => {console.log("No comunication with server")})
      );
    }

    return(
        <div>

          <div className="competitionHeader">
            <TasksNavigation competition_id = {competition_id} totalTasks = {taskData.totaltasks} />
            <Timer datestart = {taskData.datestart} duration = {taskData.duration}/>
          </div>
          
          <div className = "submitResultsButtonContainer">
            <button className = "submitResultsButton" onClick = {handleClick}>Završi natjecanje</button>
          </div>
          
          <Task title = {taskData.title} description = {taskData.description} mscore = {taskData.mscore} /> 
          
          <div className="upload">
              <form className="taskUploadForm" onSubmit={onSubmit}>
                <div class = "uploadImageContainer">
                  <label for="file-input">
                    <img className = "uploadImage" alt = "" src = {upload}/>
                  </label>
                  <input id = "file-input" type="file" name="file" onChange={readFile} accept=".java" />
                </div>

                <input className="uploadButton" type="submit" value="Predaj" />
              </form>
          </div>
    
        </div>
    );
}

export default Competition;