import React, { useEffect } from "react";
import { useNavigate } from "react-router";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import userIcon from "./resursi/user-solid.svg";
import "./css/users.css";
import "./css/Register.css";

function Users(props) {
    const [usersData, setUsersData] = React.useState([]);
    const navigate = useNavigate();
    const accType = props.user.accounttype;

    useEffect(() => {
        easyFetch(`${backend}/userinfo`, "GET")
            .then(res => {
                if(res.status === 401) {
                    console.log("error")
                }
                setUsersData(res);
                //console.log(res);
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);

    useEffect(() => {
        if(!sessionStorage.getItem("username")){
            navigate("/login");
        }
    }, []);

    function ispisiUlogu(accounttype){
        switch (accounttype){
            case 0:
                return "natjecatelj"
            case 1:
                return "voditelj"
            case 2:
                return "administrator"
            case 3:
                return "nepotvrđeni voditelj"
            default:
                return "greška"
        }
    }


    function dodajGumbe(user){
        if (accType === 2){
            return (<div className="buttons">
            <a href={"/profil/" +  user.username }><input type="button" className="register-btn" value="Posjeti profil" ></input></a>
            <a href={"/uredivanjePodataka/" + user.username}><input type="button" className="register-btn button" value="Uredi podatke" ></input></a>
            </div>
            );
        }else {
            return (
            <a href={"/profil/" +  user.username }><input type="button" className="register-btn button" value="Posjeti profil" ></input></a>);
        }
    }


    return(
        <div className = "UsersContainer">
           {usersData.filter((user) => { 
                    return (user.accounttype !== 2 && user.enabled && accType !== 2) || (user.enabled && accType === 2); 
                }).map(user =>{
                    return (
                    <div className = "UserPart" key={user.userId}>
                        <img src={(user && (user.photo && `data:image/jpeg;base64,${user.photo}`)) || userIcon} alt="" className="UserPhoto" />
                        <span className = "UserData">
                        <span> <b>Korisničko ime : </b>{user.username}</span>
                        <span> <b> Ime : </b>{user.name} </span>
                        <span> <b> Prezime: </b>{user.surname}</span>
                        <span> <b> Uloga : </b>{ispisiUlogu(user.accounttype)}</span>
                        </span>
                        {dodajGumbe(user)}
                    </div>
                )
           })}
        </div>
    )
}

export default Users;