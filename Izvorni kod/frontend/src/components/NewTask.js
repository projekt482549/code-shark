import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/newTask.css";


function NewTask(props) {
    const navigate = useNavigate();
    const {username} = useParams();

    const [form, setForm] = useState({nameTask: '', textTask: '', points: 0, in: '', out:'', threadshold: '', private: 0});
    const [error, setError] = useState('');
    const [points, setPoints] = useState(1);

    const listIn = [];
    const listOut = [];


    function onChange(event) {
        const {name, value} = event.target;
        setForm(oldForm => ({...oldForm, [name]: value}));
    }

    function onChangeCheckBox(e) {
        if(e.target.checked) {
            setForm(oldForm => ({...oldForm, private: 1}));
        } else {
            setForm(oldForm => ({...oldForm, private: 0}));
        }
    }

    function onChangePoints(e) {
        var value = e.target.value;
        setPoints(value);
        setForm(oldForm => ({...oldForm, points: value}));
    }

    function onChangeIn(e, i) {
        listIn[i] = e.target.value;
    }

    function onChangeOut(e, i) {
        listOut[i] = e.target.value;
    }

    function onSubmit(e) {
        e.preventDefault();
        setError("");

        var listExamples = [];
        for(var i = 0; i < form.points; i++) {
            listExamples.push({
                ulaz: listIn[i],
                izlaz: listOut[i]
            });
        }


        const data = {
            'nameTask': form.nameTask,
            'textTask': form.textTask,
            'points': form.points,
            'threshold': form.threadshold,
            'privatni': form.private,
            'primjeri': listExamples,
        }


        const headers = {
            'Content-Type': 'application/json'
        }

        return(
            easyFetch(`${backend}/task`, "POST", JSON.stringify(data), headers)
                .then(res => {
                  if(res.message=== "Forbidden"){
                      alert("Nemate ovlasti za ovu radnju");
                  } else{
                      alert("Uspješno napravljen zadatak")
                      window.location.reload(false)
                  }
                })
                .catch(err => {console.log("No comunication with server")})
        );
    }

    return(
        <div className="container-task">
            <h2>Novi zadatak</h2>
            <form onSubmit={onSubmit}>
                    <div>
                        <label htmlFor="nameTask"/>
                        <input type="text" id="nameTask"placeholder="Naziv zadatka" name="nameTask" onChange={onChange} required/>

                        <label htmlFor="textTask"/>
                        <textarea id="textTask" placeholder="Tekst zadatka" name="textTask" onChange={onChange} required rows={5}/>

                        <div className="left row">
                            <div className="row-item">
                                <label>Broj bodova: </label>
                                <input type="number" name="points" min={1}  onChange={onChangePoints} required/>
                            </div>
                            <div className="row-item">
                                <label>Vrijeme izvođenja (ms): </label>
                                <input type="number" name="threadshold" min={0} onChange={onChange} required step="any"/>
                            </div>
                            <div className="row-item">
                                <label><input type="checkbox" name="private" onChange={onChangeCheckBox} />Privatan zadatak</label>
                            </div>

                        </div>
                    </div>
                    <div className="left">
                        <div>
                            <label>Primjeri za evaluaciju: </label>
                        </div>
                        {Array.from(Array(Math.round(Number(points))), (e, i ) => {
                            return <div className="left">
                                <div>
                                    <label>Primjer {i+1}.</label>
                                </div>
                                <div>
                                    <textarea id="in" placeholder="Ulaz" name="in"
                                            onChange={(e) => {onChangeIn(e, i);}}
                                            required rows={5}/>
                                    <textarea id="out" placeholder="Izlaz" name="out"
                                            onChange={(e) => {onChangeOut(e, i);}}
                                            required rows={5}/>
                                    </div>
                            </div>
                        })}

                    </div>

                <div>
                    <input className="submit-btn" id="submit" type="submit"  value="Potvrdi" onSubmit={onSubmit}/>
                </div>
            </form>
        </div>
    );
}

export default NewTask;