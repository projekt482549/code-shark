import React from "react";
import "./css/competition.css";

function ResultsVirtual(props) {

    const results = props.results
    const ranking = props.ranking
    const score = props.score
    function listUsers(results){
        var res = [];
        if(results === undefined)
            return;
        for (var i = 0; i < results.length; i++) {
            res.push(
                    <div> {results[i].username} </div>
            );
        }
        return res;
    }
   
    return(
        <>
        {results ?
        <div className="usersRankingContainer">
            <div><b>Rang lista natjecatelja s originalnog natjecanja:</b></div>
            <div className="usersRanking">{listUsers(results)}</div>
        </div>
        :
        <span></span>
        }
        <div className="brojBodova"> <b>Osvojeni broj bodova: </b><span className="bestTime">{score}</span></div>
        {ranking ? <div className="brojBodova"><b>Poredak na rank listi natjecanja s obzirom na osvojeni broj bodova: </b><span className="bestTime">{ranking}</span></div> : <span></span>}
        </>
    );
}

export default ResultsVirtual;