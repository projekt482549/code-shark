import React, { useEffect, useState } from "react"
import { useNavigate } from 'react-router-dom';
import Task from './Task';
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import upload from './resursi/upload.png';
import "./css/taskpractice.css";

function TaskPractice() {

    const pathname = window.location.pathname;
    let taskid = pathname.substring(8);
    const [score, setScore] = useState(0);
    const [taskData, setTaskData] = useState({});
    const[file, setFile] = useState();
    const navigate = useNavigate();
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    useEffect(() => {
        easyFetch(`${backend}/task/${taskid}`, "GET", null, headers)
            .then(res => {
                if(res.status === 400) {
                    console.log("error")
                }

                setTaskData(res);
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);

    function readFile(e){
        let input = e.target.files;
        let reader = new FileReader();

        reader.readAsArrayBuffer(input[0]);
        reader.onload = (e) => {
            let bytes = new Uint8Array(e.target.result);
            setFile(bytes);
        }
    }


    function onSubmit(e) {
        e.preventDefault();
        const data = {
            'file': file,
        }
        const headers = {
            'Content-Type': 'application/json'
        }
        var data2 = JSON.stringify(data, function(k, v) {
            if (v instanceof  Uint8Array) {
                return JSON.stringify(Array.apply([], v));
            }
            return v;
        });

        return (
            easyFetch(`${backend}/task/${taskid}`, "POST", data2, headers)
                .then(res => {
                    if(res.status === 401) {
                        setScore(res.score);
                    }
                    setScore(res.score);

                })
                .catch(err => {console.log("No comunication with server")})
        );
    }

    return(
        <div>



            <Task title = {taskData.title} description = {taskData.description} mscore = {taskData.mscore} />

            <div className="upload">
                <form className="taskUploadForm" onSubmit={onSubmit}>
                    <div class = "uploadImageContainer">
                        <label for="file-input">
                            <img className = "uploadImage" alt = "" src = {upload}/>
                        </label>
                        <input id = "file-input" type="file" name="file" onChange={readFile} accept=".java" />
                    </div>

                    <input className="uploadButton" type="submit" value="Predaj" />
                </form>
            </div>
            <div>
                Ostvareni bodovi:
                <div id="test_score">
                    { score }
                </div>
            </div>

        </div>
    );
}

export default TaskPractice;