import React, {useState} from "react";
import icon from "./resursi/voditelj_icon.jpg";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/request.css";


function PendingRequest(props) {
    const user = props.user;
    const key = props.key;
    const [btnClicked, setBtnClicked] = useState(false);

    const headers = {
        'Content-Type': 'application/json'
    }
    function handleClick(e) {
        e.preventDefault();
        console.log('You clicked submit.');
        setBtnClicked(true);
        //fetch get na admin/enable/{username voditelja kojeg zeli potvrditi}
        return (
            easyFetch(`${backend}/admin/enable/${user.username}`, "GET", null, headers)
            .then(res => {
                if(res.status === 401) {
                    //error
                } 
            })
            .catch(err => {console.log("No comunication with server")})
        )
    }

    return (<div className = "request" key = {key} >
                <div className = "imageInfoContainer">
                    <div>
                        <img src= {`data:image/jpeg;base64,${user.photo}`} className = "userImage" alt = ""/>
                    </div>
                    <div className = "userInfo">
                        <ul>
                            <li className = "listItem">korisničko ime: <b>{user.username}</b> </li>
                            <li className = "listItem">ime i prezime: <b>{user.name + " " + user.surname}</b></li>
                            <li className = "listItem">email : <b>{user.email}</b></li>
                        </ul>  
                    </div>
                </div>
                <button className = "acceptButton" disabled={btnClicked} onClick = {handleClick} >Potvrdi zahtjev</button>
            </div>
            );
}

export default PendingRequest;