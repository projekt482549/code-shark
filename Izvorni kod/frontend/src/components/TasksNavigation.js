import React from "react";
import "./css/competition.css";

function TasksNavigation(props) {

    const {competition_id, totalTasks}  = props;

    function taskLinks(total){
        var links = [];
        for (var i = 1; i < total+1; i++) {
            links.push(
                    <a href={'/natjecanje/' + competition_id + '/zadaci/' + i} key = {i} className="taskLink">
                        {i}                        
                    </a>
            );
        }
        return links;
    }

return (
    <nav className="zadatci">{taskLinks(totalTasks)}</nav>
    );
}

export default TasksNavigation;
