import React from "react";
import { useNavigate } from "react-router";
import Error from "./errorPages/Error";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/Login.css";
import "./css/Register.css";

function Login(props) {

    const[form, setForm] = React.useState({username: '', password: ''});
    const[error, setError] = React.useState('');
    const navigate = useNavigate()

    if(props.user.userId){
        return <Error id = "403"/>
    }

    function onChange(event) {
        const {name, value} = event.target;
        setForm(oldForm => ({...oldForm, [name]: value}));
    }

    function onSubmit(e) {
        e.preventDefault();
        setError("");

        const data = `username=${form.username}&password=${form.password}`;
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        return (
            easyFetch(`${backend}/login`, "POST", data, headers)
                .then(res => {
                    if(res.status === 401) {
                        setError("Login failed");
                        alert("Uneseni podaci nisu ispravni, pokušajte ponovo!");
                    } else if(res.status === 200) {
                        props.setUsername(form.username);
                        navigate("/");
                    }
                })
                .catch(err => {console.log("No comunication with server")})
        );
    }

  return (
    <div className="container">
        <h2>Prijava</h2>
        <form onSubmit={onSubmit}>
            <label htmlFor="username"/>
            <input type="text" placeholder="Korisničko ime"  name="username" onChange={onChange} required/>
            <label htmlFor="passsword"/>
            <input type="password" placeholder="Lozinka"  name="password" onChange={onChange}  required/>
            <br/>
            <button type="submit" className="prijava" >Prijava</button>
            <button type="reset" className="prijava reset" >Reset!</button>
            <p>Nemate račun? <a className="register" href="/registration">Registriraj se.</a></p>
        </form>
    </div>
  );
}

export default Login;