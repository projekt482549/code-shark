import React, { useEffect} from "react";
import { useNavigate } from "react-router";
import SearchBar from "./SearchBar";
import RandomVirtualButton from "./RandomVirtualButton";


function HomePage() {
    const navigate = useNavigate()
    
    useEffect(() => {
        if(!sessionStorage.getItem("username")){
            navigate("/login");
        }
    }, []);

    
    return(
        <>
        <div className = "searchBar">
            <SearchBar/>
        </div>
        <div className="virtualButton">
            <RandomVirtualButton/>
        </div>
        </>
    );
}

export default HomePage;