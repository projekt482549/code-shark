import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from 'react-router-dom';
import ResultsRegular from "./ResultsRegular";
import ResultsVirtual from "./ResultsVirtual";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/competition.css";

function CompetitionResultsVirtual() {

    const {competition_id} = useParams();
    const navigate = useNavigate();
    
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    // const [competitionData, setCompetitionData] = useState({});
    const [competitionResults, setCompetitionResults] = useState({});
    // useEffect(() => {
    //     easyFetch(`${backend}/exam/${competition_id}`, "GET", null, headers)
    //         .then(res => {
    //             if(res.status === 400) {
    //                 console.log("error")
    //             }
    //             setCompetitionData(res);
    //         })
    // }, []);
   
    useEffect(() => {
        easyFetch(`${backend}/exam/${competition_id}/infoVirtual`, "GET", null, headers)
            .then(res => {
                if(res.status === 400) {
                    console.log("error")
                }
                setCompetitionResults(res);
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);

    return(
        <>
        <div>
            <ResultsVirtual results = {competitionResults.usersFromOriginal} ranking = {competitionResults.ranking} score = {competitionResults.userScore}/> 
        </div>
        </>
    );
}

export default CompetitionResultsVirtual;