import React, { useState, useEffect } from "react";
import { useLocation } from 'react-router-dom'
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/task.css";

function Task(props) {

    const location = useLocation()

    const [title, setTitle] = useState();
    const [description, setDescription] = useState();
    const [mscore, setMscore] = useState();

    useEffect(() => {
        if(props.title !== undefined && props.description !== undefined && props.mscore !== undefined){
            setTitle(props.title);
            setDescription(props.description);
            setMscore(props.mscore);
        }else if(location !== undefined && 
            location.state === undefined && 
            location.state.title !== undefined && 
            location.state.description !== undefined && 
            location.state.mscore !== undefined) {
                setTitle(location.state.title);
                setDescription(location.state.description);
                setMscore(location.state.mscore);
        }else {
            const pathname = window.location.pathname;
            let pathnameSplit = pathname.split("/");
            let taskId = pathnameSplit.at(pathnameSplit.length - 1);
            
            easyFetch(`${backend}/task/${taskId}/taskInfo`, "GET")
                .then(res => {
                    if(res.status === undefined) {
                        console.log(res)
                        setTitle(res.title);
                        setDescription(res.description);
                        setMscore(res.points);
                    }
                })
                .catch(err => {console.log("No comunication with server")});
        }
    },[props])

    return(
        <div className="taskContainer">
        <h1 className="competitionTitle">{title}</h1>
        <div className="competitionInfo">
            <p>
            <i>Opis zadatka: </i> {description}<br/>
            <i>Broj bodova: </i> {mscore}<br/>
            </p>
        </div>
    </div>
    );
}

export default Task;