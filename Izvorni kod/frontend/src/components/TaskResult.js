import React, { useEffect, useState } from "react";
import { useParams} from 'react-router-dom';
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";

function TaskResult() {

    const {competition_id, task_no} = useParams();
    const [resultsInfo, setResultsInfo] = useState({});
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    ///natjecanje/natjecanjeid/zadaci/zadatakNo/info
    useEffect(() => {
        easyFetch(`${backend}/exam/${competition_id}/task/${task_no}/info`, "GET", null, headers)
            .then(res => {
                if(res.status === 400) {
                    console.log("error")
                }
                setResultsInfo(res);
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);

    function listUsers(results){
        var res = [];
        if(results === undefined)
        return;
        for (var i = 0; i < results.length; i++) {
            res.push(
                    <div> {results[i].username} </div>
            );
        }
        return res;
    }
    function saveByteArray(reportName, byte) {
        var blob = new Blob([byte], {type: 'application/octet-stream'});
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        var fileName = reportName;
        link.download = fileName;
        link.click();
    }

    function _base64ToArrayBuffer(base64) {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }
    
    function downloadFile(e){
        e.preventDefault();
        console.log(resultsInfo.bestAnswer)
        var bytes = _base64ToArrayBuffer(resultsInfo.bestAnswer)
        console.log(bytes)
        saveByteArray("najbolje_rjesenje.java", bytes);
    }

    return(
        <>
        {resultsInfo.usersThatFinishedTaskOnExam ? 
            <>
            <div className="usersRankingContainer">
                <div><b>Korisnici koji su predali rješenje:</b></div>
                <div className="usersRanking">{listUsers(resultsInfo.usersThatFinishedTaskOnExam)}</div>
            </div>
            <div className="brojBodova">
                <b>Najveći broj bodova ostvaren na ovom zadatku je <span className="bestScore">{resultsInfo.bestScore}</span></b>
            </div>
            <div className="vrijemeIzvodjenja">
                <b>Najbrže vrijeme izvođenja postignuto na ovom zadatku je <span className="bestTime">{resultsInfo.bestAverageTime}</span>ms</b>
            </div >
            {resultsInfo.userCanAccessBestAnswer && resultsInfo.bestAnswer ? <button type="button" className = "downloadBestResultButton" onClick={downloadFile}>Preuzmi najbolje rješenje</button> : <div className="notAllowed"> Nije dopušteno preuzimanje najboljeg rješenja</div>}
            </> 
            :
            <div className="noAvailableResults"><b>Nema predanih rješenja</b></div>}
        </>
    );
}

export default TaskResult;