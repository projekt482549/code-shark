import React, { useEffect, useState } from "react";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import icon from "./resursi/voditelj_icon.jpg";
import userImage from "./resursi/user-solid.svg";
import "./css/searchBar.css";

function SearchBar() {
    const [searchTerm, setSearchTerm] = useState("");
    const [usersData, setUsersData] = useState([]);       
    const dropDownLimit = 10;

    useEffect(() => {
        if(sessionStorage.username !== "")
            easyFetch(`${backend}/userinfo`, "GET")
                .then(res => {
                    if(res.status === 401) {
                        console.log("error")
                    }
                    setUsersData(res);
                })
                .catch(err => {console.log("No comunication with server")});
    }, []);

    return(
        <div className="search">
            <input id="searchInput" type="text" placeholder="Find user" name="search" autoComplete="off" 
                onChange = {event => setSearchTerm(event.target.value)}/>

            <div className = "resultsContainer">
            {
            usersData
                .filter((val) => { 
                    return val.enabled === true && val.accounttype !== 2; 
                })
                .filter((val) => {
                    if(searchTerm === "") {
                        return false;
                    }    
                    return val.username.toLowerCase().includes(searchTerm.toLowerCase());
                })
                .sort((a, b) => {
                    return a.accounttype - b.accounttype;
                })
                .slice(0, dropDownLimit)
                .map((val, key) => {
                    return <div className = "result" key = {key} >
                                <div className = "imageContainer">
                                    <img src = {(val.photo && `data:image/jpeg;base64,${val.photo}`) || userImage} alt = "" className = "search_image"/>
                                </div>
                                {/* trebalo bi ici Link umjesto a, ali onda ne radi jer SerachBar nije u Routesima u App.js */}
                                <a href= {"/profil/" +  val.username }><span className="link-spanner"></span></a>
                                <div className = "usernameContainer">
                                    <p>{val.username}</p>
                                </div>
                                {val.accounttype === 1 ? ( <img src = {icon} alt = "" className = "scaled-image scaled-image-fixed"/>):(<p> </p>)}
                            </div>
                })
            } 
            </div>

        </div>
    );
}

export default SearchBar;