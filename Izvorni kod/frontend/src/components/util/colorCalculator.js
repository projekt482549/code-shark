//Source - https://dev.to/isarisariver/how-to-determine-font-color-based-on-a-random-background-color-8ek

var seedrandom = require('seedrandom');

function calculateBackgroundColor(id) {
    var rand = seedrandom(id);
    let color = Math.floor(rand(id)*16777215).toString(16)

    while (color.length<6) {
        color = Math.floor(Math.random()*16777215).toString(16)
    }
    color = "#" + color;

    return color;
}

function calculateFontColor(color) {
    let red = parseInt(color.substring(0,2),16);
    let green = parseInt(color.substring(2,4),16);
    let blue = parseInt(color.substring(4,6),16);
    let brightness = red*0.299 + green*0.587 + blue*0.114;

    if (brightness > 180)
        return "#000000"
    else
        return "#ffffff";
}

export {calculateBackgroundColor, calculateFontColor};

