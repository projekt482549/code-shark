

async function easyFetch (_url, _method, _body, _headers) {

    let url;
    let fetchOptions;

    if(_method === "GET" || _method === "get")
        fetchOptions = {
            method: _method,
            headers: _headers,
            credentials: 'include'
        }
    else
        fetchOptions = {
            method: _method,
            headers: _headers,
            credentials: 'include',
            body: _body
        }
        
    url = _url;

    let res = await fetch(url, fetchOptions);
    let data = await isValidJson(res);
    if(data !== false)
        return data;
    else if (res.headers.get("Content-Type") === "application/json")
        return [];
    else
      return res;

      async function isValidJson(resource) {
        let data;
        
        try {
          data = await resource.json();
        } catch (_) {
          return false;  
        }
      
        return data;
      }
}


export default easyFetch;