/**
 * Function tries to extract date from string and return it in format yyyy-mm-dd
 * 
 * @param {String} data should be date in format: "2021-12-17T13:00:00.000+00:00"
 * @returns 
 */
function getDate(data) {
    if(data === undefined)
        return undefined;

    let date;

    if(data.includes("T"))
        date = data.split("T")[0];
    else if(data.match(/\d{4}[.\-/]\d{1,2}[.\-/]\d{1,2}\.?/)) {
        if(data.includes(".")){
            if(data.endsWith("."))
                data = data.substring(0, data.length - 1);
            data.replace(".", "-");
        }
        else if(data.includes("/"))
            data.replace("/", "-");
        date = data;
    }
    else if(data.match(/\d{1,2}[.\-/]\d{1,2}[.\-/]\d{4}\.?/)) {
        if(data.includes(".")){
            let temp = data.split(".");
            data = temp[2] + "-" + temp[1] + "-" + temp[0];
        }else if(data.includes("-")){
            let temp = data.split("-");
            data = temp[2] + "-" + temp[1] + "-" + temp[0];
        } else if(data.includes("/")){
            let temp = data.split("/");
            data = temp[2] + "-" + temp[1] + "-" + temp[0];
        }
        date = data;
    }

    return date;
}

/**
 * Given date string: "2021-12-16T13:27:59.609+00:00", and some other formats
 * extracts year
 * 
 * @param {String} data 
 * @returns Year from date string in integer format
 */
function getYear(data){
    let date = getDate(data);
    
    if(date === undefined)
        return undefined;

    let year = date.split("-")[0];

    let yearNo = parseInt(year);
    if(isNaN(yearNo)) {
        console.log("Error while reading year, expected number and got: " + year);
        console.log("Defaulting to 0");
        yearNo = 0;
    }

    return yearNo;
}

/**
 * Given date string: "2021-12-16T13:27:59.609+00:00", and some other formats
 * extracts month
 * 
 * @param {String} data 
 * @returns Month from date string in integer format
 */
function getMonth(data){
    let date = getDate(data);

    if(date === undefined)
        return undefined;

    let month = date.split("-")[1];

    let monthNo = parseInt(month);
    if(isNaN(monthNo)) {
        console.log("Error while reading month, expected number and got: " + month);
        console.log("Defaulting to 0");
        monthNo = 0;
    }


    return monthNo;
}

/**
 * Given date string: "2021-12-16T13:27:59.609+00:00", and some other formats
 * extracts day
 * 
 * @param {String} data 
 * @returns Day from date string in integer format
 */
function getDay(data){
    let date = getDate(data);

    if(date === undefined)
        return undefined;

    let day = date.split("-")[2];

    let dayNo = parseInt(day);
    if(isNaN(dayNo)) {
        console.log("Error while reading day, expected number and got: " + day);
        console.log("Defaulting to 0");
        dayNo = 0;
    }

    return dayNo;
}

/**
 * 
 * @param {int} month 
 * @returns String of months name in Croatian
 */
function getMonthName(month) {

    let calendarMonth = "";

    if(month > 12 || month < 1)
        console.log("Expected month as integer and got: "+ month);

    switch (month) {
        case 1:
            calendarMonth = "Siječanj";
            break;
        case 2:
            calendarMonth = "Veljača";
            break;
        case 3:
            calendarMonth = "Ožujak";
            break;
        case 4:
            calendarMonth = "Travanj";
            break;
        case 5:
            calendarMonth = "Svibanj";
            break;
        case 6:
            calendarMonth = "Lipanj";
            break;
        case 7:
            calendarMonth = "Srpanj";
            break;
        case 8:
            calendarMonth = "Kolovoz";
            break;
        case 9:
            calendarMonth = "Rujan";
            break;
        case 10:
            calendarMonth = "Listopad";
            break;
        case 11:
            calendarMonth = "Studeni";
            break;
        case 12:
            calendarMonth  = "Prosinac";
            break;
        default:
            console.log("Error while geting month name, expected month as integer between 1 and 12 and got: " + month);
            console.log("Defaulting to \"\"");
            calendarMonth = "";
    }

    return calendarMonth;
}

/**
 * Year can be passed on to chacke for leap year because of febuary
 * 
 * @param {int} month 
 * @param {int} year if not passed will calculate for non-leap year
 * @returns Number of days in month
 */
function getMonthNumberOfDays(month, year) {
    let numberOfDays;
    if(year === undefined)
        year = 2001;

    if(month > 12 || month < 1)
        console.log("Expected month as integer and got: "+ month);

    switch (month) {
        case 2:
            if(year % 4 !== 0 || (year % 100 === 0 && year % 400 !== 0))
                numberOfDays = 28;
            else
                numberOfDays = 29
            break;
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            numberOfDays = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            numberOfDays = 30;
            break;
        default:
            console.log("Error while calculating number of days, expected month as integer between 1 and 12 and got: " + month);
            console.log("Defaulting to 0");
            numberOfDays = 0;
    }

    return numberOfDays;
}

/**
 * @param {String} date
 * @param {boolean} alwaysTwoDigits should there always be two digits (eg. 2022-01-05)
 * @returns Formated string
 */
 function formatDate(date, alwaysTwoDigits) {

    let newDate = "";

    date = getDate(date);

    let dateDigit = date.split("-");
    
    newDate = dateDigit[0] + "-";
    if(alwaysTwoDigits){
        if(dateDigit[1].length === 1)
            newDate += "0" + dateDigit[1] + "-";
        else
            newDate += dateDigit[1];
        if(dateDigit[2].length === 1)
            newDate += "0" + dateDigit[2];
        else
            newDate += dateDigit[2];
    }else
        newDate = date

    return newDate;

}



/**
 * Function tries to extract date from string and return it in format hh:mm:ss
 * 
 * @param {String} data should be date in format: "2021-12-17T13:00:00.000+00:00", but some other formats are accepted
 * @returns 
 */
function getTime(data) {
    if(data === undefined)
        return undefined;

    let time;

    if(data.includes("T"))
        time = data.split("T")[1];
    else if(data.match(/\d{1,2}:\d{1,2}:\d{1,2}/)) {
        time = data;
    }

    if(time.includes(".")){
        time = time.split(".")[0];
    }

    data = time.split(":");

    for(let i = 0; i < 3; i++)
        if(data[i].length === 1)
            data[i] = "0" + data[i];
    
    time = data[0] + ":" + data[1] + ":" + data[2];

    return time;
}

/**
 * Given date string: "2021-12-16T13:27:59.609+00:00", and some other formats
 * extracts hours
 * 
 * @param {String} data 
 * @returns Hours from date string
 */
function getHours(data) {
    let time = getTime(data);

    if(time === undefined)
        return undefined;

    let hours = time.split(":")[0];

    let hoursNo = parseInt(hours);

    if(isNaN(hoursNo)) {
        console.log("Error while reading hours, expected number and got: " + hours);
        console.log("Defaulting to 0");
        hoursNo = 0;
    }

    return hoursNo;
}

/**
 * Given date string: "2021-12-16T13:27:59.609+00:00", and some other formats
 * extracts minutes
 * 
 * @param {String} data 
 * @returns Minutes from date string
 */
function getMinutes(data) {
    let time = getTime(data);

    if(time === undefined)
        return undefined;

    let minutes = time.split(":")[1];

    let minutesNo = parseInt(minutes);

    if(isNaN(minutesNo)) {
        console.log("Error while reading minutes, expected number and got: " + minutes);
        console.log("Defaulting to 0");
        minutesNo = 0;
    }

    return minutesNo;
}

/**
 * Given date string: "2021-12-16T13:27:59.609+00:00", and some other formats
 * extracts seconds
 * 
 * @param {String} data 
 * @returns Seconds from date string
 */
function getSeconds(data) {
    let time = getTime(data);

    if(time === undefined)
        return undefined;

    let seconds = time.split(":")[2];

    let secondsNo = parseInt(seconds);

    if(isNaN(secondsNo)) {
        console.log("Error while reading seconds, expected number and got: " + seconds);
        console.log("Defaulting to 0");
        secondsNo = 0;
    }

    return secondsNo;
}

/**
 * Convert time to decimal number
 * 
 * @param {String} time 
 * @param {boolean} hours 
 * @param {boolean} minutes 
 * @param {boolean} seconds 
 * @returns Time calculated to hours, minutes or seconds (depending on which is true)
 */
function getTimeAsDecimal(time, hours, minutes, seconds) {

    let newTime = 0;

    if(hours) {
        minutes = false;
        seconds = false;
    }else if(minutes)
        seconds = false;

    let hoursTime = getHours(time);
    let minutesTime = getMinutes(time);
    let secondsTime = getSeconds(time);

    if(!seconds) {
        minutesTime = minutesTime + secondsTime / 60;
    }
    if(!hours) {
        minutesTime = minutesTime + hoursTime * 60;
    }
    if(!minutes) {
        hoursTime = hoursTime + minutesTime / 60;
        secondsTime = secondsTime + minutesTime * 60;
    }

    if(hours)
        newTime = hoursTime;
    else if(minutes)
        newTime = minutesTime;
    else if(seconds)
        newTime = secondsTime;

    return newTime;

}

/**
 * 
 * @param {String} time1 in format "hh:mm:ss"
 * @param {String} time2  in format "hh:mm:ss"
 * @returns New time in format "hh:mm:ss" calculated by adding time1 and time 2
 */
function addTimes(time1, time2) {
    let hour1 = getHours(time1);
    let hour2 = getHours(time2);
    let minute1 = getMinutes(time1);
    let minute2 = getMinutes(time2);
    let second1 = getSeconds(time1);
    let second2 = getSeconds(time2);

    let newSecond = second1 + second2;
    let newMinute = minute1 + minute2;
    let newHour = hour1 + hour2;
    if(newSecond >= 60) {
        newMinute = newMinute + Math.floor(newSecond/60);
        newSecond = newSecond % 60;
    }
    if(newMinute >= 60) {
        newHour = newHour + Math.floor(newMinute/60);
        newMinute = newMinute % 60;
    }

    let newTime = newHour + ":" + newMinute + ":" + newSecond;

    return newTime;
}

/**
 * Function does not calculate decimal numbers (there is another function for that),
 * but will convert hours to minutes and minutes to secounds if not showing first
 * 
 * @param {String} time
 * @param {boolean} hours should hours be shown
 * @param {boolean} minutes should minutes be shown
 * @param {boolean} seconds should seconds be shown
 * @param {boolean} alwaysTwoDigits should there always be two digits (eg. 05:10:08)
 * @returns Formated string
 */
function formatTime(time, hours, minutes, seconds, alwaysTwoDigits) {

    let newTime = "";

    if(hours && seconds)
        minutes = true;

    let hoursTime = getHours(time);
    let minutesTime = getMinutes(time);
    let secondsTime = getSeconds(time);

    if(!hours) {
        minutesTime = minutesTime + hoursTime * 60;
    }
    if(!minutes) {
        secondsTime = secondsTime + minutesTime * 60;
    }

    if(hours) {
        if(alwaysTwoDigits && hoursTime < 10)
            newTime = "0" + hoursTime;
        else
            newTime = hoursTime;
    }
    if(minutes) {
        let minutesTemp;
        if(alwaysTwoDigits && minutesTime < 10)
            minutesTemp = "0" + minutesTime;
        else
            minutesTemp = minutesTime;

        if(hours)
            newTime = newTime + ":" + minutesTemp
        else
            newTime = minutesTemp;
    }
    if(seconds) {
        let secondsTemp;
        if(alwaysTwoDigits && secondsTime < 10)
            secondsTemp = "0" + secondsTime;
        else
            secondsTemp = secondsTime;

        if(minutes)
            newTime = newTime + ":" + secondsTemp
        else
            newTime = secondsTemp;
    }

    return newTime;

}


export {getDate ,getDay, getMonth, getYear, getMonthName, getMonthNumberOfDays, formatDate};

export {getTime ,getHours, getMinutes, getSeconds, getTimeAsDecimal, addTimes, formatTime};