import React, { useState } from "react";
import { useNavigate } from "react-router";
import Error from "./errorPages/Error";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/Register.css";

function Register(props) {
    const[form, setForm] = useState({username:'', name: '', surname: '', email: '', password: '', uloga: '', picture: ''});
    const[error, setError] = useState('');
    const navigate = useNavigate()

    if(props.user.userId){
        return <Error id = "403"/>
    }

    function onChange(event) {
        const {name, value} = event.target;
        setForm(oldForm => ({...oldForm, [name]: value}));
    }

    function readPhoto(e) {
        let input = e.target.files;
        let reader = new FileReader();

        reader.readAsArrayBuffer(input[0]);
        reader.onload = (e) => {
            let bytes = new Uint8Array(e.target.result);
            //let base64 = Buffer.from(bytes).toString('base64');
            //console.log(base64);
            setForm(oldForm => ({...oldForm, picture: bytes}));
        }
    }

    function onSubmit(e) {
        e.preventDefault();
        setError("");

        let uloga = form.uloga;
        
        if(uloga === "voditelj") uloga = 1;
        else uloga = 0;

        const data = {
            'username': form.username,
            'password': form.password,
            'email': form.email,
            'name': form.name,
            'surname': form.surname,
            'photo': form.picture,
            'accounttype': uloga
        }
        
        const headers = {
            'Content-Type': 'application/json'
        }

        var data2 = JSON.stringify(data, function(k, v) {
            if (v instanceof  Uint8Array) {
                return JSON.stringify(Array.apply([], v));
            }
            return v;
        });

        return (
            easyFetch(`${backend}/registration`, "POST", data2, headers)
                .then(res => {
                    if (res.userId !== null){
                        navigate('/login'); 
                    }else {
                        if (res.username === "email"){
                            alert("Uneseni email je zauzet!");
                        } else {
                            if (res.username === "username")
                                alert("Uneseni username  je zauzet!");
                        }
                    }
                })
                .catch(err => {console.log("No comunication with server")})
        );
    }

    return(
        <div className="container">
            <h2>Registracija</h2>
            <form onSubmit={onSubmit}>
                <label htmlFor="username"/>
                <input type="text" placeholder="Korisničko ime" name="username" onChange={onChange} required/>

                <label htmlFor="name"/>
                <input type="text" placeholder="Ime"  name="name" onChange={onChange} required/>

                <label htmlFor="lastname"/>
                <input type="text" placeholder="Prezime"  name="surname" onChange={onChange} required/>

                <label htmlFor="email"/>
                <input type="email" placeholder="Email"  name="email" onChange={onChange}  required/>

                <label htmlFor="passsword"/>
                <input type="password" placeholder="Lozinka" name="password" minLength={8} onChange={onChange}  required/>

                <fieldset>
                    <legend>Uloga</legend>
                    <label><input type="radio" name="uloga" onChange={onChange} value="natjecatelj" defaultChecked/>Natjecatelj</label>
                    <br/>
                    <label><input type="radio" name="uloga" onChange={onChange}  value="voditelj" />Voditelj natjecanja</label>
                    <br/>
                </fieldset>

                <div className="upload">
                    <label>Profilna fotografija: </label>
                    <input type="file" name="picture" onChange={readPhoto} accept="image/*" />
                </div>

                <input className="register-btn" id="submit" type="submit"  value="Registriraj se" />

            </form>
        </div>
    );
}

export default Register;