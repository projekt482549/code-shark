import React from "react";
import { useNavigate } from "react-router";
import { TextField, InputAdornment } from "@material-ui/core";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";

function NewRandomVirtual() {

    const navigate = useNavigate();
    const [error, setError] = React.useState('');

    const durationRef = React.useRef();
    const numOfTasksRef = React.useRef();

    const headers = {
        'Content-Type': 'application/json'
    }

    function isInt(value) {
        return !isNaN(value) &&
               parseInt(Number(value)) == value && 
               !isNaN(parseInt(value, 10));
    }

    function onSubmit(e) {
        e.preventDefault();
        setError("");

        var duration = durationRef.current.value.toString()
        var numOfTasks = numOfTasksRef.current.value

        if(!duration.match("[0-9]{2}:[0-9]{2}:[0-9]{2}") || duration.length !== 8 || duration === "00:00:00"){
            alert("Neispravan format trajanja natjecanja!")
            return 
        }
        //var hrs = duration.substring(0,2)
        var min = duration.substring(3,5)
        var sec = duration.substring(6)
        if(min >= 60 || sec >= 60){
            alert("Neispravan format trajanja natjecanja!")
            return 
        }
        if(!isInt(numOfTasks) || numOfTasks <= 0){
            alert("Neispravan broj zadataka!")
            return 
        }
        const data = {
            'numOfTasks': numOfTasks,
            'duration': duration
        }
        var data2 = JSON.stringify(data, function(k, v) {
            if (v instanceof  Uint8Array) {
                return JSON.stringify(Array.apply([], v));
            }
            return v;
        });
        
        return(
            easyFetch(`${backend}/exam/newVirtual`, "POST", data2, headers)
              .then(res => {
                  if(res.status === 401) {
                      setError("New virtual competition failed");
                  } else {
                          navigate(`/natjecanje/${res.examId}`);
                  }
              })
              .catch(err => {console.log("No comunication with server")})
        );
    }

    return(
        <div className="container-competition">
        <h2>Novo virtualno natjecanje</h2>
        <form onSubmit={onSubmit}>
            <div className="flex-container">
                <div>
                    <div className="virtualInput">
                        <label className="virtualInputLabel">Trajanje natjecanja: </label>
                        <TextField
                            inputRef={durationRef}
                            type="text"
                            id="duration"
                            name="duration"
                            sx={{ m: 1, width: '250ch' }}
                            required
                            InputProps={{
                                startAdornment: <InputAdornment position="start">h:min:s</InputAdornment>,
                            }}
                            defaultValue={"00:00:00"}
                        />
                    </div>
                    <div className="virtualInput">
                        <label className="virtualInputLabel">Broj zadataka: </label>
                        <TextField
                            inputRef={numOfTasksRef}
                            id="numOfTasks"
                            name="numOfTasks"
                            sx={{ m: 1, width: '250ch' }}
                            defaultValue={0}
                            required
                        />
                    </div>
                </div>
            </div>
            <div>
                <input className="submit-btn" id="submit" type="submit"  value="Potvrdi" onSubmit={onSubmit}/>
            </div>
        </form>
    </div>
    );
}

export default NewRandomVirtual;