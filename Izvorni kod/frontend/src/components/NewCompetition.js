import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router";
import { TextField } from "@material-ui/core";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/newCompetition.css";
//import MOCK_ZADATAK from './resursi/MOCK_DATA_ZADATAK.json'


function NewCompetition(props) {
    const navigate = useNavigate();
    const { username } = useParams();

    const [form, setForm] = React.useState({name: '', description: '', cup:''});
    const [error, setError] = React.useState('');
    const [tasks, setTasks] = useState([]);

    const today = new Date()
    const dateRef = React.useRef();
    const startTimeRef = React.useRef();
    const endTimeRef = React.useRef();

    const listOfTasks = []

    const headers = {
        'Content-Type': 'application/json'
    }

    useEffect(()=>{
           easyFetch(`${backend}/task/${username}`, "GET", null, headers)
               .then( res => {
                    setTasks(res);
                })
                .catch( err => {
                    console.log("error");
                })
        }, []);

    function onChange(event) {
        const {name, value} = event.target;
        setForm(oldForm => ({...oldForm, [name]: value}));
    }

    function onChangeCheckBox(e) {
        if(e.target.checked) {
            listOfTasks.push(e.target.value)
        } else {
            var index = listOfTasks.indexOf(e.target.value);
            if (index > -1) {
                listOfTasks.splice(index, 1);
            }

        }
    }

    function readPhoto(e) {
        let input = e.target.files;
        let reader = new FileReader();

        reader.readAsArrayBuffer(input[0]);
        reader.onload = (e) => {
            let bytes = new Uint8Array(e.target.result);
            setForm(oldForm => ({...oldForm, cup: bytes}));
        }
    }


    function onSubmit(e) {
        e.preventDefault();
        setError("");

        var [hh1, mm1] = startTimeRef.current.value.toString().split(":");
        var [hh2, mm2] = endTimeRef.current.value.toString().split(":");
        var start = parseInt(hh1*60) + parseInt(mm1);
        var end = parseInt(hh2)*60 + parseInt(mm2);
        var dur = (end - start)/60
        var hh = parseInt(dur);
        var mm = Math.round((dur - hh)*60);
        if(hh.toString().length == 1) {
            hh = "0" + hh;
        }
        if(mm.toString().length == 1) {
            mm = "0" + mm;
        }
        var duration= hh+":"+mm+":00";

        const data = {
            'name': form.name,
            'description': form.description,
            'date': dateRef.current.value,
            'startTime': startTimeRef.current.value,
            'duration': duration,
            'cup': form.cup,
            'tasks': listOfTasks
        }

        var data2 = JSON.stringify(data, function(k, v) {
            if (v instanceof  Uint8Array) {
                return JSON.stringify(Array.apply([], v));
            }
            return v;
        });

        console.log(data2);
        return(
            easyFetch(`${backend}/exam`, "POST", data2, headers)
              .then(res => {
                  alert('Uspješno napravljeno natjecanje')
                  navigate('/kalendar');
                })
                .catch(err => {console.log("No comunication with server")})
        );

    }

    function dateValue(){
        
        if (today.getMonth()+1 < 10 && today.getDate()<10)
            return today.getFullYear()+'-0'+(today.getMonth()+1)+'-0'+today.getDate();
        else {
            if (today.getMonth()+1 < 10)
                return today.getFullYear()+'-0'+(today.getMonth()+1)+'-'+today.getDate();
            else {
                return today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            }
        }
        
    }

    return(
            <div className="container-competition">
                <h2>Novo natjecanje</h2>
                <form onSubmit={onSubmit}>
                    <div className="flex-container">
                        <div>
                            <label htmlFor="name"/>
                            <input type="text" id="competitionName" placeholder="Naziv natjecanja" name="name" onChange={onChange} required/>

                            <label htmlFor="description"/>
                            <textarea id="description" placeholder="Opis natjecanja" name="description" onChange={onChange} required rows={3}/>

                            <div className="left-align">
                                <label>Datum natjecanja: </label>
                                <TextField
                                    inputRef={dateRef}
                                    id="date"
                                    name="date"
                                    type="date"
                                    //defaultValue={today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()}
                                    defaultValue={dateValue()}
                                    inputProps={{
                                        min: today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(),
                                    }}
                                    required
                                />
                            </div>
                            <div className="left-align">
                                <label>Početak natjecanja: </label>
                                <TextField
                                    inputRef={startTimeRef}
                                    id="startTime"
                                    name="startTime"
                                    type="time"
                                    defaultValue={"08:00"}
                                    required

                                />
                            </div>
                            <div className="left-align">
                                <label>Kraj natjecanja: </label>
                                <TextField
                                    inputRef={endTimeRef}
                                    id="endTime"
                                    name="endTime"
                                    type="time"
                                    defaultValue={"08:00"}
                                    required
                                />
                            </div>
                            <div className="left-align">
                                <label>Slika pehara: </label>
                                <input type="file" name="picture" onChange={readPhoto} accept="image/*" />
                            </div>
                        </div>
                        <div className="flex-item">
                            <fieldset>
                                <legend>Zadaci</legend>
                                {Array.from(Array(tasks.length), (e, i) => {
                                    return <label><input type="checkbox" name="task" onChange={onChangeCheckBox} value={tasks[i].taskId}/>{tasks[i].title}<br/></label>
                                })}
                            </fieldset>
                        </div>
                    </div>

                    <div>
                        <input className="submit-btn" id="submit" type="submit"  value="Potvrdi" onSubmit={onSubmit}/>
                    </div>
                </form>

            </div>

    );
}

export default NewCompetition;