import React from "react";
import CalendarEvent from "./CalendarEvent";
import Error from "./errorPages/Error";
import { calculateBackgroundColor, calculateFontColor } from "./util/colorCalculator";

function CalendarItem(props) {
    const {year, month, day, dayName, special, empty} = props;

    //CalendarItem je "naslov" tj. mjesto gdje treba zapisati godinu i mjesec
    if(year !== undefined && month !== undefined) {
        return (
            <div className = "calendar_yearmonth">
                <h1>{month}</h1>
                <h2>{year}</h2>
            </div>
        );
    }

    //CalendarItem je kucica u kalendaru, treba ustvrditi koi stil se primjenjuje i postoje li dogadaji koji pridadaju toj kucici
    if(day !== undefined){
        let {weekend, current, events} = props;
        let eventList = [];
        let className = "calendar_day";
        if(weekend)
            className += " calendar_weekend";
        if(current)
            className += " calendar_current_day";

        //Ako postoje dogadaji onda ih dodaj u kucicu
        if(events !== undefined) {
            events.forEach(element => {
                element.backgroundColor = calculateBackgroundColor(element.title);
                element.fontColor = calculateFontColor(element.backgroundColor);
                eventList.push(<CalendarEvent event = {element} key = {element.examId}/>);
            });
        }
        if(events.length === 0)
            events.day = day;
        //Vrati gotovu kucicu sa svim pripadnim dogadajima
        return (
            <div className = {className} onClick = {props.changeTimeMapData(events)}>
                <h4>{day}</h4>
                <div className = "calendar_event_list">
                    {eventList}
                </div>
            </div>
        );
    }

    //CalendarItem je kucica sa nazivom dana
    if(dayName !== undefined){
        return (
        <div className = "calendar_day_name">
            <h3>{dayName}</h3>
        </div>
        );
    }

    //CalendarItem je mjesto znaka za pomicanje
    if(special !== undefined){
        if(special === "<") {
            return (
                <div className = "calendar_arrow" onClick={props.moveMonth(-1)} id = "left_arrow">
                    <h2>{special}</h2>
                </div>
            );
        }
        if(special === ">") {
            return (
                <div className = "calendar_arrow" onClick={props.moveMonth(1)} id = "right_arrow">
                    <h2>{special}</h2>
                </div>
            );
        }
    }

    //CalendarItem je prazna kucica
    if(empty !== undefined){
        return <div className = "calendar_day calendar_empty_day"><h1>{}</h1></div>
    }

    //Ako nista od prethodnih uvjeta nije zadovoljeno onda CalendarItem nije dobro definiran, tj. interna pogreska u kodu
    return(
        <Error id = {500}/>
    );
}

export default CalendarItem;