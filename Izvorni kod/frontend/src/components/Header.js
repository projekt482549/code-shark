import React from "react";
import LogoImage from "./LogoImage";
import Navigation from "./Navigation";
import "./css/header.css";

function Header(props) {
    return(
        <div className = "header">
            <LogoImage/>
            <Navigation {... props}/>
        </div>
    );
}

export default Header;