import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/Register.css";



function DataEdit() {
    let [form, setForm] = React.useState({username: null, name: null, surname: null, email: null, password: null, uloga: null, picture: null});
    const [error, setError] = React.useState('');
    const [userData, setUserData] = React.useState({appUser:{username:'', name: '', surname: '', email: '', password: '', uloga: '', picture: ''}});
    const navigate = useNavigate();
    const pathname = window.location.pathname;
    let username = pathname.substring(20);

    useEffect(() => {
        easyFetch(`${backend}/userinfo/${username}`, "GET")
            .then(res => {
                if(res.status === 401) {
                    console.log("error")
                }
                setUserData(res);
                form.username = username;
                form.name = res.appUser.name;
                form.surname = res.appUser.surname;
                form.email = res.appUser.email;
                //console.log(form);
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);


    function onChange(event) {
        const {name, value} = event.target;
        setForm(oldForm => ({...oldForm, [name]: value}));
    }

    function readPhoto(e) {
        let input = e.target.files;
        let reader = new FileReader();

        reader.readAsArrayBuffer(input[0]);
        reader.onload = (e) => {
            let bytes = new Uint8Array(e.target.result);
            //let base64 = Buffer.from(bytes).toString('base64');
            //console.log(base64);
            setForm(oldForm => ({...oldForm, picture: bytes}));
        }
    }

    function onSubmit(e) {
        e.preventDefault();
        setError("");

        let uloga = form.uloga;
        
        if(uloga === "voditelj") uloga = 1;
        else uloga = 0;


        //nije elegantno, ali radi...ako se nade vremena trebalo bi popravit!
        if (form.username === null)
            form.username = userData.appUser.username;

        if (form.name === null)
            form.name = userData.appUser.name;
        
        if (form.surname === null)
            form.surname = userData.appUser.surname;
        
        if (form.email === null)
            form.email = userData.appUser.email;
        //ovo, jel

        const data = {
            'username': form.username,
            'password': form.password,
            'email': form.email,
            'name': form.name,
            'surname': form.surname,
            'accounttype': uloga,
            'photo': form.picture,
        }
        
        const headers = {
            'Content-Type': 'application/json'
        }

        var data2 = JSON.stringify(data, function(k, v) {
            if (v instanceof  Uint8Array) {
                return JSON.stringify(Array.apply([], v));
            }
            return v;
        });

        return (
            easyFetch(`${backend}/admin/${username}`, "POST", data2, headers)
                .then(res => {
                    //if (res.message === 'ok'){
                    //    navigate('/korisnici');
                    //} else{
                    //    if (res.message === 'username'){
                    //    setError("Change failed");
                    //    alert("Odabrani username je zauzet");
                    //    } else {
                    //        if (res.message === 'email'){
                    //            setError("Change failed");
                    //            alert("Odabrani email je zauzet");
                    //        } else{
                    //            alert("Dogodila se neočekivana pogreška");
                    //        }
                    //    }
                    //}

                    switch (res.message){
                        case 'ok':
                            navigate('/korisnici');
                            break;
                        case 'username':
                            setError("Change failed");
                            alert("Odabrani username je zauzet");
                            break;
                        case 'email':
                            setError("Change failed");
                            alert("Odabrani email je zauzet");
                            break;
                        default:
                            alert("Dogodila se neočeivana pogreška!");
                    }
                })
                .catch(err => {console.log("No comunication with server")})
        );
    }

    function provjeriUlogu(accounttype){
        if (userData.appUser.accounttype == accounttype){
            return true;
        }
    }


    return(
        <div className="container">
            <h2>Uređivanje podataka</h2>
            <form onSubmit={onSubmit}> 
                <label htmlFor="username"/>
                <input type="text" placeholder="Username" name="username" onChange={onChange} defaultValue ={userData.appUser.username}/>

                <label htmlFor="name"/>
                <input type="text" placeholder="Ime"  name="name" onChange={onChange} defaultValue = {userData.appUser.name}/>

                <label htmlFor="lastname"/>
                <input type="text" placeholder="Prezime"  name="surname" onChange={onChange} defaultValue = {userData.appUser.surname}/>

                <label htmlFor="email"/>
                <input type="email" placeholder="Email"  name="email" onChange={onChange} defaultValue = {userData.appUser.email}/>

                <label htmlFor="passsword"/>
                <input type="password" placeholder="Lozinka" name="password" onChange={onChange} defaultValue = {null} />

                <fieldset>
                    <legend>Uloga</legend>
                    <label><input type="radio" name="uloga" onChange={onChange} value="natjecatelj" checked = {provjeriUlogu(0)} />Natjecatelj</label>
                    <br/>
                    <label><input type="radio" name="uloga" onChange={onChange}  value="voditelj" checked = {provjeriUlogu(1)} />Voditelj natjecanja</label>
                    <br/>
                </fieldset>

                <div className="upload">
                    <label>Profilna fotografija: </label>
                    <input type="file" name="picture" onChange={readPhoto} accept="image/*" />
                </div>

                <input className="register-btn" id="submit" type="submit"  value="Spremi podatke" />

            </form>
        </div>
    );
}

export default DataEdit;