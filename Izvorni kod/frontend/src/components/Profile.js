import React, { useEffect, useState } from "react";
import {Link} from 'react-router-dom';
import SearchBar from "./SearchBar";
import Calendar from "./Calendar";
import backend from "./util/constants";
import easyFetch from "./util/fetchUtility";
import userIcon from "./resursi/user-solid.svg";
import "./css/profile.css";
//import testDataNatjecatelj from "./resursi/MOCK_DATA_PROFIL_NATJECATELJ.json";
//import testDataVoditelj from "./resursi/MOCK_DATA_ZADATAK.json";


    /*kada proradi backend onda maknut voditeljData i natjecateljData, 
        i koristi userData, 
        gdje su podaci za korisnika koristiti userData.appUser.(sta vec treba)*/

function Profile(props) {

    const pathname = window.location.pathname;
    let username = pathname.substring(8);
    let [userData, setUserData] = useState({});
    //let natjecateljData = testDataNatjecatelj;
    //let voditeljData = testDataVoditelj;


    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }


    if(username === ""){
        username = props.user.userId;
    }

    useEffect(() => {
        easyFetch(`${backend}/userinfo/${username}`, "GET", null, headers)
            .then(res => {
                if(res.status === 400) {
                    console.log("error")
                }
                setUserData(res);
                //console.log(res);
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);
    


    //console.log(userData);

    //function recognizeUser(){
    //    if (userData.appUser.accounttype == 0){
    //        return renderNatjecatelj();
    //    }else if (userData.appUser.accounttype == 1 || userData.appUser.accounttype == 3){
    //        return renderVoditelj();
    //    }     }
        
    function recognizeUser(){
        //console.log("\t"+userData.username);
        if (userData.appUser !== undefined){
            if (userData.appUser.accounttype == 0){
                  return renderNatjecatelj();
              }else if (userData.appUser.accounttype == 1 || userData.appUser.accounttype == 3){
                  return renderVoditelj();
              }     
        }else {
            if (userData.accounttype == 0){
                return renderNatjecatelj();
            }else if (userData.accounttype == 1 || userData.accounttype == 3){
                return renderVoditelj();
            }
        }     }
     
    function renderNatjecatelj(){

        return <div className="CompetitorContainer">
            <p className="ProfileText CompetitorText">Natjecatelj je točno riješio <b>{userData.correctlySolvedTasks}</b> zadataka</p>
            <p className="ProfileText CompetitorText">Korisnik je isprobao <b>{userData.triedTasks}</b> zadataka</p>
            {renderPehari()}
        </div>
    }

    function renderPehari(){
        if (userData.trophyLinkList.length > 0){
            //console.log(natjecateljData.trophyLinkList);
            let trophies = [];

           for (const trophy of userData.trophyLinkList){
            trophies.push(<img src = {`data:image/jpeg;base64,${trophy}`} alt = "" className = "trophyImage" key={trophy}/>);
           }
        return <div>
            <p className="ProfileText CompetitorText">Ovo su njegovi pehari</p>
                {trophies}
            </div>
        } else return <p className="ProfileText CompetitorText">Korisnik još nije osvojio niti jedan pehar.</p>;
    }
    
 
    function renderVoditelj(){

        if (userData.appUser.accounttype == 3 ){
            return <h1>Ovaj voditelj još nije potvrđen!</h1>
        }else {
        return <div className="voditeljContainer">
            <h1>Korisnik je stvorio ova natjecanja:</h1>
            <Calendar username = {userData.appUser.username}></Calendar>
            {renderZadaci()}
            </div>
        }
    }
    

    function renderZadaci(){
       if (userData.taskList !== undefined){
           let tasks = [];
           userData.taskList.forEach((task)=>{
            tasks.push(<div className="taskItem" key={task.taskId}>
                            <a href={"/zadaci/"+task.taskId}>{task.taskId}: {task.title}</a>
                            <pre>{task.description}</pre>
                            <p><b>{lijepoIspisiBodove(task.mscore)}</b></p>
                            {dodajGumbe(task)}
                        </div>);
           });

       return <div className="tasksContainer">
           <h1>{tasks.length>0 ? "Korisnik je unio ove zadatke:" : "Korisnik nije unio zadatke"}</h1>
            {tasks}
           </div>
       }
    }

    function dodajGumbe(task){
        var loggedUser = sessionStorage.getItem("username");
        if (task.creator.username ===loggedUser){
            return (<div className="buttons">
                    <Link to={"/uredivanjeZadatka/"+task.taskId}><input type="button" className="register-btn button" value="Uredi zadatak" ></input></Link>
                    </div>
            );
        }
    }

    function lijepoIspisiBodove(score){
        if (score % 10 >=1 && score % 10 <=4 && (score > 20 || score < 10)){
            if (score < 10){
                return score + " boda";
            } else{
                return score + " bod";
            }
        } else{
            return score + " bodova";
        }
    }
    
    return(
        <div>
            <div className = "searchBar">
                <SearchBar/> 
            </div>
            <div className="cardContainer">
                <div className="card">
                    <img src={(userData.appUser && (userData.appUser.photo && `data:image/jpeg;base64,${userData.appUser.photo}`)) || userIcon} alt="" className="profilePic" />
                    <h1 className="username">{userData.appUser && userData.appUser.username}</h1>
                    <p className="ProfileText">{(userData.appUser && userData.appUser.name) + " " + (userData.appUser && userData.appUser.surname)}</p>

                    {userData.appUser && userData.appUser.accounttype === 0 ?
                        (<div className="roleUser">Natjecatelj</div>)
                        :
                        (<div className="roleAdmin">Voditelj</div>)}

                </div>
            </div>
            <div>
                {recognizeUser()}
            </div>
        </div>
    );
}

export default Profile;