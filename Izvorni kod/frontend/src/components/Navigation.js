import React from "react";
import { Link } from 'react-router-dom';
import userIcon from "./resursi/user-circle-regular.svg"; // Source - https://fontawesome.com/
import adminIcon from "./resursi/user-cog-solid.svg"; // Source - https://fontawesome.com/
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";

function Navigation(props) {

    const {user, setUsername} = props;

    function logout() {
        easyFetch(`${backend}/logout`, "GET")
            .catch(err => {console.log("No comunication with server")});
        sessionStorage.removeItem("username");
        setUsername("");
    }

    function drawLogout(){
        if(user.userId === undefined)
            return;
        
        return (
            <div className = "nav_user_menu">
                {natjecateljFunctions()}
                {VoditeljFunctions()}
                
                <Link to = "/" className = "nav_user_menu_item" onClick = {logout}>
                    <div>Logout</div>
                </Link>

            </div>
        );
    }

    function VoditeljFunctions(){
        if (user.accounttype == 1){
            let elements = [];
            elements.push(<Link to = "/zadaci" className = "nav_user_menu_item">
            <div>Zadaci</div>
            </Link>);
            elements.push(<Link to = {"/noviZadatak/" + user.username }className = "nav_user_menu_item">
            <div>Stvori zadatak</div> </Link>);
            elements.push(<Link to = {"/novoNatjecanje/" + user.username} className = "nav_user_menu_item">
                    <div>Stvori natjecanje</div>
                        </Link>);
            return elements;
        }else return "";
    }

    function natjecateljFunctions(){
        if (user.accounttype == 0){
            return <Link to = "/zadaci" className = "nav_user_menu_item">
            <div>Zadaci</div>
            </Link>;
        }else return "";
    }

    return(
        <div className = "nav">
            <Link to = "/kalendar" className = "nav_item">
                <div>Kalendar</div>
            </Link>
            <Link to = "/korisnici" className = "nav_item">
                <div>Korisnici</div>
            </Link>
            <div className = "nav_user">
                <Link to = {(user.username && ((user.accounttype===2 && "/admin") || ("/profil/" + user.username))) || "/login"} className = "nav_item">
                    <div className = "nav_user_inner">
                        <span className = "nav_user_text">{user.username || "Login"}</span>
                        <img src = {user.userId && ((user.accounttype===2 && adminIcon) || (user.photo || userIcon))} alt = "" className = "nav_user_image"/> 
                    </div>
                </Link>
                {drawLogout()}
            </div>


        </div>
    );
}

export default Navigation;