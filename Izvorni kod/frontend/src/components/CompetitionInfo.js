import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from 'react-router-dom';
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import { getDate, getTime } from "./util/dateTimeAnalyzer"
import "./css/competition.css";

function CompetitionInfo(props) {

    const {competition_id} = useParams();
    const today = new Date();
    const navigate = useNavigate();
    const user = props.user;
    const [creator, setCreator] = React.useState({});


    useEffect(()=>{
        easyFetch(`${backend}/exam/${competition_id}/tasks`, "GET", null, headers)
            .then( res => {
                 //setUsedTasks(res);
                 if(res[0].creator){
                     setCreator(res[0].creator.username);
                 }
                 //console.log(res[0].creator.username);
             })
             .catch( err => {
                 console.log("error");
             })
     }, []);

    // exam/:id vraca: examid, title, description, trophy, maxscore, datestart, duration, totaltasks (nije u tablici)
    // za sva natjecanja koja nisu virtualna
    
    const [competitionData, setCompetitionData] = useState({});
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    useEffect(() => {
        easyFetch(`${backend}/exam/${competition_id}`, "GET", null, headers)
            .then(res => {
                if(res.status === 400) {
                    console.log("error")
                }
                setCompetitionData(res);
                console.log(res)

                var godinaNatjecanja = parseInt(res.date.substring(0,4));
                var mjesecNatjecanja = parseInt(res.date.substring(5,7));
                var danNatjecanja = parseInt(res.date.substring(8,10));
    
                var danasnjaGodina = today.getFullYear();
                var danasnjiMjesec = (today.getMonth()+1);
                var danasnjiDan = today.getDate();
    
                console.log(godinaNatjecanja, mjesecNatjecanja, danNatjecanja)
                console.log(danasnjaGodina, danasnjiMjesec, danasnjiDan)
                
                var natjecanjeValue = godinaNatjecanja*10000+mjesecNatjecanja*100+danNatjecanja;
                var danasnjaValue = danasnjaGodina*10000+danasnjiMjesec*100+danasnjiDan;
                console.log(natjecanjeValue, danasnjaValue)
                if (natjecanjeValue === danasnjaValue){
                    var satiPocetka = parseInt(res.date.substring(11,13));
                    var minutePocetka = parseInt(res.date.substring(14,16))
                    var trenutniSati = today.getHours();
                    var trenutneMinute = today.getMinutes();
                    var trajeSati = parseInt(res.duration.substring(0,2));
                    console.log(res)
                    var trajeMinuta = parseInt(res.duration.substring(3,5));

                    console.log(satiPocetka, minutePocetka)
                    console.log(trenutniSati, trenutneMinute)
                    console.log(trajeSati, trajeMinuta)

                    var pocetakNatjecanja = satiPocetka*100+minutePocetka;
                    var krajNatjecanja = pocetakNatjecanja + trajeSati*100+trajeMinuta;
                    var trenutnoVrijeme = trenutniSati*100 + trenutneMinute;
                    console.log(pocetakNatjecanja, krajNatjecanja)
                    console.log(trenutnoVrijeme + "  " + krajNatjecanja)
                    if (trenutnoVrijeme > krajNatjecanja){
                        navigate(`/natjecanje/${competition_id}/info`)
                    }
                } else if(natjecanjeValue < danasnjaValue && !natjecanjeUTijeku()){
                    navigate(`/natjecanje/${competition_id}/info`)
                }
            })
    }, []);
   
    function natjecanjeUTijeku(){
        if (competitionData.date){
            //console.log(competitionData.date.substring(8,10));
            var godinaNatjecanja = parseInt(competitionData.date.substring(0,4));
            var mjesecNatjecanja = parseInt(competitionData.date.substring(5,7));
            var danNatjecanja = parseInt(competitionData.date.substring(8,10));

            var danasnjaGodina = today.getFullYear();
            var danasnjiMjesec = (today.getMonth()+1);
            var danasnjiDan = today.getDate();

            
            var natjecanjeValue = godinaNatjecanja*10000+mjesecNatjecanja*100+danNatjecanja;
            var danasnjaValue = danasnjaGodina*10000+danasnjiMjesec*100+danasnjiDan;
            
            if (natjecanjeValue === danasnjaValue){
                    var satiPocetka = parseInt(competitionData.date.substring(11,13));
                    var minutePocetka = parseInt(competitionData.date.substring(14,16))
                    var trenutniSati = today.getHours();
                    var trenutneMinute = today.getMinutes();
                    var trajeSati = parseInt(competitionData.duration.substring(0,2));
                    var trajeMinuta = parseInt(competitionData.duration.substring(3,5));

                    //stvaranje pocetka i kraja, te ako je aktivno natjecanje, moguće ga je započeti!
                    var pocetakNatjecanja = satiPocetka*100+minutePocetka;
                    var krajNatjecanja = pocetakNatjecanja + trajeSati*100+trajeMinuta;
                    var trenutnoVrijeme = trenutniSati*100 + trenutneMinute;

                    //console.log("Pocetak natjecanja "+pocetakNatjecanja+"\nkraj Natjecanja "+krajNatjecanja+"\ntrenutno Vrijeme "+trenutnoVrijeme );
                    if (trenutnoVrijeme >= pocetakNatjecanja && trenutnoVrijeme < krajNatjecanja){
                        return true;
                    }
                }
        }
    }

    function natjecanjeZapocelo(){
        if (competitionData.date){
            //console.log(competitionData.date.substring(8,10));
            var godinaNatjecanja = parseInt(competitionData.date.substring(0,4));
            var mjesecNatjecanja = parseInt(competitionData.date.substring(5,7));
            var danNatjecanja = parseInt(competitionData.date.substring(8,10));

            var danasnjaGodina = parseInt(today.getFullYear());
            var danasnjiMjesec = parseInt(today.getMonth()+1);
            var danasnjiDan = parseInt(today.getDate());

            var natjecanjeValue = godinaNatjecanja*10000+mjesecNatjecanja*100+danNatjecanja;
            var danasnjaValue = danasnjaGodina*10000+danasnjiMjesec*100+danasnjiDan;

            if (natjecanjeValue === danasnjaValue){
               
                    var satiPocetka = parseInt(competitionData.date.substring(11,13));
                    var minutePocetka = parseInt(competitionData.date.substring(14,16))
                    var trenutniSati = today.getHours();
                    var trenutneMinute = today.getMinutes();

                    //stvaranje pocetka i kraja, te ako je aktivno natjecanje, moguće ga je započeti!
                    var pocetakNatjecanja = satiPocetka*100+minutePocetka;
                    var trenutnoVrijeme = trenutniSati*100 + trenutneMinute;

                    //console.log("Pocetak natjecanja "+pocetakNatjecanja+"\nkraj Natjecanja "+krajNatjecanja+"\ntrenutno Vrijeme "+trenutnoVrijeme );
                    if (trenutnoVrijeme < pocetakNatjecanja){
                        return true;
                    }
            }else{
                if (natjecanjeValue > danasnjaValue)
                    return true;
            }
        }
    }

    function mozeZapocetNatjecanje(){
       if (natjecanjeUTijeku()){
            if(user.accounttype === 0)  
                return <Link to= { '/natjecanje/' + competition_id + '/zadaci/1'} className = "acceptButton"><div>Započni natjecanje</div></Link>;
       }else {
           return <div><h1>Natjecanje nije u tijeku!</h1></div>
       }
    }

    function natjecanjeSeMozeUredivati(){
        if (natjecanjeZapocelo() && user.accounttype === 1 && (creator === user.username))
            return <Link to= { '/uredivanjeNatjecanja/' + competition_id} className = "acceptButton grayColoured"><div>Uredi natjecanje</div></Link>
    }

    return(
        <div>
            <h1 className="competitionTitle">{competitionData.title}</h1>
            <div className="competitionInfo">
                <p>
                <i>Kratki opis: </i> {competitionData.description}<br/>
                <i>Broj zadataka: </i> {competitionData.totalTasks}<br/>
                <i>Maksimalno bodova: </i> {competitionData.maxScore}<br/>
                <i>Početak natjecanja: </i> {getDate(competitionData.date) + " " + getTime(competitionData.date)}<br/>
                <i>Trajanje: </i> {competitionData.duration}<br/>
                </p>
            </div>
            {competitionData.trophy ? 
                        <div className = "trophyContainer">
                <img src = {`data:image/jpeg;base64,${competitionData.trophy}`} alt = "" className = "trophyImage"/>
            </div>
            :
            <span></span>
            }

            <div>
                {mozeZapocetNatjecanje()}
                {natjecanjeSeMozeUredivati()}
            </div>
        </div>
    );
}

export default CompetitionInfo;