import React from "react";
import { Link } from 'react-router-dom';
import logo from "./resursi/logo.png";

function LogoImage() {
    return(
            <Link to = "/" className = "logo">
                <img src = {logo} alt = "" className = "logo_image"/>
                <span className = "logo_text">CodeShark</span>
            </Link>
            
    );
}

export default LogoImage;