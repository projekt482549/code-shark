import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import CalendarItem from "./CalendarItem";
import CalendarTimeMap from "./CalendarTimeMap";
import easyFetch from "./util/fetchUtility";
import { getDate, getMonthName, getMonthNumberOfDays, formatDate } from "./util/dateTimeAnalyzer";
import backend from "./util/constants";
import "./css/calendar.css";

function Calendar(props) {
    const navigate = useNavigate();

    const [year, setYear] = useState();
    const [month, setMonth] = useState();
    const [timeMapData, setTimeMapData] = useState();
    const [eventList, setEventList] = useState([]);

    if (props.username){
        var username = props.username;
    }

    useEffect(() => {
        if(!sessionStorage.getItem("username")){
            navigate("/login");
        }
    }, []);

    useEffect(() => {
        let now = new Date();
        if(year === undefined)
            setYear(now.getFullYear());
        if(month === undefined)
            setMonth(now.getMonth());
    }, []);

    useEffect(() => {
        easyFetch(`${backend}/exams/${year}/${month + 1}`)
            .then(res => {
                if(res.status === undefined){
                    let data;
                    if(username !== undefined) {
                        data = res.filter(
                            (element) => {
                                return element.creator.username === username;
                            });
                    }else
                        data = res;
                    data.sort((f, s) => {
                        if(f.date < s.date)
                            return -1
                        else if(f.date > s.date) 
                            return 1;
                        else
                            return 0;
                    });
                    setEventList(data);
                }
            })
            .catch(err => {console.log("No comunication with server")});
    }, [month, year]);

    //Funcija koja definira sve elemente kalendara i stavlja ih u odgovarajucem redoslijedu u grid
    function drawCalendar(year, month) {

        let elements = [];

        let now = new Date();
        let date = new Date(year, month, 1);
        let day = date.getDay();

        //Prilagodba dana obliku koji nam pase (pon = 0, uto = 1...)
        day = day - 1;
        if (day === -1) {
            day = 6;
        }

        //Provjeravamo broj dana u mjesecu "rucno" i o kojem mjesecu je rijec
        let numberOfDays = getMonthNumberOfDays(month + 1, year);
        let calendarMonth = getMonthName(month + 1);

        //Postavi godinu i mjesec
        let calendarYearMonth = <CalendarItem year = {year} month = {calendarMonth} key = {1}/>;

        //Dodaj godinu i mjesec u output
        elements.push(<CalendarItem special = "<" moveMonth = {moveMonth} key = {0}/>);
        elements.push(calendarYearMonth);
        elements.push(<CalendarItem special = ">" moveMonth = {moveMonth} key = {2}/>);

        //Dodaj nazive dana u output
        elements.push(<CalendarItem dayName = "Pon" key = {3}/>);
        elements.push(<CalendarItem dayName = "Uto" key = {4}/>);
        elements.push(<CalendarItem dayName = "Sri" key = {5}/>);
        elements.push(<CalendarItem dayName = "Čet" key = {6}/>);
        elements.push(<CalendarItem dayName = "Pet" key = {7}/>);
        elements.push(<CalendarItem dayName = "Sub" key = {8}/>);
        elements.push(<CalendarItem dayName = "Ned" key = {9}/>);

        //Dodaj praznine kako bi dani bili poravnati
        for (let i = 0; i < day; i++) {
            elements.push(<CalendarItem empty = {true} key = {10 + i}/>);
        }

        let isCurrentYear = now.getFullYear() === year;
        let isCurrentMonth = now.getMonth() === month;

        let j = 0;
        let eventDate;
        let currentDate;
        //Dodaj dane
        for (let i = 0; i < numberOfDays; i++) {
            let events = [];
            if(j < eventList.length){
                currentDate = formatDate(year + "-" + (month + 1) + "-" + (i + 1), true);
                eventDate = getDate(eventList.at(j).date);
            }
            while(j < eventList.length && eventDate === currentDate) {
                events.push(eventList.at(j));
                j++;
                if(j < eventList.length)
                    eventDate = getDate(eventList.at(j).date);
            }
            elements.push(<CalendarItem day = {i + 1}
                weekend = {(day + i) % 7 === 5 || (day + i) % 7 === 6}
                current = {isCurrentYear && isCurrentMonth && (now.getDate() === i + 1)}
                events = {events}
                changeTimeMapData = {changeTimeMapData}
                key = {10 + day + i}/>);
        }

        //Dodaj praznine na kraj
        for (let i = 0; i < 42 - numberOfDays - day; i++) {
            elements.push(<CalendarItem empty = {true} key = {10 + day + numberOfDays + i}/>);
        }

        //Vrati popis svih elemenata kalendara
        return elements;
    }

    //Promjena mjeseca (mora biti rijesena funkcijski radi mogucnosti promjene godine), takoder se omogucava pomicanje za vise mjeseci
    function moveMonth(amount) {
        return function() {
            if ((month + amount) >= 12) {
                setYear(year + Math.floor((month + amount) / 11));
                setMonth(month - Math.floor((month + amount) / 12) * 11);
            }
            else if ((month + amount) <= -1) {
                setYear(year + Math.floor((month + amount) / 11));
                setMonth(month - Math.floor((month + amount) / 12) * 11);
            }
            else
                setMonth(month + amount);
            setTimeMapData(undefined);
        }
    }

    //Funkcija za prikaz vremenske tablice dogadaja za pojedini dan, ako nema podataka onda tablica nije prikazana
    function drawTimeMap(data) {
        if(data === undefined)
            return ;
        else
            return <CalendarTimeMap data = {data} changeTimeMapData = {changeTimeMapData} month = {month}/>;
    }

    //Funkcija se poziva pritiskom na kucicu u kalendaru i postavlja podatke o dogadajima iz te kucice u vremensku tablicu
    function changeTimeMapData(data) {
        return function() {
            setTimeMapData(data);
        }
    }

    return(
        <div className = "calendar_container">
            <div className = "calendar">
                {drawCalendar(year, month)}
            </div>
            <div className = "calendar_time_map_root">
                {drawTimeMap(timeMapData)}
            </div>
        </div>
    );
}

export default Calendar;