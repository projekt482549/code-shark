import React  from "react";
import { useNavigate } from 'react-router-dom';
import TasksResultsNavigation from './TasksResultsNavigation';
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/competition.css";

function ResultsRegular(props) {

    const results = props.results
    const competition_id = props.competition_id
    const totalTasks = props.totalTasks
    const navigate = useNavigate();
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    function handleClick(e) {
        e.preventDefault();
        return (
            easyFetch(`${backend}/exam/${competition_id}/newVirtual`, "GET", null, headers) // /exam/{examId}/newVirtual
                .then(res => {
                    if(res.status === 401) {
                        //error
                    } else{
                        navigate(`/natjecanje/${res.examId}`)
                    }
                })
                .catch(err => {console.log("No comunication with server")})
        )
    }
    function listUsers(results){
        var res = [];
        if(results === undefined)
            return;
        for (var i = 0; i < results.length; i++) {
            res.push(
                    <div className="userResult"> <b>{(i+1)}</b> {results[i].username} </div>
            );
        }
        return res;
    }
   
    return(
        <>
        <div className="usersRankingContainer">
            <div><b>Rang lista natjecatelja:</b></div>
            <div className="usersRanking">{listUsers(results)}</div>
        </div>
        <div className="taskDetailsContainer">
            <b>Detalji o pojedinom zadatku:</b>
        </div>
        <div>
            <TasksResultsNavigation className ="postExamNavigation" competition_id = {competition_id} totalTasks = {totalTasks} />
        </div>
        <div>
            <button className = "virtualCompetitionButton" onClick = {handleClick}>Stvori virtualno natjecanje</button>
        </div>
        </>
    );
}

export default ResultsRegular;