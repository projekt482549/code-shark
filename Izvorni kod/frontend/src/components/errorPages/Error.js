import React from "react";

function Error(props) {

    const {id} = props;

    let idText, description;

    switch(id){
        case "400":
            idText = "Bad Request";
            description = "Poslali ste neispravan zahtjev";
            break;
        case "401":
            idText = "Unauthorized";
            description = "Nemate ovlasti pristupiti traženim resursima";
            break;
        case "403":
            idText = "Forbidden";
            description = "Pokušali ste pristupiti stranici za koju nemate ovlasti";
            break;
        case "404":
            idText = "Not Found";
            description = "Tražena stranica nije pronađena";
            break;
        case "500":
            idText = "Server Error";
            description = "Pogreška na serveru";
            break;
        case "501":
            idText = "Not Implemented";
            description = "Tražena radnja još nije implementirana";
            break;
        default:
            idText = "Unknown";
            description = "Nepoznata greška";
    }

    return(
        <div className = "error">
            <h1>Error {id}</h1>
            <h2>{idText}</h2>
            <span>{description}</span>
        </div>
    );
}

export default Error;