import React from "react";
import { Link } from "react-router-dom";

function CalendarEvent(props) {

    let {event} = props;

    //Jedan dogadaj koji se smjesta u kucicu
    return(
        <Link to={`/natjecanje/${event.examId}`} className="calendar_event" style={{"--backgroundColor": event.backgroundColor, "--color": event.fontColor}}>
            <span className="calendar_event_name">{event.title}</span>
            <span className="calendar_event_organizer">{event.creator.username}</span>
        </Link>
    );
}

export default CalendarEvent;