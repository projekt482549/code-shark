import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/newTask.css";


function TaskDataEdit() {
    const navigate = useNavigate();
    const {username} = useParams();
    const pathname = window.location.pathname;
    let taskId = pathname.substring(18);

    const[form, setForm] = React.useState({nameTask: '', textTask: '', points: 0, threshold: '', private: 0, primjeri: ''});
    const[error, setError] = React.useState('');
    const [points, setPoints] = React.useState(1);
    const[taskData, setTaskData] = React.useState({});

    const listIn = [];
    const listOut = [];

    useEffect(() => {
        easyFetch(`${backend}/task${taskId}/taskInfo`, "GET")
            .then(res => {
                if(res.status === 401) {
                    console.log("error")
                    return;
                }
                setTaskData(res);
                form.primjeri = res.primjeri;
                form.nameTask= res.nameTask;
                form.textTask= res.textTask;
                form.points = res.points;
                form.threshold =  res.threshold;
                form.private = res.privatni;
 
    
                if (res.primjeri){
                let i = 0;
                    res.primjeri.forEach(
                        (primjer) =>{
                            //console.log(" ulaz = "+primjer.ulaz+" izlaz = "+primjer.izlaz);
                            listIn[i] = primjer.ulaz;
                            listOut[i] = primjer.izlaz;
                            //console.log("LISTE HEH  ulaz = "+listIn[i]+" izlaz = "+listOut[i]);
                            i++;
                        });
                }
                               
                setPoints(res.points);
                setForm(form);
                //console.log(form);
                return;
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);


    function onChange(event) {
        const {name, value} = event.target;
        setForm(oldForm => ({...oldForm, [name]: value}));
        return;
    }

    function onChangeCheckBox(e) {
        if(e.target.checked) {
            setForm(oldForm => ({...oldForm, private: 1}));
        } else {
            setForm(oldForm => ({...oldForm, private: 0}));
        }
        return;
    }

    function onChangePoints(e) {
        var value = e.target.value;
        setPoints(value);
        setForm(oldForm => ({...oldForm, points: value}));
        return;
    }

    function onChangeIn(e, i) {
        listIn[i] = e.target.value;
        return;
    }

    function onChangeOut(e, i) {
        listOut[i] = e.target.value;
        return;
    }

    function onSubmit(e) {
        e.preventDefault();
        setError("");

        var listExamples = [];
        for(var i = 0; i < form.points; i++) {
            if (!listIn[i] && form.primjeri[i]){
                listIn[i] = form.primjeri[i].ulaz;
            }
            if (!listOut[i] && form.primjeri[i]){
                listOut[i] = form.primjeri[i].izlaz;
            }
            listExamples.push({
                ulaz: listIn[i],
                izlaz: listOut[i]
            });
        }


        //nije elegantno, ali radi...ako se nade vremena trebalo bi popravit!
        //kao u dataEdit
        if (form.nameTask === null)
            form.nameTask = taskData.nameTask;

        if (form.textTask === null)
            form.textTask = taskData.textTask;
        
        if (form.points === null)
            form.points = taskData.points;
        
        if (form.threshold === null)
            form.threshold = taskData.threshold;
        
        if (form.private === null)
            form.private = taskData.private;
        //ovo, jel

        const data = {
            'nameTask': form.nameTask,
            'textTask': form.textTask,
            'points': form.points,
            'threshold': form.threshold,
            'privatni': form.private,
            'primjeri': listExamples,
        }


        const headers = {
            'Content-Type': 'application/json'
        }

        return(
            easyFetch(`${backend}/task${taskId}/update`, "POST", JSON.stringify(data), headers)
                .then(res => {
                    switch (res.message){
                        case 'ok':
                            navigate('/zadaci/'+taskId.substring(1));
                            break;
                        case 'forbidden':
                            alert("Nemate ovlasti za mijenjanje zadatka!");
                            break;
                        default:
                            alert("Dogodila se neočeivana pogreška!");
                    }
                })
                .catch(err => {console.log("No comunication with server")})
        );
    }



    function ispisiUlaz(i){

        if (form.primjeri[i] && form.primjeri[i].ulaz){
            return form.primjeri[i].ulaz;
        }
    }

    function ispisiIzlaz(i){
        if (form.primjeri[i] && form.primjeri[i].izlaz){
            return form.primjeri[i].izlaz;
        }
    }


//iz nekog razloga kod bodova zeza
    return(
        <div className="container-task">
            <h2>Uredi zadatak</h2>
            <form onSubmit={onSubmit}>
                    <div>
                        <label htmlFor="nameTask"/>
                        <input type="text" id="nameTask"placeholder="Naziv zadatka" name="nameTask" onChange={onChange} defaultValue = {form.nameTask} required/>

                        <label htmlFor="textTask"/>
                        <textarea id="textTask" placeholder="Tekst zadatka" name="textTask" onChange={onChange} defaultValue = {form.textTask} required rows={5}/>

                        <div className="left row">
                            <div className="row-item">
                                <label>Broj bodova: </label>
                                <input type="number" name="points" min={1}  onChange={onChangePoints} defaultValue={taskData.points} required/>
                            </div>
                            <div className="row-item">
                                <label>Vrijeme izvođenja (ms): </label>
                                <input type="number" name="threadshold" min={0} onChange={onChange} defaultValue = {form.threshold} required step="any"/>
                            </div>
                            <div className="row-item">
                                <label><input type="checkbox" name="private" onChange={onChangeCheckBox} checked = {form.private? true:false} />Privatan zadatak</label>
                            </div>

                        </div>
                    </div>
                    <div className="left">
                        <div>
                            <label>Primjeri za evaluaciju: </label>
                        </div>
                        {Array.from(Array(Math.round(Number(points))), (e, i ) => {
                            return <div className="left" key={i}>
                                <div>
                                    <label>Primjer {i+1}.</label>
                                </div>
                                <div>
                                    <textarea id="in" placeholder="Ulaz" name="in"
                                            onChange={(e) => {onChangeIn(e, i);}}
                                            required rows={5} defaultValue={ispisiUlaz(i)} />
                                    <textarea id="out" placeholder="Izlaz" name="out"
                                            onChange={(e) => {onChangeOut(e, i);}}
                                            required rows={5} defaultValue={ispisiIzlaz(i)}/>
                                    </div>
                            </div>
                        })}

                    </div>

                <div>
                    <input className="submit-btn" id="submit" type="submit"  value="Spremi Promjene" onSubmit={onSubmit}/>
                </div>
            </form>
        </div>
    );
}

export default TaskDataEdit;