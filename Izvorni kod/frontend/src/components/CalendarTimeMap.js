import React from "react";
import { Link } from "react-router-dom";
import { getDay, getMonthName, getTime, getTimeAsDecimal, addTimes, formatTime } from "./util/dateTimeAnalyzer"
import "./css/calendarTimeMap.css";

function CalendarTimeMap(props) {

    const {data, month} = props;

    let date;
    let timeSlotColor;
    let eventStart;
    let eventDuration;
    let eventStartHour;
    let elements = [];
    let j = 0;

    if(data.day !== undefined) {
        date = data.day + ". " + getMonthName(month + 1);
    } else {
        date = getDay(data[0].date) + ". " + getMonthName(month + 1);
    }
    //Dodaj naslov vremenske tablice u obliku "dan. mjesec" i dodaj gumb za zatvaranje
    elements.push(
        <div className = "calendat_time_map_title" key = {-1}>
            <h4>{date}</h4>
            <div className = "calendat_time_map_exit" onClick = {props.changeTimeMapData(undefined)}>X</div>
        </div>);

    //Kreiraj vremensku tablicu sat po sat
    for (let i = 0; i < 24; i++) {
        if(i % 2 == 0) 
            timeSlotColor = "rgba(0, 0, 0, 0.05)";
        else
            timeSlotColor = "rgb(255, 255, 255)";
        //Dodaj vrijeme u vremenski slot
        elements.push(<div className = "calendat_time_map_hours" style = {{"--backgroundColor": timeSlotColor}} key = {i}><h5>{i}</h5></div>);

        if(data.length > j) {
            eventStartHour = getTimeAsDecimal(data.at(j).date, true);
            eventDuration = getTimeAsDecimal(data.at(j).duration, true);
            eventStart = getTime(data.at(j).date);
        }

        //Dodaj polje uz vremenski slot, ako se neki dogadaj odvija u tom slotu dodaj dogadaj sa odgovarajucim trajanjem i pocetkom
        if(data.length > j && eventStartHour >= i && eventStartHour < i + 1) {
            elements.push(
                <div className = "calendat_time_map_slot calendat_time_map_slot_even" style = {{"--backgroundColor": timeSlotColor}} key = {24 + i}>
                    <Link to = {`/natjecanje/${data.at(j).examId}`} className = "calendat_time_map_event" 
                        style = {{
                            //odmak od vrha vremenskog skota
                            "--top": eventStartHour % 1 * 100 + "%", 
                            //odgovarajuca visina 100% je jedan sat
                            "--height": eventDuration * 100 + "%", 
                            //nasumicno dodjeljena boja kako bi izgledalo zivlje
                            "--backgroundColor": data.at(j).backgroundColor,
                            //boja teksta kako bi bio vidljiv
                            "--color":  data.at(j).fontColor}}>
                        <h5>{data.at(j).title} <span>({formatTime(eventStart, true, true, false, true)} - {formatTime(addTimes(eventStart, data.at(j).duration), true, true, false, true)})</span></h5>
                        <h6>{data.at(j).creator.username}</h6>
                        <span>{data.at(j).description}</span>
                    </Link>
                </div>);
                j++;
        } else
            elements.push(<div className = "calendat_time_map_slot" style = {{"--backgroundColor": timeSlotColor}} key = {24 + i}></div>);
    }

    return(
        <div className="calendar_time_map">
            {elements}
        </div>
    );
}

export default CalendarTimeMap;