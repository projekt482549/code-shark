import React, { useEffect, useState } from "react";
import PendingRequest from "./PendingRequest";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/request.css";

function Admin() {
    const [users, setUsers] = useState([]);

    useEffect(()=>{
        easyFetch(`${backend}/admin`)
            .then( res => {
                setUsers(res);
             })
            .catch( err => {
                console.log(err);
            })
     }, []);

    return(
    <div>
        <h1 className ="title"> Pregled zahtjeva za registraciju voditelja</h1>
        <div className="requestsContainer">
            {
                users.length !== 0
                 && users
                    .filter((val) => { 
                        return val.enabled === true})
                    .map((val, key) => {
                        return <PendingRequest user = {val} key = {key}/>
                })
            } 
        </div>
    </div>
    );
}

export default Admin;