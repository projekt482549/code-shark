import React, { useEffect } from "react";
import { Link } from 'react-router-dom';
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
//import testData from "./resursi/MOCK_DATA_ZADATAK.json";

function Tasks(props) {
    //let zadaciData = testData;

    const [tasksData, setTasksData] = React.useState([]);
    const user = props.user;


    useEffect(() => {
        easyFetch(`${backend}/task`, "GET")
            .then(res => {
                if(res.status === 404) {
                    console.log("error");
                    console.log(tasksData);
                }else {
                setTasksData(res);
                //console.log("probao sam dohvatit ovo "+tasksData.forEach);
                }
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);


    function renderZadaci(){
       if (tasksData !== undefined){
           let tasks = [];
           tasksData.forEach((task)=>{
            tasks.push(<div className="taskItem" key={task.taskId}>
                            <Link to={"/zadaci/"+task.taskId} state={{title: task.title, description: task.description, mscore: task.mscore}}>{task.taskId}: {task.title}</Link>
                            <pre>{task.description}</pre>
                            <p><b>{lijepoIspisiBodove(task.mscore)}</b></p>
                        {dodajGumbe(task)}
                        </div>
                        );
           });

       return <div className="tasksContainer">
            {tasks}
           </div>
       }
    }

    function lijepoIspisiBodove(score){
       if (score % 10 >=1 && score % 10 <=4 && (score > 20 || score < 10)){
           if (score < 10){
               return score + " boda";
           } else{
               return score + " bod";
           }
       } else{
           return score + " bodova";
       }
    }

    function dodajGumbe(task){
        if (task.creator.username === user.username && (user.accounttype === 1 || user.accounttype === 2)){
            return (<div className="buttons">
            <Link to={"/uredivanjeZadatka/"+task.taskId}><input type="button" className="register-btn button" value="Uredi zadatak" ></input></Link>
            </div>
            );
        }
    }

    return(
        <div>
            <h1>Zadaci:</h1>
            <p>Zadatak koji želite rješavati odaberite klikom na njegov naziv</p>
            {renderZadaci()}
        </div>
    );
}

export default Tasks;