import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router";
import {TextField} from "@material-ui/core";
import easyFetch from "./util/fetchUtility";
import backend from "./util/constants";
import "./css/newCompetition.css";


function CompetitionDataEdit(props) {
    const navigate = useNavigate();
    //const { username } = useParams();
    const user = props.user;
    const username = sessionStorage.getItem("username");
    const pathname = window.location.pathname;
    const competition_id = pathname.substring(22);

    const[form, setForm] = React.useState({name: null, description: null, date: null, startTime: null, duration: null, cup: null, tasks: null});
    const[error, setError] = React.useState('');
    const [tasks, setTasks] = useState([]);
    const [usedTasks, setUsedTasks] = useState([]);
    const [competitionData, setCompetitionData] = React.useState({});
    const [compDate, setCompDate] = React.useState({});

    const today = new Date()
    const dateRef = React.useRef();
    const startTimeRef = React.useRef();
    const endTimeRef = React.useRef();

    const listOfTasks = [];

    const headers = {
        'Content-Type': 'application/json'
    }

    useEffect(() => {
        easyFetch(`${backend}/exam/${competition_id}`, "GET", null, headers)
            .then(res => {
                if(res.status === 400) {
                    console.log("error")
                }
                //setCompetitionData(res);
                let compDate = res.date.substring(0,4)+'-'+( res.date.substring(5,7))+'-'+ res.date.substring(8,10);
                form.name = res.title;
                form.description = res.description;
                form.date = compDate;
                form.startTime = res.date.substring(11,16);
                //form.cup = res.trophy;
                form.duration = res.duration;

                setForm(form);
                setCompetitionData(res);
                setCompDate(compDate);
                //console.log(form);
            })
            .catch(err => {console.log("No comunication with server")});
    }, []);

    useEffect(()=>{
           easyFetch(`${backend}/task/${username}`, "GET", null, headers)
               .then( res => {
                    setTasks(res);
                })
                .catch( err => {
                    console.log("error");
                })
        }, []);

    useEffect(()=>{
        easyFetch(`${backend}/exam/${competition_id}/tasks`, "GET", null, headers)
            .then( res => {
                 setUsedTasks(res);
                 //console.log(res);
             })
             .catch( err => {
                 console.log("error");
             })
     }, []);


    function onChange(event) {
        const {name, value} = event.target;
        setForm(oldForm => ({...oldForm, [name]: value}));
    }

    function onChangeCheckBox(e) {
        if(e.target.checked) {
            listOfTasks.push(e.target.value)
        } else {
            var index = listOfTasks.indexOf(e.target.value);
            if (index > -1) {
                listOfTasks.splice(index, 1);
            }

        }
    }

    function readPhoto(e) {
        let input = e.target.files;
        let reader = new FileReader();

        reader.readAsArrayBuffer(input[0]);
        reader.onload = (e) => {
            let bytes = new Uint8Array(e.target.result);
            setForm(oldForm => ({...oldForm, cup: bytes}));
        }
    }


    function onSubmit(e) {
        e.preventDefault();
        setError("");

        if (form.name === null)
            form.name = competitionData.title;
        if (form.description)
          form.description = competitionData.description;
        if (form.date === null)
          form.date = compDate;
        if (form.startTime === null)
          form.startTime = competitionData.date.substring(11,16);
        //if (form.cup === null )
        //  form.cup = competitionData.trophy;
        if (form.duration === null)
            form.duration = competitionData.duration;

        var [hh1, mm1] = startTimeRef.current.value.toString().split(":");
        var [hh2, mm2] = endTimeRef.current.value.toString().split(":");
        var start = parseInt(hh1*60) + parseInt(mm1);
        var end = parseInt(hh2)*60 + parseInt(mm2);
        var dur = (end - start)/60
        var hh = parseInt(dur);
        var mm = Math.round((dur - hh)*60);
        if(hh.toString().length == 1) {
            hh = "0" + hh;
        }
        if(mm.toString().length == 1) {
            mm = "0" + mm;
        }
        var duration= hh+":"+mm+":00";

        const data = {
            'name': form.name,
            'description': form.description,
            'date': dateRef.current.value,
            'startTime': startTimeRef.current.value,
            'duration': duration,
            'cup': form.cup,
            'tasks': listOfTasks
        }


        var data2 = JSON.stringify(data, function(k, v) {
            if (v instanceof  Uint8Array) {
                return JSON.stringify(Array.apply([], v));
            }
            return v;
        });

        return(
            easyFetch(`${backend}/exam/${competition_id}`, "POST", data2, headers)
                .then(res => {
                    switch (res.message){
                        case 'ok':
                            navigate('/natjecanje/'+competition_id);
                            break;
                        case 'forbidden':
                            alert("Nemate ovlasti za mijenjanje zadatka!");
                            break;
                        default:
                            alert("Dogodila se neočeivana pogreška!");
                    }
                })
                .catch(err => {console.log("No comunication with server")})
        );

    }

    function getStartTime(){
        if (competitionData.date){
            //console.log(competitionData);
            //console.log(competitionData.date.substring(11,16));
            return competitionData.date.substring(11,16);
        } 
    }

    function iscrtajStartTime(){
        if (form.date)
        return <div className="left-align">
                                <label>Početak natjecanja: </label>
                                <TextField
                                    inputRef={startTimeRef}
                                    id="startTime"
                                    name="startTime"
                                    type="time"
                                   // defaultValue={"08:00"}
                                    defaultValue={getStartTime(form)}
                                    required

                                />
                </div>
    }

    function iscrtajDatum(){
        if (form.date){
           return          <div className="left-align">
                                <label>Datum natjecanja: </label>
                                <TextField
                                    inputRef={dateRef}
                                    id="date"
                                    name="date"
                                    type="date"
                                    defaultValue={form.date}
                                    inputProps={{
                                        min: today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(),
                                    }}
                                    required
                                />
                            </div>
        }
    }

    function iscrtajEndTime(){
        if (competitionData.date){
            var pocetak = competitionData.date.substring(11,16);
            var trajanje = competitionData.duration.substring(0,5);

            var [hh1, mm1] = pocetak.split(":");
            var [hh2, mm2] = trajanje.split(":"); 

            var minute = parseInt(mm1)+parseInt(mm2);
            var sati = parseInt(hh1)+ parseInt(hh2);
            
            if (minute >= 60){
                minute -= 60;
                sati++;
            }

            //console.log("pocetak = "+ pocetak+ " trajanje= "+trajanje);
            //console.log("skupa = "+ sati +":"+minute);


            return <div className="left-align">
                        <label>Kraj natjecanja: </label>
                        <TextField
                            inputRef={endTimeRef}
                            id="endTime"
                            name="endTime"
                            type="time"
                            defaultValue={izracunajEndTime(sati, minute)}
                            required
                        />
                    </div>
        }
    }

    function izracunajEndTime(sati, minute){
        var vracenString = "";
        if (sati > 10 && minute > 10){
            vracenString = sati + ":" +minute;
        } else 
            if ( sati > 10 && minute < 10){
                vracenString = sati + ":0" + minute;
            } else 
                if ( sati < 10 && minute > 10){
                    vracenString = "0"+sati+":"+minute;
            }else vracenString = "0"+sati+":0"+minute;
        
            return vracenString;
    }

    //ovo sa usedTasks[0] nije najelegantnije, ali u pravilu bi trebalo radit jer nema smisla imati natjecanje bez zadataka
    function iscrtajZadatke(usedTasks){
        let elements = [];
        if (tasks && usedTasks[0]){
            listOfTasks.push();
        tasks.forEach((task)=>{
            if (usedTasks.some(usedTasks => usedTasks.taskId === task.taskId)){
                listOfTasks.push(task.taskId);
            }
            elements.push(<label key={task.taskId}><input type="checkbox" name="task" onChange={onChangeCheckBox} 
                    value={task.taskId} defaultChecked = {usedTasks.some(usedTasks => usedTasks.taskId === task.taskId)}/>{task.title}<br/></label>);});
        }

        return <div className="flex-item">
            <fieldset>
                <legend>Zadaci</legend>
                    {elements}
                </fieldset>
            </div>
    }

    return(
            <div className="container-competition">
                <h2>Novo natjecanje</h2>
                <form onSubmit={onSubmit}>
                    <div className="flex-container">
                        <div>
                            <label htmlFor="name"/>
                            <input type="text" id="competitionName" placeholder="Naziv natjecanja" name="name" onChange={onChange} defaultValue={form.name} required/>

                            <label htmlFor="description"/>
                            <textarea id="description" placeholder="Opis natjecanja" name="description" onChange={onChange} defaultValue={form.description} required rows={3}/>

                            {iscrtajDatum()}
                            
                            {iscrtajStartTime()}

                            {iscrtajEndTime()}

                            <div className="left-align">
                                <label>Slika pehara: </label>
                                <input type="file" name="picture" onChange={readPhoto} accept="image/*" />
                            </div>
                        </div>

                        {iscrtajZadatke(usedTasks)}
                        
                    </div>

                    <div>
                        <input className="submit-btn" id="submit" type="submit"  value="Potvrdi" onSubmit={onSubmit}/>
                    </div>
                </form>

            </div>

    );
}

export default CompetitionDataEdit;