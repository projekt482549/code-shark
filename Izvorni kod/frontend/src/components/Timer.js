import React, { useEffect, useState } from "react"
import { getSeconds, getHours, getMinutes } from "./util/dateTimeAnalyzer"
import "./css/timer.css";

function Timer(props) {

    const calculateTimeLeft = () => {

        const duration = props.duration;
        var seconds = (getHours(duration)-1) * 60 * 60 + getMinutes(duration) * 60 + getSeconds(duration);
        var dateStart = new Date(props.datestart);
        var dateEnd = new Date(dateStart.getTime() + seconds * 1000)
        var difference = dateEnd - new Date().getTime()

        // if(difference < 0){
        //     console.log("haha")
        // }
        let timeLeft = {};
        if (difference >= 0) {
            timeLeft = {
                days: Math.floor(difference / (1000 * 60 * 60 * 24)),
                h: Math.floor((difference / (1000 * 60 * 60)) % 24),
                min: Math.floor((difference / 1000 / 60) % 60),
                s: Math.floor((difference / 1000) % 60)
            };
        }
        return timeLeft;
      }

    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

    useEffect(() => {
        const timer = setTimeout(() => {
          setTimeLeft(calculateTimeLeft());
        }, 1000);
        return () => clearTimeout(timer);
      });

    const timerComponents = [];

    Object.keys(timeLeft).forEach((interval) => {
        if ((interval === "days" || interval === "hours") && !timeLeft[interval]) 
          return;
        timerComponents.push(
            <div className="timerCount">
                {interval !== "days" && timeLeft[interval] < 10 ? "0" + timeLeft[interval] : timeLeft[interval]} 
                <div className = "timerInterval">{interval}</div>
            </div>
        );
    });

    return(
        <div className = "timerContainer">
            {timerComponents.length ? timerComponents : <span></span>}
        </div>
    )
}

export default Timer;