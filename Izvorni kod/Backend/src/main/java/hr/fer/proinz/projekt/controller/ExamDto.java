package hr.fer.proinz.projekt.controller;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ExamDto {

    @NotNull
    @NotEmpty
    private Long examId;

    @NotNull
    @NotEmpty
    private  String title;

    @NotNull
    @NotEmpty
    private String description;

    @NotNull
    @NotEmpty
    private  byte[] trophy;

    @NotNull
    @NotEmpty
    private Integer maxScore;

    @NotNull
    @NotEmpty
    private LocalDateTime date;

    @NotNull
    @NotEmpty
    private Time duration;

    @NotNull
    @NotEmpty
    private Boolean virtualFlag;
    
    @NotNull
    @NotEmpty
    private Integer totalTasks;
    
    

    public ExamDto(@NotNull @NotEmpty Long examId, @NotNull @NotEmpty String title,
			@NotNull @NotEmpty String description, @NotNull @NotEmpty byte[] trophy,
			@NotNull @NotEmpty Integer maxScore, @NotNull @NotEmpty Timestamp date, @NotNull @NotEmpty Time duration,
			@NotNull @NotEmpty Boolean virtualFlag, @NotNull @NotEmpty Integer totalTasks) {
		super();
		this.examId = examId;
		this.title = title;
		this.description = description;
		this.trophy = trophy;
		this.maxScore = maxScore;
		this.date = date.toLocalDateTime();
		this.duration = duration;
		this.virtualFlag = virtualFlag;
		this.totalTasks = totalTasks;
	}

    
	public Integer getTotalTasks() {
		return totalTasks;
	}


	public void setTotalTasks(Integer totalTasks) {
		this.totalTasks = totalTasks;
	}


	public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getTrophy() {
        return trophy;
    }

    public void setTrophy(byte[] trophy) {
        this.trophy = trophy;
    }

    public Integer getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Integer maxScore) {
        this.maxScore = maxScore;
    }

    public LocalDateTime getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date.toLocalDateTime();
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public Boolean getVirtualFlag() {
        return virtualFlag;
    }

    public void setVirtualFlag(Boolean virtualFlag) {
        this.virtualFlag = virtualFlag;
    }
}
