package hr.fer.proinz.projekt.controller;

import java.sql.Time;
import java.sql.Timestamp;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import hr.fer.proinz.projekt.entity.AppUser;

public class ExamsDto {
	@NotNull
    @NotEmpty
    private Long examId;
	
	@NotNull
    @NotEmpty
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private AppUser creator;

    @NotNull
    @NotEmpty
    private  String title;

    @NotNull
    @NotEmpty
    private String description;

    @NotNull
    @NotEmpty
    private Timestamp date;

    @NotNull
    @NotEmpty
    private Time duration;

    public ExamsDto(@NotNull @NotEmpty Long examId, @NotNull @NotEmpty AppUser creator,
    		@NotNull @NotEmpty String title, @NotNull @NotEmpty String description, 
    		@NotNull @NotEmpty Timestamp date, @NotNull @NotEmpty Time duration) {
		super();
		this.examId = examId;
		this.creator = creator;
		this.title = title;
		this.description = description;
		this.date = date;
		this.duration = duration;
	}

	public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public AppUser getCreator() {
		return creator;
	}

	public void setCreator(AppUser creator) {
		this.creator = creator;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

}
