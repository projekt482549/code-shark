package hr.fer.proinz.projekt.controller;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ExamNewDto {

	@NotNull
	@NotEmpty
	String name;
	
	@NotNull
	@NotEmpty
	String description;
	
	@NotNull
	@NotEmpty
	String date;
	
	@NotNull
	@NotEmpty
	String startTime;
	
	@NotNull
	@NotEmpty
	String duration;
	
	@NotNull
	@NotEmpty
	String cup;
	
	@NotNull
	@NotEmpty
	String[] tasks;

	public ExamNewDto(@NotNull @NotEmpty String name, @NotNull @NotEmpty String description,
			@NotNull @NotEmpty String date, @NotNull @NotEmpty String startTime, @NotNull @NotEmpty String duration,
			@NotNull @NotEmpty String cup, @NotNull @NotEmpty String[] tasks) {
		super();
		this.name = name;
		this.description = description;
		this.date = date;
		this.startTime = startTime;
		this.duration = duration;
		this.cup = cup;
		this.tasks = tasks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getCup() {
		return cup;
	}

	public void setCup(String cup) {
		this.cup = cup;
	}

	public String[] getTasks() {
		return tasks;
	}

	public void setTasks(String[] tasks) {
		this.tasks = tasks;
	}

			
}
