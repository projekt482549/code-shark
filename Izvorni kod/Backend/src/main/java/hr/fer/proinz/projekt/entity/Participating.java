package hr.fer.proinz.projekt.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "participating")
public class Participating {

	@EmbeddedId
	private ParticipatingPK id;
	
	@MapsId("examid")
	@ManyToOne
	@JoinColumn(name = "examid", referencedColumnName = "examid")
	private Exam exam;
	
	@MapsId("userid")
	@ManyToOne
	@JoinColumn(name = "userid", referencedColumnName = "userid")
	private AppUser user;

	public Participating() {
		super();
	}

	public Participating(ParticipatingPK id, Exam exam, AppUser user) {
		super();
		this.id = id;
		this.exam = exam;
		this.user = user;
	}

	public ParticipatingPK getId() {
		return id;
	}

	public void setId(ParticipatingPK id) {
		this.id = id;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}
	
	
	
}
