package hr.fer.proinz.projekt.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import hr.fer.proinz.projekt.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long>{
	
	public AppUser findByUsername(String username);
	
	public AppUser findByEmail(String email);

	@Query("select u from AppUser u where u.enabled = true")
	public List<AppUser> listAll();
	
	@Query("SELECT u FROM AppUser u WHERE u.verificationCode = ?1")
    public AppUser findByVerificationCode(String code);
	
	@Query("select u from AppUser u where u.accounttype = 3")
	public List<AppUser> listAllNotEnabledByAdmin();
	
	@Transactional
	@Modifying
	@Query("update AppUser set accounttype = 1 where username = ?1")
	public void enableAppUserAsAdmin(String username);

	public default AppUser saveUser(AppUser appUser) {
		try {
			return this.save(appUser);
		}catch (Exception e) {
			int i = e.getMessage().indexOf("constraint");
			String s = e.getMessage().substring(i);
			int i2 = s.indexOf("]");
			s = s.substring(0, i2).split(" ")[1].substring(1).split("_")[1];
			
			return new AppUser(s);
		}
	}
	
}