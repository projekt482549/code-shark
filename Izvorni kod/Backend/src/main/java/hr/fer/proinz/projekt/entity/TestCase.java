package hr.fer.proinz.projekt.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="testcase")
public class TestCase{

	@Id
	@GeneratedValue
	@Column(name = "testid", nullable = false)
    private Long testId;
	
	@Column(name = "input", nullable = false)
    private String input;
	
	@Column(name = "output", nullable = false)
    private String output;
    
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "taskid", referencedColumnName = "taskid")
	@JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private Task task;

	public TestCase() {
		
	}
	
	public TestCase(String input, String output, Task task) {
		super();
		this.input = input;
		this.output = output;
		this.task = task;
	}

	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

    
}