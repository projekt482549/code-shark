package hr.fer.proinz.projekt.controller;

import java.util.List;

import hr.fer.proinz.projekt.entity.AppUser;

public class VirtualResult {
	

	List<AppUser> usersFromOriginal;
	
	Integer ranking;
	
	int userScore;
	
	public VirtualResult(List<AppUser> usersFromOriginal, Integer ranking, int userScore) {
		super();
		this.usersFromOriginal = usersFromOriginal;
		this.ranking = ranking;
		this.userScore = userScore;
	}

	public int getUserScore() {
		return userScore;
	}

	public void setUserScore(int userScore) {
		this.userScore = userScore;
	}

	public List<AppUser> getUsersFromOriginal() {
		return usersFromOriginal;
	}

	public void setUsersFromOriginal(List<AppUser> usersFromOriginal) {
		this.usersFromOriginal = usersFromOriginal;
	}

	public Integer getRanking() {
		return ranking;
	}

	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}
	
	
}
