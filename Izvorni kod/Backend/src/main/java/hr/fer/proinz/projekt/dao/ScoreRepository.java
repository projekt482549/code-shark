package hr.fer.proinz.projekt.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import hr.fer.proinz.projekt.entity.Score;
import org.springframework.data.jpa.repository.Query;

public interface ScoreRepository  extends JpaRepository<Score, Long>{

	@Query("SELECT s FROM Score s WHERE s.appUser.userId = ?1 AND s.exam.examId  = ?2")
	public List<Score> findByUseridAndExamid(long  userid, long examid);

	@Query("SELECT s FROM Score s WHERE  s.exam.examId  = ?1")
	public List<Score> findByExamid( long examid);

	@Query("select s from Score s where s.exam.examId = ?1 and s.task.taskId = ?2")
	public List<Score> findByExamidAndTaskid(Long examid, Long taskid);

	@Query("select s from Score s where s.exam.examId = ?1 and s.task.taskId = ?2 and s.appUser.userId = ?3")
	public Score findByExamidAndTaskidAndUserid(Long examid, Long taskId, Long userId);

	@Query("SELECT s FROM Score s WHERE s.appUser.userId = ?1 ")
	public List<Score> findByUserid(long  userid);

	@Transactional
	@Modifying
	@Query("delete from Score s where s.id = ?1")
	public void deleteByIdMine(Long id);

}
