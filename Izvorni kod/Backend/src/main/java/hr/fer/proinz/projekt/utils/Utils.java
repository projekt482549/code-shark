package hr.fer.proinz.projekt.utils;

public class Utils {
    
	public static byte[] fileAsByteArray(String fileAsString) {
		if (fileAsString == null)
			return null;
		fileAsString = fileAsString.replace("[", "");
		fileAsString = fileAsString.replace("]", "");
		
		String[] bytesAsString = fileAsString.split(",");
		byte[] fileAsByte = new byte[bytesAsString.length];
		int i = 0;
		for(String s: bytesAsString) {
			fileAsByte[i] = (byte) Integer.parseInt(s);
			i++;
		}
		return fileAsByte;
	}
	
}