package hr.fer.proinz.projekt.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import hr.fer.proinz.projekt.entity.Task;

public interface TaskRepository extends JpaRepository<Task, Long>{

	@Query("select e.task from Inexam e where e.exam.examId = ?1")
	public List<Task> findByExamid(Long examid);
	
	@Query("select e from Task e where e.taskId = ?1")
	public Task findByTaskId(Long taskid);

	@Query("select e from Task e where e.creator.userId = ?1")
	public List<Task> findByUserId(Long userid);

	@Transactional
	@Modifying
	@Query("update Task t set t.privateTask = false where t.taskId = ?1")
	public void setPrivacy(Long taskId);

}
