package hr.fer.proinz.projekt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.service.AppUserService;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
    public AppUserService appUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	System.out.println("Name: " + username);
        AppUser appUser = this.appUserService.findByUsername(username);

        if (appUser == null) {
            System.out.println("User not found! " + username);
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }

        if(!appUser.isEnabled())
        	throw new UsernameNotFoundException("User account '" + username + "' is not yet verified");
        
        if(appUser.getAccounttype() == 3)
        	throw new UsernameNotFoundException("User account '" + username + "' is not yet verified by administrator");

        System.out.println("Found User: " + appUser);

        List<GrantedAuthority> grantList = null;
        
        int accounttype = appUser.getAccounttype();
        switch(accounttype) {
        	case 0: grantList = commaSeparatedStringToAuthorityList("ROLE_NATJECATELJ"); break;
        	case 1: grantList = commaSeparatedStringToAuthorityList("ROLE_VODITELJ");break;
        	case 2: grantList = commaSeparatedStringToAuthorityList("ROLE_ADMIN"); break;
        	default: break;
        }
        
        System.out.println(appUser.getUsername() + " " + appUser.getPassword() + " "+ grantList);
        
        UserDetails userDetails = (UserDetails) new User(appUser.getUsername(), //
                appUser.getPassword(), grantList);

        return userDetails;
    }

}
