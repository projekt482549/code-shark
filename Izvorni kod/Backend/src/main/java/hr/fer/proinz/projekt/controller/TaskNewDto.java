package hr.fer.proinz.projekt.controller;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TaskNewDto {
    
	@NotNull
	@NotEmpty
	private  String nameTask;

	@NotNull
	@NotEmpty
	private String textTask;

	@NotNull
	@NotEmpty
	private Integer points;
	
	@NotNull
	@NotEmpty
	private Integer threshold;
	
	@NotNull
	@NotEmpty
	private Boolean privatni;
	
	@NotNull
	@NotEmpty
	private List<InputOutputPair> primjeri;
	
	public TaskNewDto() {
		
	}

	public TaskNewDto(@NotNull @NotEmpty String nameTask, @NotNull @NotEmpty String textTask,
			@NotNull @NotEmpty Integer mscore, @NotNull @NotEmpty Integer threshold,
			@NotNull @NotEmpty List<InputOutputPair> primjeri, @NotNull @NotEmpty Boolean privatni) {
		super();
		this.nameTask = nameTask;
		this.textTask = textTask;
		this.points = mscore;
		this.threshold = threshold;
		this.primjeri = primjeri;
		this.privatni = privatni;
	}


	public String getNameTask() {
		return nameTask;
	}

	public void setNameTask(String nameTask) {
		this.nameTask = nameTask;
	}

	public String getTextTask() {
		return textTask;
	}

	public void setTextTask(String textTask) {
		this.textTask = textTask;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}


	public List<InputOutputPair> getPrimjeri() {
		return primjeri;
	}

	public void setPrimjeri(List<InputOutputPair> primjeri) {
		this.primjeri = primjeri;
	}

	public String getTitle() {
		return nameTask;
	}

	public void setTitle(String title) {
		this.nameTask = title;
	}

	public String getDescription() {
		return textTask;
	}

	public void setDescription(String description) {
		this.textTask = description;
	}
	
	public Boolean getPrivatni() {
		return privatni;
	}

	public void setPrivatni(Boolean privatni) {
		this.privatni = privatni;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}



	public static class InputOutputPair {
		
		@NotNull
		@NotEmpty
		private String ulaz;
		
		@NotNull
		@NotEmpty
		private String izlaz;

		public InputOutputPair(@NotNull @NotEmpty String ulaz, @NotNull @NotEmpty String izlaz) {
			super();
			this.ulaz = ulaz;
			this.izlaz = izlaz;
		}
		
		public String getUlaz() {
			return ulaz;
		}

		public void setUlaz(String ulaz) {
			this.ulaz = ulaz;
		}

		public String getIzlaz() {
			return izlaz;
		}

		public void setIzlaz(String izlaz) {
			this.izlaz = izlaz;
		}



		@Override
		public String toString() {
			return this.ulaz + " // " + this.izlaz;
		}
	}
	
}
