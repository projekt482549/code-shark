package hr.fer.proinz.projekt.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import hr.fer.proinz.projekt.entity.Exam;

public interface ExamRepository extends JpaRepository<Exam, Long>{
	
	@Transactional
	@Modifying
	@Query("delete from Exam e where e.examId = ?1 ")
	public void deleteExam(Long examid);

}
