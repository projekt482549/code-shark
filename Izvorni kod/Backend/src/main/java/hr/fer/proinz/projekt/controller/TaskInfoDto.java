package hr.fer.proinz.projekt.controller;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import hr.fer.proinz.projekt.entity.AppUser;

public class TaskInfoDto {

	@NotNull
	@NotEmpty
	List<AppUser> usersThatFinishedTaskOnExam;
	
	@NotNull
	@NotEmpty
	Integer bestScore;
	
	@NotNull
	@NotEmpty
	Integer bestAverageTime;
	
	@NotNull
	@NotEmpty
	byte[] bestAnswer;
	
	@NotNull
	@NotEmpty
	Boolean userCanAccessBestAnswer;

	public TaskInfoDto(@NotNull @NotEmpty List<AppUser> usersThatFinishedTaskOnExam, @NotNull @NotEmpty Integer bestScore, 
			@NotNull @NotEmpty Integer bestAverageTime, @NotNull @NotEmpty byte[] bestAnswer,
			@NotNull @NotEmpty Boolean userCanAccessBestAnswer) {
		super();
		this.usersThatFinishedTaskOnExam = usersThatFinishedTaskOnExam;
		this.bestScore = bestScore;
		this.bestAverageTime = bestAverageTime;
		this.bestAnswer = bestAnswer;
		this.userCanAccessBestAnswer = userCanAccessBestAnswer;
	}

	public List<AppUser> getUsersThatFinishedTaskOnExam() {
		return usersThatFinishedTaskOnExam;
	}

	public void setUsersThatFinishedTaskOnExam(List<AppUser> usersThatFinishedTaskOnExam) {
		this.usersThatFinishedTaskOnExam = usersThatFinishedTaskOnExam;
	}

	public Integer getBestScore() {
		return bestScore;
	}

	public void setBestScore(Integer bestScore) {
		this.bestScore = bestScore;
	}

	public Integer getBestAverageTime() {
		return bestAverageTime;
	}

	public void setBestAverageTime(Integer bestAverageTime) {
		this.bestAverageTime = bestAverageTime;
	}

	public byte[] getBestAnswer() {
		return bestAnswer;
	}

	public void setBestAnswer(byte[] bestAnswer) {
		this.bestAnswer = bestAnswer;
	}

	public Boolean getUserCanAccessBestAnswer() {
		return userCanAccessBestAnswer;
	}

	public void setUserCanAccessBestAnswer(Boolean userCanAccessBestAnswer) {
		this.userCanAccessBestAnswer = userCanAccessBestAnswer;
	}
	
}
