package hr.fer.proinz.projekt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InexamPK implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "examid")
	private Long examid;
	
	@Column(name = "taskid")
	private Long taskid;
	
	public InexamPK() {
		
	}
	
	public InexamPK(Long examid, Long taskid) {
		this.examid = examid;
		this.taskid = taskid;
	}
	public Long getExamid() {
		return examid;
	}
	public void setExamid(Long examid) {
		this.examid = examid;
	}
	public Long getTaskid() {
		return taskid;
	}
	public void setTaskid(Long taskid) {
		this.taskid = taskid;
	}

	
}
