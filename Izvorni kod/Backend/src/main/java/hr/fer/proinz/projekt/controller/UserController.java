package hr.fer.proinz.projekt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.proinz.projekt.entity.*;

import hr.fer.proinz.projekt.service.*;

@RestController
@RequestMapping("/userinfo")
public class UserController {
	
	@Autowired
	private AppUserService appUserService;
	
	@GetMapping("")
	@Secured({"ROLE_ADMIN", "ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public List<AppUser> listUsers(){
		return appUserService.listAll();
	}
	
	@GetMapping("/{username}")
	@Secured({"ROLE_ADMIN", "ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public UserDataDto findByUserName(@PathVariable("username") String username) {
		return appUserService.findUserData(username);
	}

	@GetMapping("/{username}/v2")
	@Secured({"ROLE_ADMIN", "ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public AppUser findByUserNameV2(@PathVariable("username") String username) {
		return appUserService.findUserDataV2(username);
	}
}
