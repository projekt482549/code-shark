package hr.fer.proinz.projekt.config;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WebCookieFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try {
            filterChain.doFilter(request, response);
        }
        finally {
            HttpServletResponse resp = (HttpServletResponse) response;
            System.out.println(resp.getHeaderNames());
            String cookie = resp.getHeader("Set-Cookie");
            if (cookie != null) {
                resp.setHeader("Set-Cookie", cookie + "; Secure; SameSite=None");
            }
        }
    }
}
