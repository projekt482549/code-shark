package hr.fer.proinz.projekt.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "task")
public class Task{

    @Id
    @GeneratedValue
    @Column(name = "taskid", nullable = false)
    private Long taskId;

    @Column(name = "title", length = 100, nullable = false)
    private  String title;

    @Column(name = "description", length = 5000, nullable = false)
    private String description;
    

    @Column(name = "mscore", nullable = false)
    private Integer mscore;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private AppUser creator;
    
    @Column(name = "privatetask", nullable = false)
    private Boolean privateTask;
    
    @Column(name = "timethreshold", nullable = false)
    private Integer timeThreshold;
    
    @OneToMany(mappedBy = "task")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private List<Inexam> examAssoc;
    

    public Task() {
    	
    }
    
	public Task(String title, String description, Integer mscore, AppUser creator, Boolean privateTask, Integer timeThreshold) {
		super();
		this.title = title;
		this.description = description;
		this.mscore = mscore;
		this.creator = creator;
		this.privateTask = privateTask;
		this.timeThreshold = timeThreshold;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMscore() {
		return mscore;
	}

	public void setMscore(Integer mscore) {
		this.mscore = mscore;
	}

	public AppUser getCreator() {
		return creator;
	}

	public void setCreator(AppUser creator) {
		this.creator = creator;
	}

	public Boolean getPrivateTask() {
		return privateTask;
	}

	public void setPrivateTask(Boolean privateTask) {
		this.privateTask = privateTask;
	}

	public Integer getTimeThreshold() {
		return timeThreshold;
	}

	public void setTimeThreshold(Integer timeThreshold) {
		this.timeThreshold = timeThreshold;
	}
	
	@JsonIgnore
	public List<Inexam> getExamAssoc() {
		return examAssoc;
	}

	public void setExamAssoc(List<Inexam> examAssoc) {
		this.examAssoc = examAssoc;
	}
}