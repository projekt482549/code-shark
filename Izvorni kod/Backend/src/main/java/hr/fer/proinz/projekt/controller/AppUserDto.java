package hr.fer.proinz.projekt.controller;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class AppUserDto {
	
	@NotNull
	@NotEmpty
	private String username;
	
	@NotNull
    @NotEmpty
    private String password;
    
    @NotNull
    @NotEmpty
    private String email;

	@NotNull
    @NotEmpty
    private String name;
    
    @NotNull
    @NotEmpty
    private String surname;
    
    private String photo;
    
    @NotNull
    @NotEmpty
    private Integer accounttype;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Integer getAccounttype() {
		return accounttype;
	}

	public void setAccounttype(@NotNull @NotEmpty Integer accounttype) {
		this.accounttype = accounttype;
	}
}
