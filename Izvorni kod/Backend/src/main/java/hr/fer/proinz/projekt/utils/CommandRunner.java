package hr.fer.proinz.projekt.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Score;
import hr.fer.proinz.projekt.entity.Task;

public class CommandRunner {
	
	/**
	 * Vraca stdout od izvodenog programa
	 * @return
	 */
	public static JavaProcessInformation runJavaProgram(Score score, String input) {
		if(score == null)
			throw new NullPointerException("Score cannot be null");
		
		AppUser user = score.getAppUser();
		byte[] answer = score.getAnswer();
		Task task = score.getTask();
		
		String fileName = "UserAnswer" + user.getName() + task.getTaskId();
				
		
		String s = new String(answer);
		StringBuilder sb = new StringBuilder();
		if(s.contains("class")) {
			int i = s.indexOf("class");
			int i2 = s.indexOf("{");
			
			sb.append(s.substring(0, i + 5));
			sb.append(" ");
			sb.append(fileName);
			sb.append(s.substring(i2));
		} else 
			throw new IllegalArgumentException("Answer error");

		File f = new File(fileName + ".java");
		File f2 = new File(fileName + "Input.txt");
		
		answer = sb.toString().getBytes();
		try(OutputStream w = new BufferedOutputStream(Files.newOutputStream(f.toPath())); 
				Writer w2 = new BufferedWriter(new OutputStreamWriter(new BufferedOutputStream(Files.newOutputStream(f2.toPath())), "UTF-8"))) {	
			w.write(answer);
			w.flush();
			
			w2.append(input);
			w2.flush();
			
			System.err.println("Tu sam");
			runProcess("javac " + f.getName(), null);
			JavaProcessInformation result = runProcess("java " + f.getName() + " <" + f2.getName(), input);
			System.err.println(result.getStdout());
			try {
				Files.delete(f.toPath());
				Files.delete(Paths.get(fileName + ".class"));
				Files.delete(f2.toPath());
			} catch(Exception e) {
				
			}
			
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static JavaProcessInformation runProcess(String command, String input) throws IOException, InterruptedException {
		String stdout;
		Long duration;
		JavaProcessInformation info;
		synchronized (CommandRunner.class) {
			long startTime = System.nanoTime();
			Process pro = Runtime.getRuntime().exec(command);
			
			if(!command.startsWith("javac"))
				write(pro.getOutputStream(), input);
			
			stdout = lines(command + " stdout: ", pro.getInputStream());
			
	        while(true) {
	        	try {
	        		if(System.nanoTime() - startTime > Long.valueOf(1000000000 * 10))
	        			return null;
	        		pro.waitFor();
	        		break;
	        	} catch(InterruptedException e) {
	        		e.printStackTrace();
	        	}
	        }
	        long endTime = System.nanoTime();
	        
	        duration = endTime - startTime;
			info = new JavaProcessInformation(stdout, duration, true);
	        return info;
		}
	}

	private static void write(OutputStream outputStream, String input) {
		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));) {
			writer.write(input);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String lines(String command, InputStream inputStream) throws IOException {
		String line = null;
		StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = in.readLine()) != null) {
            sb.append(line + "\r\n");
        }
        return sb.toString();
	}
	
}
