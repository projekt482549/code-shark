package hr.fer.proinz.projekt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ParticipatingPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "userid")
	private Long userid;
	
	@Column(name = "examid")
	private Long examid;
	
	public ParticipatingPK() {
		
	}

	public ParticipatingPK(Long userid, Long examid) {
		super();
		this.userid = userid;
		this.examid = examid;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getExamid() {
		return examid;
	}

	public void setExamid(Long examid) {
		this.examid = examid;
	}
	
	
	
}
