package hr.fer.proinz.projekt.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "users", //
        uniqueConstraints = { //
                @UniqueConstraint(name = "APP_USER_UK", columnNames = {"username", "email"})})
public class AppUser{

	@Id
    @GeneratedValue
    @Column(name = "userid", nullable = false)
    private Long userId;

    @Column(name = "username", length = 36, nullable = false)
    private String username;

    @Column(name = "password", length = 128, nullable = false)
    private String password;
    
    @Column(name = "email", length = 128, nullable = false)
    private String email;
    
    @Column(name = "name", length = 128, nullable = false)
    private String name;
    
    @Column(name = "surname", length = 128, nullable = false)
    private String surname;
    
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "enabled", length = 1, nullable = false)
    private Boolean enabled;
    
    @Column(name = "accounttype")
    private Integer accounttype;
    
    @Column(name = "verification_code")
    private String verificationCode;
    
    @OneToMany(mappedBy = "user")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private List<Participating> examAssoc;
    
    
    public AppUser() {
    	
    }

    public AppUser(String username, String password, String email, String name, String surname, byte[] photo,
			boolean enabled, Integer accounttype, String verificationCode) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.photo = photo;
		this.enabled = enabled;
		this.accounttype = accounttype;
		this.setVerificationCode(verificationCode);
	}
    
    public AppUser(String constraintViolated) {
    	super();
    	this.username = constraintViolated;
    }

	public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getAccounttype() {
		return accounttype;
	}

	public void setAccounttype(Integer accounttype) {
		this.accounttype = accounttype;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	
	@JsonIgnore
	public List<Participating> getExamAssoc() {
		return examAssoc;
	}

	public void setExamAssoc(List<Participating> examAssoc) {
		this.examAssoc = examAssoc;
	}

}