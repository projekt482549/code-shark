package hr.fer.proinz.projekt.service.impl;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import hr.fer.proinz.projekt.controller.TaskOnExamDto;
import hr.fer.proinz.projekt.controller.TaskEvalueteDto;
import hr.fer.proinz.projekt.controller.TaskNewDto;
import hr.fer.proinz.projekt.controller.TaskNewDto.InputOutputPair;
import hr.fer.proinz.projekt.dao.AppUserRepository;
import hr.fer.proinz.projekt.dao.ExamRepository;
import hr.fer.proinz.projekt.dao.ParticipatingRepository;
import hr.fer.proinz.projekt.dao.ScoreRepository;
import hr.fer.proinz.projekt.dao.TaskRepository;
import hr.fer.proinz.projekt.dao.TestcaseRepository;
import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Exam;
import hr.fer.proinz.projekt.entity.Participating;
import hr.fer.proinz.projekt.entity.ParticipatingPK;
import hr.fer.proinz.projekt.entity.Score;
import hr.fer.proinz.projekt.entity.Task;
import hr.fer.proinz.projekt.entity.TestCase;
import hr.fer.proinz.projekt.service.TaskService;
import hr.fer.proinz.projekt.utils.CommandRunner;
import hr.fer.proinz.projekt.utils.JavaProcessInformation;
import hr.fer.proinz.projekt.utils.MessageString;
import hr.fer.proinz.projekt.utils.Utils;

@Service
public class TaskServiceImpl implements TaskService{
	
	@Autowired
	private AppUserRepository userRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	@Autowired 
	private ExamRepository examRepository; 
	
	@Autowired
	private TestcaseRepository testcaseRepository;
	
	@Autowired
	private ScoreRepository scoreRepository;
	
	@Autowired
	private ParticipatingRepository participatingRepository;

	@Override
	public List<Task> listAll() {
		
		List<Exam> exams = examRepository.findAll();
		
		List<Task> temp;
		
		ZoneId z = ZoneId.of("Europe/Zagreb");
		Instant now = Instant.now();
		ZonedDateTime currZg = now.atZone(z);
		
		for(Exam e: exams) {
			LocalTime time = e.getDuration().toLocalTime();
			LocalDateTime ts = e.getDateStart().toLocalDateTime();
			if( currZg.toLocalDateTime().isAfter(ts.plusHours(time.getHour()).plusMinutes(time.getMinute()).plusSeconds(time.getSecond()))) {
				temp = taskRepository.findByExamid(e.getExamId());
				for(Task t: temp) {
					if(t.getPrivateTask()) 
						taskRepository.setPrivacy(t.getTaskId());
				}
			}
		}
		
		List<Task> allTasks = taskRepository.findAll();
		List<Task> allNonPrivateTasks = new ArrayList<>();
		
		allTasks.forEach(t -> {
			if(!t.getPrivateTask())
				allNonPrivateTasks.add(t);
		});
		return allNonPrivateTasks;
	}
	
	@Override
	public Task createTask(TaskNewDto taskDto) {
		List<TaskNewDto.InputOutputPair> primjeri = taskDto.getPrimjeri();
		taskDto.getPrimjeri().forEach(System.out::println);
		if(primjeri.size() != taskDto.getPoints())
			return null;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		Task newTask = taskRepository.save(new Task(taskDto.getNameTask(), taskDto.getTextTask(), taskDto.getPoints(), currentUser, 
									taskDto.getPrivatni(), taskDto.getThreshold()));
		
		for(int i = 0; i < primjeri.size(); i++) {
			System.out.println(primjeri.get(i).getUlaz());
			testcaseRepository.save(new TestCase(primjeri.get(i).getUlaz(), primjeri.get(i).getIzlaz() , newTask));
		}
		
		return newTask;
	}

	@Override
	public Task findByTaskid(Long taskid) {

		return taskRepository.findByTaskId(taskid);
	}

	@Override
	public TaskOnExamDto findTaskOnExam(Long examid, Integer taskno) {
						
		if(examid == null || taskno == null || taskno < 0 || examid < 0)
			return null;
		
		Exam exam = examRepository.findById(examid).get();
		
		ZoneId z = ZoneId.of("Europe/Zagreb");
		Instant now = Instant.now();
		ZonedDateTime currZg = now.atZone(z);
		
		LocalTime time = exam.getDuration().toLocalTime();
		LocalDateTime ts = exam.getDateStart().toLocalDateTime();
		
		
//		----------------------------------------------------------------------------------------------------------------
		if(ts.isAfter(currZg.toLocalDateTime()) || 
				currZg.toLocalDateTime().isAfter(ts.plusHours(time.getHour()).plusMinutes(time.getMinute()).plusSeconds(time.getSecond()))) {
			System.out.println("Too late or too early");
			return null;
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		List<Task> tasksOnExam;
		if(exam.isVirtualFlag()) {
			if(exam.getCreator().getUserId() != currentUser.getUserId()) {
				System.err.println("Nema reference na originalno ili nije doabar korisnik");
				return null;
			}
			System.out.println("Tu sam virtualno");
			if(exam.getOriginalExamFromVirtualExam() != null)
				tasksOnExam = taskRepository.findByExamid(exam.getOriginalExamFromVirtualExam().getExamId());
			else
				tasksOnExam = taskRepository.findByExamid(examid);
		}else
			tasksOnExam = taskRepository.findByExamid(examid);
		
		if(tasksOnExam.isEmpty())
			return null;
		
		int len = tasksOnExam.size();
		if(taskno - 1 >= len)
			return null;
		
		Task t = tasksOnExam.get(taskno - 1);
		return new TaskOnExamDto(t.getTaskId(), t.getTitle(), t.getDescription(), t.getMscore(), Timestamp.valueOf(ts), Time.valueOf(time), tasksOnExam.size());
	}

	@Override
	public List<Task> findByUserid(Long userid) {
		return taskRepository.findByUserId(userid);
	}

	@Override
	public Score evalueteAndScoreTask(Long taskid, TaskEvalueteDto taskFile) {
		return evalueteAndScore(taskid, null, taskFile, false);
	}

	@Override
	public Score evalueteAndScore(Long id, Integer taskno, TaskEvalueteDto taskFile, boolean fromExam) {
		
		byte[] fileAsByte = Utils.fileAsByteArray(taskFile.getFile());
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		System.out.println("Tu sam ");
		System.out.println(currentUser.getUserId());
		Score score;
		Task taskToCompare;
		Score existingScore;
				
		if(fromExam) {
			Exam exam = examRepository.findById(id).get();
			
			ZoneId z = ZoneId.of("Europe/Zagreb");
			Instant now = Instant.now();
			ZonedDateTime currZg = now.atZone(z);
			
			LocalTime time = exam.getDuration().toLocalTime();
			LocalDateTime ts = exam.getDateStart().toLocalDateTime();
			
			if(ts.isAfter(currZg.toLocalDateTime()) || 
					currZg.toLocalDateTime().isAfter(ts.plusHours(time.getHour()).plusMinutes(time.getMinute()).plusSeconds(time.getSecond()))) {
				System.out.println("Exam is over!!");
				return null;
			}
			
			if(exam.isVirtualFlag())
				id = exam.getOriginalExamFromVirtualExam().getExamId();
			
			taskToCompare = taskRepository.findByExamid(id).get(taskno - 1);
			score = new Score(fileAsByte, 0, currentUser, taskToCompare, exam, -1);
			existingScore = scoreRepository.findByExamidAndTaskidAndUserid(exam.getExamId(), taskToCompare.getTaskId(), currentUser.getUserId());
			if(existingScore != null) {//--------------------------------------------------------------------------------------------------------------
				scoreRepository.deleteByIdMine(existingScore.getId());
			}
			if(!participatingRepository.findExamsByUserId(currentUser.getUserId()).contains(exam)) {
				participatingRepository.save(new Participating(new ParticipatingPK(currentUser.getUserId(), exam.getExamId()), exam, currentUser));
			}
		} else {
			System.out.println("Tu sam 1 " + fromExam);
			taskToCompare = taskRepository.findById(id).get();
			score = new Score(fileAsByte, 0, currentUser, taskToCompare, null, -1);
		}
		
		List<TestCase> testcases = testcaseRepository.findByTaskid(taskToCompare.getTaskId());
		
		List<Long> durations = new ArrayList<>();
		
		int brTocnoRijesenihPrimjera = 0;
				
		for(TestCase test : testcases) {
			JavaProcessInformation info = CommandRunner.runJavaProgram(score, test.getInput());
			if(info == null) {
				System.out.println("Info je null");
				continue;
			}
			
			if(!info.isCompiled()) {
				System.out.println("Didn't compile");
				if(fromExam)
					scoreRepository.save(score);
				return score;
			}
			
			String[] stdoutLines = info.getStdout().trim().split(System.lineSeparator());
			String[] testOutputLines = test.getOutput().trim().split("\n");
			
			int i;
			for(i = 0; i < stdoutLines.length; i++)
				System.out.println("Tu sam ----------- MOJE: " + stdoutLines[i]);
			for(i = 0; i < testOutputLines.length; i++)
				System.out.println("Tu sam ----------- PRAVO: " + testOutputLines[i]);
			
			if(stdoutLines.length != testOutputLines.length)
				System.out.println("Velicine nisu iste");
			else {
				for( i = 0; i < stdoutLines.length; i++) {
					if(!stdoutLines[i].trim().equals(testOutputLines[i].trim()))
						break;
				}
				if(i == stdoutLines.length)
					brTocnoRijesenihPrimjera++;
			}
			
			durations.add(info.getDuration() / 1000000);
		}
		
		score.setScore(brTocnoRijesenihPrimjera);
		long sum = 0;
		for(Long d: durations)
			sum += d;
		
		if(durations.size() != 0)
			score.setAverageTime((int) sum / durations.size());
		else 
			score.setAverageTime(Integer.MAX_VALUE);
		
		if(fromExam)
			scoreRepository.save(score);
		
		return score;
	}

	@Override
	public List<Task> getVoditeljTasks(String username) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		if(!currentUser.getUsername().equals(username))
			return null;
		
		return taskRepository.findByUserId(currentUser.getUserId());
	}

	@Override
	public List<Task> getTasksFromExam(Long examid) {
		Exam exam = examRepository.getById(examid);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		if(exam.getCreator().getUserId() != currentUser.getUserId() && currentUser.getAccounttype() != 2)
			return null;
		
		return taskRepository.findByExamid(examid);
	}

	@Override
	public TaskNewDto getTask(Long taskId) {
		Task task = taskRepository.getById(taskId);
		List<InputOutputPair> ioPairs = new ArrayList<>();
		List<TestCase> testCaseList = testcaseRepository.findByTaskid(taskId);
		for(TestCase tc: testCaseList) {
			InputOutputPair ioPair = new InputOutputPair(tc.getInput(), tc.getOutput());
			ioPairs.add(ioPair);
		}
		return new TaskNewDto(task.getTitle(), task.getDescription(), task.getMscore(), task.getTimeThreshold(),
				ioPairs, task.getPrivateTask());
	}
	
	@Override
	public MessageString updateTask(Long taskId, TaskNewDto taskDto) {
		Task task = taskRepository.getById(taskId);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		if(currentUser.getUserId() != task.getCreator().getUserId() && currentUser.getAccounttype() != 2)
			return new MessageString("forbidden");
		
		task.setTitle(taskDto.getNameTask());
		task.setDescription(taskDto.getDescription());
		task.setPrivateTask(taskDto.getPrivatni());
		task.setTimeThreshold(taskDto.getThreshold());
		task.setMscore(taskDto.getPoints());
		
		testcaseRepository.deleteByTaskId(taskId);
		
		List<TaskNewDto.InputOutputPair> primjeri = taskDto.getPrimjeri();
		for(int i = 0; i < primjeri .size(); i++) 
			testcaseRepository.save(new TestCase(primjeri.get(i).getUlaz(), primjeri.get(i).getIzlaz() , task));
		
		return new MessageString("ok");
	}
}
