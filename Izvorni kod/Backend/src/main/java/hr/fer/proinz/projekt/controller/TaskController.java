package hr.fer.proinz.projekt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.proinz.projekt.entity.Score;
import hr.fer.proinz.projekt.entity.Task;
import hr.fer.proinz.projekt.service.TaskService;
import hr.fer.proinz.projekt.utils.MessageString;

@RestController
@RequestMapping("/task")
public class TaskController {
	
	@Autowired 
	private TaskService taskService;
	
	@PostMapping("/{taskid}")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public Score evalueteTask(@PathVariable("taskid") Long taskId, @RequestBody TaskEvalueteDto taskFile){
		return taskService.evalueteAndScoreTask(taskId, taskFile);
	}
	
	@GetMapping("/{taskid}/taskInfo")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public TaskNewDto getTask(@PathVariable("taskid") Long taskId){
		return taskService.getTask(taskId);
	}
	
	@PostMapping("/{taskid}/update")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public MessageString updateTask(@PathVariable("taskid") Long taskId, @RequestBody TaskNewDto taskDto){
		return taskService.updateTask(taskId, taskDto);
	}
	
	@PostMapping("")
	@Secured({"ROLE_VODITELJ"})
	public Task createTask(@RequestBody TaskNewDto taskDto) {
		return taskService.createTask(taskDto);
	}
	
	@GetMapping("")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public List<Task> getNonPrivateTasks(){
		return taskService.listAll();
	}
	
	@GetMapping("/{username}")
	@Secured({"ROLE_VODITELJ"})
	public List<Task> getVoditeljTasks(@PathVariable("username") String username){
		return taskService.getVoditeljTasks(username);
	}
	
}
