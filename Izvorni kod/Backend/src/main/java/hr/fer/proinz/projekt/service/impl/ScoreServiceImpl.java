package hr.fer.proinz.projekt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import hr.fer.proinz.projekt.dao.ScoreRepository;
import hr.fer.proinz.projekt.entity.Score;
import hr.fer.proinz.projekt.service.ScoreService;
import org.springframework.stereotype.Service;

@Service
public class ScoreServiceImpl implements ScoreService{
	
	@Autowired
	private ScoreRepository scoreRepository;

	@Override
	public List<Score> getUserScoresOnExam(Long userid, Long examid) {

		return   scoreRepository.findByUseridAndExamid(userid,examid);
	}

	@Override
	public List<Score> getByExamid(long examid) {
		return   scoreRepository.findByExamid(examid);
	}

	@Override
	public List<Score> getByUserid(long userid) {
		return scoreRepository.findByUserid(userid);
	}


}


