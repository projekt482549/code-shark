package hr.fer.proinz.projekt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.proinz.projekt.service.ExamService;

@RestController
@RequestMapping("/exams")
public class ExamsController {

	@Autowired
	private ExamService examService;
	
	@GetMapping("")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public List<ExamsDto> getExams(){
		return examService.getExamsForYearAndMonth(null, null);
	}
	
	@GetMapping("/{year}/{month}")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public List<ExamsDto> getExamsForYearAndMonth(@PathVariable("year") Integer year, @PathVariable("month") Integer month){
		return examService.getExamsForYearAndMonth(year, month);
	}
	
	@GetMapping("/{year}")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public List<ExamsDto> getExamsForYear(@PathVariable("year") Integer year){
		return examService.getExamsForYearAndMonth(year, null);
	}
	
	
	
	
}
