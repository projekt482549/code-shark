package hr.fer.proinz.projekt.service;

import java.util.List;

import hr.fer.proinz.projekt.controller.ExamDto;
import hr.fer.proinz.projekt.controller.ExamNewDto;
import hr.fer.proinz.projekt.controller.ExamsDto;
import hr.fer.proinz.projekt.controller.TaskInfoDto;
import hr.fer.proinz.projekt.controller.VirtualRandomDto;
import hr.fer.proinz.projekt.controller.VirtualResult;
import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Exam;
import hr.fer.proinz.projekt.utils.MessageString;

public interface ExamService {
	
	public ExamDto findById(Long examId);

	public Exam createExam(ExamNewDto examDto);

	public List<ExamsDto> getExamsForYearAndMonth(Integer year, Integer month);

	public List<AppUser> getInfoForFinishedExam(Long examid);

	public TaskInfoDto getInfoForTaskOnFinishedExam(Long examid, Integer taskno);

	public ExamDto createVirtualExamBasedOnExam(Long examid);
	
	public ExamDto createVirtualExamRandom(VirtualRandomDto virtualExamInfo);

	public VirtualResult getInfoForVirtualExam(Long examid);

	public MessageString updateExam(ExamNewDto examDto, Long examid);
	
}
