package hr.fer.proinz.projekt.service.impl;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import hr.fer.proinz.projekt.controller.ExamDto;
import hr.fer.proinz.projekt.controller.ExamNewDto;
import hr.fer.proinz.projekt.controller.ExamsDto;
import hr.fer.proinz.projekt.controller.TaskInfoDto;
import hr.fer.proinz.projekt.controller.VirtualRandomDto;
import hr.fer.proinz.projekt.controller.VirtualResult;
import hr.fer.proinz.projekt.dao.AppUserRepository;
import hr.fer.proinz.projekt.dao.ExamRepository;
import hr.fer.proinz.projekt.dao.InexamRepository;
import hr.fer.proinz.projekt.dao.ScoreRepository;
import hr.fer.proinz.projekt.dao.TaskRepository;
import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Exam;
import hr.fer.proinz.projekt.entity.Inexam;
import hr.fer.proinz.projekt.entity.InexamPK;
import hr.fer.proinz.projekt.entity.Score;
import hr.fer.proinz.projekt.entity.Task;
import hr.fer.proinz.projekt.service.ExamService;
import hr.fer.proinz.projekt.service.TaskService;
import hr.fer.proinz.projekt.utils.MessageString;
import hr.fer.proinz.projekt.utils.Utils;

@Service
public class ExamServiceImpl implements ExamService {

	@Autowired
	private ExamRepository examRepository;

	@Autowired
	private TaskRepository taskRepository;
	
	@Autowired
	private ScoreRepository scoreRepository;
	
	@Autowired
	private AppUserRepository userRepository;
	
	@Autowired
	private InexamRepository inexamRepository;
	
	@Autowired
	private TaskService taskService;

	@Override
	public Exam createExam(ExamNewDto examDto) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		byte[] trophy = Utils.fileAsByteArray(examDto.getCup());
		
		List<Task> tasksOnExam = new ArrayList<>();
		String[] taskids = examDto.getTasks();
		int i;
		for( i = 0; i < taskids.length; i++)
			tasksOnExam.add(taskRepository.findByTaskId(Long.parseLong(taskids[i])));
		
		Integer maxScore = 0;
		for(Task t: tasksOnExam)
			maxScore += t.getMscore();
		
		
		Date dateStart = Date.valueOf(examDto.getDate());
		Time timeStart = Time.valueOf(examDto.getStartTime() + ":00");
		Time duration = Time.valueOf(examDto.getDuration());
		
		Exam newExam = new Exam(currentUser, examDto.getName(), examDto.getDescription(), trophy, maxScore,
								Timestamp.valueOf(LocalDateTime.of(dateStart.toLocalDate(), timeStart.toLocalTime())), duration, false, null);
			
		newExam = examRepository.save(newExam);
		for( i = 0; i < tasksOnExam.size(); i++)
			inexamRepository.save(new Inexam(new InexamPK(newExam.getExamId(), tasksOnExam.get(i).getTaskId()), newExam, tasksOnExam.get(i), i + 1));
		
		return newExam;
	}

	@Override
	public ExamDto findById(Long examId) {
		Exam exam = examRepository.findById(examId).get();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		if (exam.isVirtualFlag() && exam.getCreator().getUserId() != currentUser.getUserId())
			return null;

		List<Task> examTasks = taskRepository.findByExamid(examId);
		
		ExamDto examDto = new ExamDto(examId, exam.getTitle(), exam.getDescription(), exam.getTrophy(),
				exam.getMaxScore(), exam.getDateStart(), exam.getDuration(), exam.isVirtualFlag(), examTasks.size());
		System.err.println(examDto.getDate());
		return examDto;
	}

	@Override
	public List<ExamsDto> getExamsForYearAndMonth(Integer year, Integer month) {
		List<Exam> allExams = examRepository.findAll();
		List<ExamsDto> returnExams = new ArrayList<>();
		
		for(Exam e: allExams) {
			Timestamp t = e.getDateStart();
			if(((month == null && year == null) ||
				 (month == null && t.toLocalDateTime().getYear() == year) ||
				 (t.toLocalDateTime().getYear() == year && t.toLocalDateTime().getMonth().getValue() == month)) && !e.isVirtualFlag())
				returnExams.add(new ExamsDto(e.getExamId(), e.getCreator(), e.getTitle(), e.getDescription(), e.getDateStart(), e.getDuration()));
		}
		
		return returnExams;
	}

	@Override
	public List<AppUser> getInfoForFinishedExam(Long examid) {
		
		Exam exam = examRepository.getById(examid);
		if(exam == null || exam.isVirtualFlag())
			return null;
		
		LocalTime time = exam.getDuration().toLocalTime();
		LocalDateTime ts = exam.getDateStart().toLocalDateTime();
		
		Timestamp currZg = getCurrentTimeZagreb();
		
		System.out.println("------------------ " + currZg.toLocalDateTime());
		System.out.println("------------------ " + ts.plusHours(time.getHour()).plusMinutes(time.getMinute()).plusSeconds(time.getSecond()));
		if(!currZg.toLocalDateTime().isAfter(ts.plusHours(time.getHour()).plusMinutes(time.getMinute()).plusSeconds(time.getSecond()))) {
			System.err.println("Ovo ? ");
			return null;
		}
		
		Map<Long, UserScore> result = getSortedUserScoresForExam(exam);
        
        result.forEach((id, us) -> System.out.println("User " + id + " " + us.getOverallScore() + " " + us.getOverallAverageTime()));
		
        List<AppUser> users = new ArrayList<>();
        
        result.forEach((id, userScore) -> {
        	AppUser user = userRepository.findById(id).get();
        	users.add(user);
        });
        
		return users;
	}
	
	private Map<Long, UserScore> getSortedUserScoresForExam(Exam exam){
		List<Score> scoresFromExam = scoreRepository.findByExamid(exam.getExamId());
		System.out.println(scoresFromExam.size());
		Map<Long, List<Score>> userScores = new HashMap<>();
		for(Score s : scoresFromExam) {
			List<Score> temp = userScores.get(s.getAppUser().getUserId());
			if(temp == null) {
				temp = new ArrayList<>();
			}
			temp.add(s);
			userScores.put(s.getAppUser().getUserId(), temp);
		}

		Map<Long, UserScore> userOverallScore = new HashMap<>();
		
		userScores.forEach((id, list) -> {
			int sumScore = 0;
			int sumTime = 0;
			for(Score s: list) {
				sumScore += s.getScore();
				sumTime += s.getAverageTime();
			}
			userOverallScore.put(id, new UserScore(sumScore, sumTime / list.size()));
		});
		
		List<Entry<Long, UserScore>> list = new ArrayList<>(userOverallScore.entrySet());
        list.sort(Entry.comparingByValue());

        Map<Long, UserScore> result = new LinkedHashMap<>();
        for (Entry<Long, UserScore> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
	}

	private static class UserScore implements Comparable<UserScore>{
		private Integer overallScore;
		private Integer overallAverageTime;
		
		public UserScore(Integer overallScore, Integer overallAverageTime) {
			this.overallAverageTime = overallAverageTime;
			this.overallScore = overallScore;
		}

		public Integer getOverallScore() {
			return overallScore;
		}

		public Integer getOverallAverageTime() {
			return overallAverageTime;
		}

		@Override
		public int compareTo(UserScore o) {
			if(this.overallScore > o.overallScore)
				return -1;
			else if ( this.overallScore < o.overallScore)
				return 1;
			else if(this.overallAverageTime > o.overallAverageTime)
				return 1;
			else if(this.overallAverageTime < o.overallAverageTime)
				return -1;
			else return 0;
		}
		
	}

	@Override
	public TaskInfoDto getInfoForTaskOnFinishedExam(Long examid, Integer taskno) {
		Task task = taskRepository.findByExamid(examid).get(taskno - 1);
		
		List<Score> scoresFromTaskOnExam = scoreRepository.findByExamidAndTaskid(examid, task.getTaskId());
		
		List<AppUser> usersForTaskOnExam = new ArrayList<>();
		
		Score bestScore = scoresFromTaskOnExam.get(0);
		for(Score s: scoresFromTaskOnExam) {
			usersForTaskOnExam.add(s.getAppUser());
			if(s.getScore() > bestScore.getScore())
				bestScore = s;
			else if (s.getScore() == bestScore.getScore()) {
				if(s.getAverageTime() < bestScore.getAverageTime())
					bestScore = s;
			}
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		boolean eligible = false;
		for(AppUser u: usersForTaskOnExam) {
			if(currentUser.getUserId() == u.getUserId()) {
				Score s = scoreRepository.findByExamidAndTaskidAndUserid(examid, task.getTaskId(), currentUser.getUserId());
				if(s.getScore() == task.getMscore())
					eligible = true;
			}
		}
		
		return new TaskInfoDto(usersForTaskOnExam, bestScore.getScore(), bestScore.getAverageTime(), bestScore.getAnswer(), eligible);
	}

	@Override
	public ExamDto createVirtualExamBasedOnExam(Long examid) {
		Exam originalExam = examRepository.findById(examid).get();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		Timestamp currZg = getCurrentTimeZagreb();
		
		Exam virtualExam = new Exam(currentUser, originalExam.getTitle(), originalExam.getDescription(),originalExam.getTrophy(), 
									originalExam.getMaxScore(), currZg, originalExam.getDuration(), true, originalExam);
		
		int numTasks = taskRepository.findByExamid(examid).size();
		
		examRepository.save(virtualExam);
		
		return new ExamDto(virtualExam.getExamId(), virtualExam.getTitle(), virtualExam.getDescription(), virtualExam.getTrophy(),
							virtualExam.getMaxScore(), virtualExam.getDateStart(), virtualExam.getDuration(), true, numTasks);
	}

	@Override
	public VirtualResult getInfoForVirtualExam(Long examid) {
		Exam exam = examRepository.getById(examid);
		
		if(exam == null || !exam.isVirtualFlag())
			return null;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		List<Score> userScoresOnVirtualExam = scoreRepository.findByUseridAndExamid(currentUser.getUserId(), exam.getExamId());
		
		int sumScore = 0;
		int sumTime = 0;
		for(Score s: userScoresOnVirtualExam) {
			sumScore += s.getScore();
			sumTime += s.getAverageTime();
		}
		
		UserScore userOverallScore;
		if(sumScore == 0) {
			userOverallScore = new UserScore(sumScore, Integer.MAX_VALUE);
		} else
			userOverallScore = new UserScore(sumScore, sumTime / userScoresOnVirtualExam.size());
		
		Exam originalExam = exam.getOriginalExamFromVirtualExam();
		if(originalExam == null) {
			inexamRepository.deleteByExamId(exam.getExamId());
			examRepository.deleteExam(exam.getExamId());			
			return new VirtualResult(null, null, sumScore);
		}
		
		Map<Long, UserScore> originalExamUserScores = this.getSortedUserScoresForExam(originalExam);
		
		List<AppUser> users = new ArrayList<>();
        int ranking = 0;
        
		boolean flag = false;
		int i = 1;
		for(Entry<Long, UserScore> entry: originalExamUserScores.entrySet()) {
			if(!flag && userOverallScore.compareTo(entry.getValue()) >= 0) {
				ranking = i;
				flag = true;
			}
        	AppUser user = userRepository.findById(entry.getKey()).get();
        	users.add(user);
        	i++;
		}
		
		if(ranking == 0)
			ranking = users.size() + 1;
		
		examRepository.deleteExam(exam.getExamId());
		
		return new VirtualResult(users, ranking, userOverallScore.getOverallScore());
	}

	@Override
	public ExamDto createVirtualExamRandom(VirtualRandomDto virtualExamInfo) {
		
		List<Task> allTasks = taskService.listAll();
		
		int numberOfTasks = virtualExamInfo.getNumOfTasks();
		
		if(numberOfTasks > allTasks.size())
			numberOfTasks = allTasks.size();
		
		Map<Integer, List<Task>> tasksGroupedByMaxScore = new HashMap<>();
		for(Task t: allTasks) {
			List<Task> temp = tasksGroupedByMaxScore.get(t.getMscore());
			if(temp == null) {
				temp = new ArrayList<>();
				temp.add(t);
				tasksGroupedByMaxScore.put(t.getMscore(), temp);
			} else
				tasksGroupedByMaxScore.get(t.getMscore()).add(t);
		}
		
		tasksGroupedByMaxScore.forEach((k, v) -> {
			System.out.println("Values for " + k);
			for(Task t: v)
				System.out.print(t.getMscore() + ", ");
			System.out.print("\n");
		});

		List<Task> selectedTasks = new ArrayList<>();
		int selected = 0;
		while(selected < numberOfTasks) {
			int remainingNumber = numberOfTasks - selected;
			System.out.println("Pocinjem ispocetka za " + selected + " " + remainingNumber);
			System.out.println("Kljuceva ima " + tasksGroupedByMaxScore.size());
			if(tasksGroupedByMaxScore.size() <= remainingNumber) {
				System.out.println("Ima dosta");
				List<Integer> keysToBeRemoved = new ArrayList<>();
				for(Entry<Integer, List<Task>> entry: tasksGroupedByMaxScore.entrySet()) {
					System.out.println("\tBiram random za " + entry.getKey() + ", velicina " + entry.getValue().size());
					List<Task> tasks = entry.getValue();
					Task randomTask = tasks.get(new Random().nextInt(tasks.size()));
					selectedTasks.add(randomTask);
					tasks.remove(randomTask);
					if(tasks.isEmpty())
						keysToBeRemoved.add(entry.getKey());
					selected++;
					System.out.println("Velicina za" + entry.getKey() +" nakon biranja " + entry.getValue().size());
				}
				for(Integer key: keysToBeRemoved) {
					tasksGroupedByMaxScore.remove(key);
					System.out.println("Micem " + key);
				}
			} else {
				System.out.println("Nema dosta");
				int sum = 0;
				List<Integer> allAvailableKeys = new ArrayList<>();
				Collections.sort(allAvailableKeys);
				for(Entry<Integer, List<Task>> entry: tasksGroupedByMaxScore.entrySet()) {
					sum += entry.getKey();
					allAvailableKeys.add(entry.getKey());
				}
				
				double average = (double) sum / tasksGroupedByMaxScore.size();
				System.out.println("Average " + average);
				Random random = new Random();
				while(selected < numberOfTasks) {
					remainingNumber = numberOfTasks - selected;
					System.out.println("Tu sam za " + selected + " " + remainingNumber);
					if(remainingNumber != 1) {
						System.out.println("\tBiram par");
						List<Task> tasks = null;
						Integer randomKey = null;
						while(tasks == null) {
							randomKey = allAvailableKeys.get(random.nextInt(allAvailableKeys.size()));
							tasks = tasksGroupedByMaxScore.get(randomKey);
						}
						System.out.println("\tOdabrao sam " + randomKey);
						Task randomTask = tasks.get(random.nextInt(tasks.size()));
						double temp = 2 * average - randomKey;
						int closestKey = allAvailableKeys.get(0);
						double minDelta = Math.abs(closestKey - temp);
						for(int i = 1; i < allAvailableKeys.size(); i++) {
							double d = Math.abs(allAvailableKeys.get(i) - temp);
							if(d < minDelta) {
								minDelta = d;
								closestKey = allAvailableKeys.get(i);
							}
						}
						System.out.println("\tOnda sam drugi odabrao " + closestKey);
						List<Task> otherTasks = tasksGroupedByMaxScore.get(closestKey);
						Task otherRandomTask = otherTasks.get(random.nextInt(otherTasks.size()));
						
						selectedTasks.add(randomTask);
						tasks.remove(randomTask);
						if(tasks.isEmpty()) {
							tasksGroupedByMaxScore.remove(randomKey);
							allAvailableKeys.remove(randomKey);
						}
						
						selectedTasks.add(otherRandomTask);
						otherTasks.remove(otherRandomTask);
						if(otherTasks.isEmpty()) {
							tasksGroupedByMaxScore.remove(closestKey);
							allAvailableKeys.remove(closestKey);
						}
						
						selected += 2;
					} else {
						System.out.println("Ostao mi je jedan");
						int middleKey = allAvailableKeys.get(allAvailableKeys.size() / 2);
						System.out.println("Middle key je " + middleKey);
						List<Task> tasks = tasksGroupedByMaxScore.get(middleKey);
						Task randomTask = tasks.get(random.nextInt(tasks.size()));
						selectedTasks.add(randomTask);
						selected++;
					}
				}
			}
			
		}
		
		int maxScore = 0;
		for(Task t: selectedTasks)
			maxScore += t.getMscore();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		Exam exam = new Exam(currentUser, "Generated exam", "Generated exam", null, maxScore, 
							 getCurrentTimeZagreb(), virtualExamInfo.getDuration(), true, null);
		examRepository.save(exam);
		for(int i = 0; i < selectedTasks.size(); i++)
			inexamRepository.save(new Inexam(new InexamPK(exam.getExamId(), selectedTasks.get(i).getTaskId()), exam, selectedTasks.get(i), i + 1));
		
		
		
		return new ExamDto(exam.getExamId(), exam.getTitle(), exam.getDescription(), null, exam.getMaxScore(), exam.getDateStart(), 
								exam.getDuration(), true, selectedTasks.size());
		
	}
	
	private Timestamp getCurrentTimeZagreb() {
		ZoneId z = ZoneId.of("Europe/Zagreb");
		Instant now = Instant.now();
		ZonedDateTime currentDateTimeZagreb = now.atZone(z);
		return Timestamp.valueOf(currentDateTimeZagreb.toLocalDateTime());
	}

	@Override
	public MessageString updateExam(ExamNewDto examDto, Long examid) {
		
		Exam originalExam = examRepository.getById(examid);
		if(originalExam == null)
			return null;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AppUser currentUser = userRepository.findByUsername(auth.getName());
		
		if(currentUser.getUserId() != originalExam.getCreator().getUserId() && currentUser.getAccounttype() != 2)
			return new MessageString("forbidden");;
		
		originalExam.setTitle(examDto.getName());
		originalExam.setDescription(examDto.getDescription());
		Date dateStart = Date.valueOf(examDto.getDate());
		Time timeStart = Time.valueOf(examDto.getStartTime() + ":00");
		Time duration = Time.valueOf(examDto.getDuration());
		originalExam.setDateStart(Timestamp.valueOf(LocalDateTime.of(dateStart.toLocalDate(), timeStart.toLocalTime())));
		originalExam.setDuration(duration);
		originalExam.setTrophy(Utils.fileAsByteArray(examDto.getCup()));
		
		List<Task> tasksOnExam = new ArrayList<>();
		String[] taskids = examDto.getTasks();
		int i;
		for( i = 0; i < taskids.length; i++)
			tasksOnExam.add(taskRepository.findByTaskId(Long.parseLong(taskids[i])));
		
		Integer maxScore = 0;
		for(Task t: tasksOnExam)
			maxScore += t.getMscore();
		originalExam.setMaxScore(maxScore);
		
		inexamRepository.deleteByExamId(examid);
		
		for( i = 0; i < tasksOnExam.size(); i++)
			inexamRepository.save(new Inexam(new InexamPK(originalExam.getExamId(), tasksOnExam.get(i).getTaskId()), originalExam, tasksOnExam.get(i), i + 1));
		
		examRepository.save(originalExam);
		
		return new MessageString("ok");
	}
}
