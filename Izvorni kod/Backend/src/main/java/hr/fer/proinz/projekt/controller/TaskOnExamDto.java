package hr.fer.proinz.projekt.controller;

import java.sql.Time;
import java.sql.Timestamp;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TaskOnExamDto {
		
	@NotNull
	@NotEmpty
    public Long taskId;
    
	@NotNull
	@NotEmpty
   	public  String title;

	@NotNull
	@NotEmpty
    public String description;

	@NotNull
	@NotEmpty
    public Integer mscore;
	
	@NotNull
	@NotEmpty
    public Timestamp datestart;
	
	@NotNull
	@NotEmpty
    public Time duration;
    
	@NotNull
	@NotEmpty
    public Integer totaltasks;

	public TaskOnExamDto(@NotNull @NotEmpty Long taskId, @NotNull @NotEmpty String title,
			@NotNull @NotEmpty String description, @NotNull @NotEmpty Integer score,
			@NotNull @NotEmpty Timestamp datestart, @NotNull @NotEmpty Time duration,
			@NotNull @NotEmpty Integer totaltasks) {
		super();
		this.taskId = taskId;
		this.title = title;
		this.description = description;
		this.mscore = score;
		this.datestart = datestart;
		this.duration = duration;
		this.totaltasks = totaltasks;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getScore() {
		return mscore;
	}

	public void setScore(Integer score) {
		this.mscore = score;
	}

	public Integer getTotaltasks() {
		return totaltasks;
	}

	public void setTotaltasks(Integer totaltasks) {
		this.totaltasks = totaltasks;
	}

	public Integer getMscore() {
		return mscore;
	}

	public void setMscore(Integer mscore) {
		this.mscore = mscore;
	}

	public Timestamp getDatestart() {
		return datestart;
	}

	public void setDatestart(Timestamp datestart) {
		this.datestart = datestart;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}
	
	
}
