package hr.fer.proinz.projekt.controller;

public class TaskEvalueteDto {

	String file;
	
	public TaskEvalueteDto() {
		
	}
	
	public TaskEvalueteDto(String file) {
		this.file = file;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
	
}
