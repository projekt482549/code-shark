package hr.fer.proinz.projekt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Exam;
import hr.fer.proinz.projekt.entity.Score;
import hr.fer.proinz.projekt.entity.Task;
import hr.fer.proinz.projekt.service.ExamService;
import hr.fer.proinz.projekt.service.TaskService;
import hr.fer.proinz.projekt.utils.MessageString;

@RestController
@RequestMapping("/exam")
public class ExamController {

	@Autowired
	private ExamService examService;
	
	@Autowired 
	private TaskService taskService;
	
	@PostMapping("")
	@Secured({"ROLE_VODITELJ"})
	public Exam createExam(@RequestBody ExamNewDto examDto) { 
		return examService.createExam(examDto);
	}
	
	@GetMapping("/{examid}")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public ExamDto getExamForId(@PathVariable("examid") Long examid) { 
		return examService.findById(examid);
	}
	
	@PostMapping("/{examid}")
	@Secured({"ROLE_VODITELJ", "ROLE_ADMIN"})
	public MessageString updateExam(@RequestBody ExamNewDto examDto, @PathVariable("examid") Long examid) {
		return examService.updateExam(examDto, examid);
	}
	
	@GetMapping("/{examid}/newVirtual")
	@Secured({"ROLE_NATJECATELJ"})
	public ExamDto createVirtualExam(@PathVariable("examid") Long examid) { 
		return examService.createVirtualExamBasedOnExam(examid);
	}
	
	@PostMapping("/newVirtual")
	@Secured({"ROLE_NATJECATELJ"})
	public ExamDto createVirtualExamRandom(@RequestBody VirtualRandomDto virtualExamInfo) { 
		return examService.createVirtualExamRandom(virtualExamInfo);
	}
	
	@GetMapping("/{examid}/info")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public List<AppUser> getInfoForFinishedExam(@PathVariable("examid") Long examid) { 
		return examService.getInfoForFinishedExam(examid);
	}
	
	@GetMapping("/{examid}/infoVirtual")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public VirtualResult getInfoForVirtualExam(@PathVariable("examid") Long examid) { 
		return examService.getInfoForVirtualExam(examid);
	}
	
	@GetMapping("/{examid}/tasks")
	@Secured({"ROLE_VODITELJ", "ROLE_ADMIN"})
	public List<Task> getTasksFromExam(@PathVariable("examid") Long examid) {
		return taskService.getTasksFromExam(examid);
	}
	
	@GetMapping("/{examid}/task/{taskno}")
	@Secured({"ROLE_NATJECATELJ"})
	public TaskOnExamDto getTaskFromExam(@PathVariable("examid") Long examid, @PathVariable("taskno") Integer taskno) {
		return taskService.findTaskOnExam(examid, taskno);
	}
	
	@PostMapping("/{examid}/task/{taskno}")
	@Secured({"ROLE_NATJECATELJ"})
	public Score submitedTaskFromExam(@PathVariable("examid") Long examid, @PathVariable("taskno") Integer taskno, @RequestBody TaskEvalueteDto taskFile) {
		return taskService.evalueteAndScore(examid, taskno, taskFile, true);
	}
	
	@GetMapping("/{examid}/task/{taskno}/info")
	@Secured({"ROLE_NATJECATELJ", "ROLE_VODITELJ"})
	public TaskInfoDto getTaskInfoFromFinishedExam(@PathVariable("examid") Long examid, @PathVariable("taskno") Integer taskno) {
		return examService.getInfoForTaskOnFinishedExam(examid, taskno);
	}
}
