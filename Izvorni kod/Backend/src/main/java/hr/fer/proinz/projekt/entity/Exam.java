package hr.fer.proinz.projekt.entity;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "exam")
public class Exam{

    @Id
    @GeneratedValue
    @Column(name = "examid", nullable = false)
    private Long examId;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private AppUser creator;
    
    @Column(name = "title", length = 100, nullable = false)
    private  String title;

    @Column(name = "description", length = 5000, nullable = false)
    private String description;
    
    @Column(name = "trophy")
    private  byte[] trophy;
    
    @Column(name = "maxscore", nullable = false)
    private Integer maxScore;
    
    @Column(name = "datestart", length = 128, nullable = false)
    private Timestamp dateStart;
    
    @Column(name = "duration")
    private Time duration;
    
    @Column(name = "virtualflag")
    private boolean virtualFlag;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "virtualexamid", referencedColumnName = "examid")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private Exam originalExamFromVirtualExam;
    
    @OneToMany(mappedBy = "exam")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private List<Participating> userAssoc;

    @OneToMany(mappedBy = "exam")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private List<Inexam> taskAssoc;

    public Exam() {
    	
    }

	public Exam(AppUser creator, String title, String description, byte[] trophy, Integer maxScore, Timestamp dateStart,
			Time duration, boolean virtualFlag, Exam originalExamFromVirtualExam) {
		super();
		this.creator = creator;
		this.title = title;
		this.description = description;
		this.trophy = trophy;
		this.maxScore = maxScore;
		this.dateStart = dateStart;
		this.duration = duration;
		this.virtualFlag = virtualFlag;
		this.originalExamFromVirtualExam = originalExamFromVirtualExam;
	}


	public Long getExamId() {
		return examId;
	}


	public void setExamId(Long examId) {
		this.examId = examId;
	}


	public AppUser getCreator() {
		return creator;
	}


	public void setCreator(AppUser creator) {
		this.creator = creator;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public byte[] getTrophy() {
		return trophy;
	}


	public void setTrophy(byte[] trophy) {
		this.trophy = trophy;
	}


	public Integer getMaxScore() {
		return maxScore;
	}


	public void setMaxScore(Integer maxScore) {
		this.maxScore = maxScore;
	}


	public Timestamp getDateStart() {
		return dateStart;
	}


	public void setDateStart(Timestamp dateStart) {
		this.dateStart = dateStart;
	}


	public Time getDuration() {
		return duration;
	}


	public void setDuration(Time duration) {
		this.duration = duration;
	}


	public boolean isVirtualFlag() {
		return virtualFlag;
	}


	public void setVirtualFlag(boolean virtualFlag) {
		this.virtualFlag = virtualFlag;
	}


	public Exam getOriginalExamFromVirtualExam() {
		return originalExamFromVirtualExam;
	}


	public void setOriginalExamFromVirtualExam(Exam originalExamFromVirtualExam) {
		this.originalExamFromVirtualExam = originalExamFromVirtualExam;
	}

	@JsonIgnore
	public List<Inexam> getTaskAssoc() {
		return taskAssoc;
	}

	public void setTaskAssoc(List<Inexam> taskAssoc) {
		this.taskAssoc = taskAssoc;
	}

	@JsonIgnore
	public List<Participating> getUserAssoc() {
		return userAssoc;
	}

	public void setUserAssoc(List<Participating> userAssoc) {
		this.userAssoc = userAssoc;
	}

	

	
}