package hr.fer.proinz.projekt.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import hr.fer.proinz.projekt.dao.*;
import hr.fer.proinz.projekt.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import hr.fer.proinz.projekt.controller.AppUserDto;
import hr.fer.proinz.projekt.controller.UserDataDto;
import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Exam;
import hr.fer.proinz.projekt.entity.Score;
import hr.fer.proinz.projekt.service.AppUserService;
import hr.fer.proinz.projekt.utils.MessageString;
import hr.fer.proinz.projekt.utils.Utils;
import net.bytebuddy.utility.RandomString;

@Service
public class AppUserServiceImpl implements AppUserService {
	@Autowired
	private ExamService examService;
	@Autowired
	private AppUserRepository appUserRepository;
	@Autowired
	private ParticipatingRepository participatingRepository;
	@Autowired
	private ScoreRepository scoreRepository;

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JavaMailSender mailSender;

	@Override
	public AppUser findByUsername(String username) {
		Assert.notNull(username, "Username must not be null");
		return appUserRepository.findByUsername(username);
	}

	@Override
	public UserDataDto findUserData(String username) {
		AppUser appUser = appUserRepository.findByUsername(username);
		long userid = appUser.getUserId();

		List<Score> userScoresList = scoreRepository.findByUserid(userid);
		int solved = 0;
		List<byte[]> trophies = new ArrayList<>();
		int tried = userScoresList.size();
		for (Score userScore : userScoresList) {

			if (userScore.getScore() == taskRepository.findByTaskId(userScore.getTask().getTaskId()).getMscore()) {
				solved++;
			}

		}

		List<Exam> exams = participatingRepository.findExamsByUserId(userid);
		for (Exam exam : exams) {
			List<AppUser> users = examService.getInfoForFinishedExam(exam.getExamId());
			if (users.size() > 0 && appUser.getUserId() == users.get(0).getUserId()) {
				trophies.add(exam.getTrophy());
			} else if (users.size() > 1 && appUser.getUserId() == users.get(1).getUserId()) {
				trophies.add(exam.getTrophy());
			} else if (users.size() > 2 && appUser.getUserId() == users.get(2).getUserId()) {
				trophies.add(exam.getTrophy());
			}

		}

		if (appUser.getAccounttype() != 0) {
			UserDataDto userDataDto = new UserDataDto(taskRepository.findByUserId(userid), appUser);

			return userDataDto;
		} else {
			UserDataDto userDataDto = new UserDataDto(appUser, solved, tried, trophies);
			return userDataDto;
		}

	}

	@Override
	public List<AppUser> listAll() {
		List<AppUser> users = appUserRepository.findAll();
		List<AppUser> returnUsers = new ArrayList<>();
		users.forEach(u -> {
			if (u.getAccounttype() == 0 || u.getAccounttype() == 1)
				returnUsers.add(u);
		});
		return returnUsers;
	}

	@Override
	public AppUser findByEmail(String email) {
		Assert.notNull(email, "Email cant be null");
		return appUserRepository.findByEmail(email);
	}

	@Override
	public AppUser createUser(AppUserDto dto, String siteUrl) throws UnsupportedEncodingException, MessagingException {

		String randomCode = RandomString.make(64);

		if (dto.getAccounttype() == 1)
			dto.setAccounttype(3);

		byte[] photo = Utils.fileAsByteArray(dto.getPhoto());

		AppUser createdUser = appUserRepository
				.saveUser(new AppUser(dto.getUsername(), passwordEncoder.encode(dto.getPassword()), dto.getEmail(),
						dto.getName(), dto.getSurname(), photo, false, dto.getAccounttype(), randomCode));
		if (createdUser.getEmail() != null)
			sendVerificationEmail(createdUser, siteUrl);

		return createdUser;
	}

	private void sendVerificationEmail(AppUser user, String siteUrl)
			throws UnsupportedEncodingException, MessagingException {
		String toAddress = user.getEmail();
		String fromAddress = "codesharkprojekt@gmail.com";
		String senderName = "Code-shark";
		String subject = "Please verify your registration";
		String content = "Dear [[name]],<br>" + "Please click the link below to verify your registration:<br>"
				+ "<h3><a href=\"[[URL]]\" target=\"_self\">VERIFY</a></h3>" + "Thank you,<br>" + "Your company name.";

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setFrom(fromAddress, senderName);
		helper.setTo(toAddress);
		helper.setSubject(subject);

		content = content.replace("[[name]]", user.getName());
		String verifyURL = siteUrl + "/verify?code=" + user.getVerificationCode();

		content = content.replace("[[URL]]", verifyURL);

		helper.setText(content, true);

		mailSender.send(message);
	}

	@Override
	public boolean verify(String verificationCode) {
		AppUser user = appUserRepository.findByVerificationCode(verificationCode);

		if (user == null || user.isEnabled()) {
			return false;
		} else {
			user.setVerificationCode(null);
			user.setEnabled(true);
			appUserRepository.save(user);

			return true;
		}
	}

	@Override
	public List<AppUser> listAllNotEnabledByAdmin() {
		return appUserRepository.listAllNotEnabledByAdmin();
	}

	@Override
	public void enableAppUserAsAdmin(String username) {
		appUserRepository.enableAppUserAsAdmin(username);
	}

	@Override
	public AppUser findUserDataV2(String username) {
		return appUserRepository.findByUsername(username);
	}

	@Override
	public MessageString updateAppUser(AppUserDto appUserDto, String oldUsername) {
		AppUser user = appUserRepository.findByUsername(oldUsername);
		AppUser tempUser = appUserRepository.findByUsername(appUserDto.getUsername());

		if (tempUser != null && user.getUserId() != tempUser.getUserId()) {
			return new MessageString("username");
		}

		tempUser = appUserRepository.findByEmail(appUserDto.getEmail());
		if (tempUser != null && user.getUserId() != tempUser.getUserId()) {
			return new MessageString("email");
		}

		String password;
		if (appUserDto.getPassword() != null)
			password = passwordEncoder.encode(appUserDto.getPassword());
		else
			password = user.getPassword();

		byte[] photo;
		if (appUserDto.getPhoto() != null)
			photo = Utils.fileAsByteArray(appUserDto.getPhoto());
		else
			photo = user.getPhoto();

		user.setName(appUserDto.getName());
		user.setEmail(appUserDto.getEmail());
		user.setSurname(appUserDto.getSurname());
		user.setAccounttype(appUserDto.getAccounttype());
		user.setPassword(password);
		user.setPhoto(photo);
		user.setUsername(appUserDto.getUsername());

		appUserRepository.save(user);
		return new MessageString("ok");
	}

}
