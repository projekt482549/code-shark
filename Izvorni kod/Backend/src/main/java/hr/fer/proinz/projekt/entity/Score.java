package hr.fer.proinz.projekt.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "score")
public class Score{
	
	@Id
	@GeneratedValue
	@Column(name = "scoreid", nullable = false)
	private Long id;
  
	@Column(name = "answer", nullable = true)
    private  byte[] answer;

	@Column(name = "score", nullable = false)
	private Integer score;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private AppUser appUser;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "taskid", referencedColumnName = "taskid")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private Task task;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "examid", referencedColumnName = "examid")
    @JsonIgnoreProperties(value = {"applications", "hibernateLazyInitializer"})
    private Exam exam;
    
    @Column(name = "averagetime", nullable = false)
    private Integer averageTime;
    
    public Score() {
    	
    }
	
	public Score(byte[] answer, Integer score, AppUser appUser, Task task, Exam exam, Integer averageTime) {
		super();
		this.answer = answer;
		this.score = score;
		this.appUser = appUser;
		this.task = task;
		this.exam = exam;
		this.averageTime = averageTime;
	}
	
	public byte[] getAnswer() {
		return answer;
	}
	
	public void setAnswer(byte[] answer) {
		this.answer = answer;
	}
	
	public Integer getScore() {
		return score;
	}
	
	public void setScore(Integer score) {
		this.score = score;
	}
	
	public AppUser getAppUser() {
		return appUser;
	}
	
	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}
	
	public Task getTask() {
		return task;
	}
	
	public void setTask(Task task) {
		this.task = task;
	}
	
	public Exam getExam() {
		return exam;
	}
	
	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAverageTime() {
		return averageTime;
	}

	public void setAverageTime(Integer averageTime) {
		this.averageTime = averageTime;
	}
	
}