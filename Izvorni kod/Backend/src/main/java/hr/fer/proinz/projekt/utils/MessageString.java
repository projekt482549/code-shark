package hr.fer.proinz.projekt.utils;

public class MessageString {

	private String message;

	public MessageString(String message) {
		super();
		this.setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
