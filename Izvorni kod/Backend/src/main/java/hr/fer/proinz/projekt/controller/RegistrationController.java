package hr.fer.proinz.projekt.controller;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.service.AppUserService;

@RestController
@RequestMapping("/")
public class RegistrationController {
	
	@Autowired
	private AppUserService appUserService;

	@PostMapping("/registration")
	public AppUser createAppUser(@RequestBody AppUserDto dto, HttpServletRequest request) throws UnsupportedEncodingException, MessagingException  {
		AppUser saved = appUserService.createUser(dto, getSiteURL(request));
		return saved;
	}
	
	@GetMapping("/verify")
	public String verifyUser(@Param("code") String code) {
	    if (appUserService.verify(code)) {
	        return "verify_success";
	    } else {
	        return "verify_fail";
	    }
	}
	
	private String getSiteURL(HttpServletRequest request) {
		String siteURL = request.getRequestURL().toString();
		return siteURL.replace(request.getServletPath(), "");
	}  
	 
	
}
