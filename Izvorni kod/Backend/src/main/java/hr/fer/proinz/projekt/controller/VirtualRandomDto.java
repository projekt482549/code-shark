package hr.fer.proinz.projekt.controller;

import java.sql.Time;

public class VirtualRandomDto {
	
	private Time duration;
	
	private Integer numOfTasks;

	public VirtualRandomDto(Time duration, Integer numOfTasks) {
		super();
		this.duration = duration;
		this.numOfTasks = numOfTasks;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public Integer getNumOfTasks() {
		return numOfTasks;
	}

	public void setNumOfTasks(Integer numOftasks) {
		this.numOfTasks = numOftasks;
	}
	
	
	
}
