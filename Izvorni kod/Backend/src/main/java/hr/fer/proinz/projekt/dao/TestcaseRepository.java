package hr.fer.proinz.projekt.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import hr.fer.proinz.projekt.entity.TestCase;

public interface TestcaseRepository extends JpaRepository<TestCase, Long>{

	@Query("select tc from TestCase tc where tc.task.taskId = ?1")
	public List<TestCase> findByTaskid(Long taskid);
	
	@Transactional
	@Modifying
	@Query("delete from TestCase tc where tc.task.taskId = ?1")
	public void deleteByTaskId(Long teskId);
	
}
