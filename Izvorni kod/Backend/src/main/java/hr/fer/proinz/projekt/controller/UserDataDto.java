package hr.fer.proinz.projekt.controller;

import hr.fer.proinz.projekt.entity.*;
import java.util.List;

public class UserDataDto {

    private int correctlySolvedTasks;

    private int triedTasks;
    private List<byte[]> trophyLinkList;

    private List<Task> taskList=null;

    private AppUser appUser;

    public UserDataDto(AppUser appUser, int solved, int tried, List<byte[]> trophies) {
        this.correctlySolvedTasks = solved;
        this.triedTasks = tried;
        this.trophyLinkList = trophies;
        this.appUser = appUser;
    }

    public int getCorrectlySolvedTasks() {
        return correctlySolvedTasks;
    }

    public void setCorrectlySolvedTasks(int correctlySolvedTasks) {
        this.correctlySolvedTasks = correctlySolvedTasks;
    }

    public int getTriedTasks() {
        return triedTasks;
    }

    public void setTriedTasks(int triedTasks) {
        this.triedTasks = triedTasks;
    }

    public List<byte[]> getTrophyLinkList() {
        return trophyLinkList;
    }

    public void setTrophyLinkList(List<byte[]> trophyLinkList) {
        this.trophyLinkList = trophyLinkList;	
    }


    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public AppUser getAppUser() {
		return appUser;
	}

	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

    public UserDataDto(int correctlySolvedTasks, int triedTasks, List<byte[]> trophyLinkList, List<Task> task, AppUser appUser) {
        this.correctlySolvedTasks = correctlySolvedTasks;
        this.triedTasks = triedTasks;
        this.trophyLinkList = trophyLinkList;
        this.taskList = task;
        this.appUser = appUser;
    }

    public UserDataDto(List<Task> taskList, AppUser appUser) {
        this.taskList = taskList;
        this.appUser = appUser;
    }
}