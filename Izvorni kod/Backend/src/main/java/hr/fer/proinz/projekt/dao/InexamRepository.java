package hr.fer.proinz.projekt.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import hr.fer.proinz.projekt.entity.Inexam;
import hr.fer.proinz.projekt.entity.InexamPK;

public interface InexamRepository extends JpaRepository<Inexam, InexamPK>{

	@Transactional
	@Modifying
	@Query("delete from Inexam ie where ie.exam.examId = ?1")
	public void deleteByExamId(Long examId);
}
