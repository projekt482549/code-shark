package hr.fer.proinz.projekt.utils;

public class JavaProcessInformation {

	private String stdout;
	
	private Long duration;
	
	private boolean compiled;

	public JavaProcessInformation(String stdout, Long duration, Boolean compiled) {
		super();
		this.stdout = stdout;
		this.duration = duration;
		this.setCompiled(compiled);
	}

	public String getStdout() {
		return stdout;
	}

	public Long getDuration() {
		return duration;
	}

	public boolean isCompiled() {
		return compiled;
	}

	public void setCompiled(boolean compiled) {
		this.compiled = compiled;
	}
	
}
