package hr.fer.proinz.projekt.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;

import hr.fer.proinz.projekt.controller.AppUserDto;
import hr.fer.proinz.projekt.controller.UserDataDto;
import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.utils.MessageString;

public interface AppUserService {
	
	public AppUser findByUsername(String username);

	public UserDataDto findUserData(String username);

	public List<AppUser> listAll();
	
	public AppUser findByEmail(String email);
	
	public AppUser createUser(AppUserDto dto, String siteUrl) throws UnsupportedEncodingException, MessagingException ;
	
	public boolean verify(String verificationCode);

	public List<AppUser> listAllNotEnabledByAdmin();

	public void enableAppUserAsAdmin(String username);

	public AppUser findUserDataV2(String username);

	public MessageString updateAppUser(AppUserDto appUser, String oldUsername);


	
}
