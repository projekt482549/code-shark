package hr.fer.proinz.projekt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Exam;
import hr.fer.proinz.projekt.entity.Task;
import hr.fer.proinz.projekt.service.AppUserService;
import hr.fer.proinz.projekt.utils.MessageString;

@RestController
@RequestMapping("/admin")
//@CrossOrigin(origins = "http://localhost:3000/profil")
public class AdminController {

	@Autowired
	private AppUserService appUserService;
	
//	@Autowired
//	private ExamService examService;
	
//	@Autowired
//	private TaskService taskService;
	
	@GetMapping("")
	@Secured({"ROLE_ADMIN"})
	public List<AppUser> listUsersNotEnabledByAdmin(){
		return appUserService.listAllNotEnabledByAdmin();
	}
	
	@PostMapping("/{username}")
	@Secured("ROLE_ADMIN")
	public MessageString changeAppUserData(@RequestBody AppUserDto appUser, @PathVariable("username") String oldUsername) {
		return appUserService.updateAppUser(appUser, oldUsername);
	}
	
	@GetMapping("/enable/{username}")
	@Secured("ROLE_ADMIN")
	public void enableAppUserAsAdmin(@PathVariable("username") String username) {
		appUserService.enableAppUserAsAdmin(username);
	}
	
	public List<Task> listAllTasks(){
		return null;
	}
	
	public List<Exam> listAllExams(){
		return null;
	}
}
