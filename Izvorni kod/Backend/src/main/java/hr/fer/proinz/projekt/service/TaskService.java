package hr.fer.proinz.projekt.service;

import java.util.List;

import hr.fer.proinz.projekt.controller.TaskEvalueteDto;
import hr.fer.proinz.projekt.controller.TaskNewDto;
import hr.fer.proinz.projekt.controller.TaskOnExamDto;
import hr.fer.proinz.projekt.entity.Score;
import hr.fer.proinz.projekt.entity.Task;
import hr.fer.proinz.projekt.utils.MessageString;

public interface TaskService {
	
	List<Task> getVoditeljTasks(String username);;

	public List<Task> listAll();
	
	public Task findByTaskid(Long taskid);

	
	public Task createTask(TaskNewDto taskDto);

	public TaskOnExamDto findTaskOnExam(Long examid, Integer taskno);

	public List<Task> findByUserid(Long userid);

	public Score evalueteAndScore(Long id, Integer taskno, TaskEvalueteDto taskFile, boolean fromExam);
	
	public Score evalueteAndScoreTask(Long taskid, TaskEvalueteDto taskFile);

	List<Task> getTasksFromExam(Long examid);

	TaskNewDto getTask(Long taskId);

	MessageString updateTask(Long taskId, TaskNewDto taskDto);
	
}
