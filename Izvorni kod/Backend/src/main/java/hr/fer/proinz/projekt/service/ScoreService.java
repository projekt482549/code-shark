package hr.fer.proinz.projekt.service;

import java.util.List;

import hr.fer.proinz.projekt.entity.Score;

public interface ScoreService {


	
	public List<Score> getUserScoresOnExam(Long userid, Long examid);

	public List<Score> getByExamid( long examid);

	public List<Score> getByUserid( long userid);
}
