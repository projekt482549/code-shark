package hr.fer.proinz.projekt.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "inexam")
public class Inexam {
	
	@EmbeddedId
	private InexamPK id;
	
	@MapsId("examid")
	@ManyToOne
	@JoinColumn(name = "examid", referencedColumnName = "examid")
	private Exam exam;
	
	@MapsId("taskid")
	@ManyToOne
	@JoinColumn(name = "taskid", referencedColumnName = "taskid")
	private Task task;
	
	@Column(name = "taskno", nullable = false)
	private Integer taskno;
	
	public Inexam() {
		
	}

	public Inexam(InexamPK id, Exam exam, Task task, Integer taskno) {
		super();
		this.id = id;
		this.exam = exam;
		this.task = task;
		this.taskno = taskno;
	}

	public InexamPK getId() {
		return id;
	}

	public void setId(InexamPK id) {
		this.id = id;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Integer getTaskno() {
		return taskno;
	}

	public void setTaskno(Integer taskno) {
		this.taskno = taskno;
	}
	
	
}
