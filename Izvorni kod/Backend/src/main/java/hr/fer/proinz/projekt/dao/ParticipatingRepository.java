package hr.fer.proinz.projekt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import hr.fer.proinz.projekt.entity.Exam;
import hr.fer.proinz.projekt.entity.Participating;
import hr.fer.proinz.projekt.entity.ParticipatingPK;

public interface ParticipatingRepository extends JpaRepository<Participating, ParticipatingPK>{

	@Query("select p.exam from Participating p where p.user.userId = ?1")
	List<Exam> findExamsByUserId(long userid);
}
