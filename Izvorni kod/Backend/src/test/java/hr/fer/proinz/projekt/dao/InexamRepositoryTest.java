package hr.fer.proinz.projekt.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
@Sql(statements = {
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (11, 'Creator', '12345678', 'creator@gmail.com', 'Creator', 'One', null, true, 1, 'ayvfHrrfgrEG')",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (11, 'Exam 1', 'Test exam', null, 10, current_timestamp - INTERVAL '1' HOUR, '02:00:00', 11, false, null)",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (12, 'Exam 2', 'Test exam 2', null, 10, current_timestamp - INTERVAL '1' DAY, '00:30:00', 11, false, null)",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (11, 'Task 1', 'Task 1 Description', 10, 11, false, 5000)",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (12, 'Task 2', 'Task 2 Description', 20, 11, false, 10000)",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (13, 'Task 3', 'Task 3 Description', 10, 11, true, 5000)",
        "insert into inexam (examid, taskid, taskno) values (11, 11, 1)",
        "insert into inexam (examid, taskid, taskno) values (11, 12, 2)",
        "insert into inexam (examid, taskid, taskno) values (12, 12, 1)",
        "insert into inexam (examid, taskid, taskno) values (12, 13, 2)"
})
public class InexamRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private InexamRepository repo;

    @Test
    void InexamRepositoryContext() {
        assertThat(repo.count()).isEqualTo(4);
        assertThat(repo.findAll()).hasSize(4);
    }

    @Test
    void deleteByExamIdTest() {
        repo.deleteByExamId(11L);
        entityManager.flush();

        Long foundNo = repo.count();
        assertThat(foundNo).isEqualTo(2);
    }

    @Test
    void deleteByExamId_NoExamTest() {
        repo.deleteByExamId(99L);
        entityManager.flush();

        Long foundNo = repo.count();
        assertThat(foundNo).isEqualTo(4);
    }


}
