package hr.fer.proinz.projekt.systemtests;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class LoginSystemTest {

    WebDriver driver;

    @BeforeEach
    void setup(){
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/login");//Postavljeno na local host kako bi podaci bili ispitani na lokalnom serveru koji je pod kontrolom testa, a ne na udaljenom poslužitelju
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testLoginGoodCreds() throws InterruptedException {

        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("admin1234");

        element = driver.findElement(By.name("password"));
        element.sendKeys("admin1234");

        driver.findElement(By.cssSelector("button[type='submit']")).click();

        Thread.sleep(1500);//Dodano radi dohvata podataka sa backenda i redirecta

        String redirURL = driver.getCurrentUrl();

        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("admin1234");
    }

    @Test
    public void testLoginBadCreds() {
        WebDriverWait wait = new WebDriverWait(driver,2);

        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("administrator");

        element = driver.findElement(By.name("password"));
        element.sendKeys("administrator1234");

        driver.findElement(By.cssSelector("button[type='submit']")).click();

        String alert = new String();

        try {
            wait.until(ExpectedConditions.alertIsPresent());
            alert = driver.switchTo().alert().getText();
            driver.switchTo().alert().dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }


        String redirURL = driver.getCurrentUrl();

        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        assertThat(redirURL).isEqualTo("http://localhost:3000/login");
        assertThat(nav_user_text).isEqualTo("Login");
        assertThat(alert).isEqualTo("Uneseni podaci nisu ispravni, pokušajte ponovo!");
    }

    @Test
    public void testLoginComplite() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver,2);

        WebElement elementUsername = driver.findElement(By.name("username"));
        elementUsername.sendKeys("administrator");

        WebElement elementPassword = driver.findElement(By.name("password"));
        elementPassword.sendKeys("administrator12345678");

        driver.findElement(By.cssSelector("button[type='submit']")).click();

        String alert = new String();

        try {
            wait.until(ExpectedConditions.alertIsPresent());
            alert = driver.switchTo().alert().getText();
            driver.switchTo().alert().dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String redirURL = driver.getCurrentUrl();

        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        assertThat(redirURL).isEqualTo("http://localhost:3000/login");
        assertThat(nav_user_text).isEqualTo("Login");
        assertThat(alert).isEqualTo("Uneseni podaci nisu ispravni, pokušajte ponovo!");

        driver.findElement(By.cssSelector("button[type='reset']")).click();

        elementUsername.sendKeys("admin1234");

        driver.findElement(By.cssSelector("button[type='submit']")).click();
        Thread.sleep(1500);

        redirURL = driver.getCurrentUrl();
        nav_user_text = nav_user.getText();

        assertThat(redirURL).isEqualTo("http://localhost:3000/login");
        assertThat(nav_user_text).isEqualTo("Login");

        elementPassword.sendKeys("admin1234");

        driver.findElement(By.cssSelector("button[type='submit']")).click();
        Thread.sleep(1500);

        redirURL = driver.getCurrentUrl();
        nav_user_text = nav_user.getText();

        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("admin1234");
    }
}
