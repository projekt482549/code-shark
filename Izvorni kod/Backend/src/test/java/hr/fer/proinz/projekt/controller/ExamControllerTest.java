package hr.fer.proinz.projekt.controller;

import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Exam;
import static org.mockito.Matchers.any;
import hr.fer.proinz.projekt.service.ExamService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ExamControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ExamService examService;

    @Test
    @WithMockUser(roles = "VODITELJ")
    void newCompetition() throws Exception {
        AppUser user = new AppUser("TestUser1", "12345678", "test@gmail.com", "Test", "Tester", null, false, 2, "1234");
        user.setUserId(1L);
        when(examService.createExam(any(ExamNewDto.class))).thenReturn(new Exam(user, "TestComp","New Test Competition", new byte[]{0}, 20,
                Timestamp.valueOf(LocalDateTime.of(Date.valueOf("2022-01-09").toLocalDate(), Time.valueOf("9:00:00").toLocalTime())), Time.valueOf("1:00:00"), false, null));

        mvc.perform(post("/exam")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"name\": \"Test\"," +
                                "\"description\": \"Test for Test\"," +
                                "\"date\": \"2022-01-09\"," +
                                "\"startTime\": \"08:00\"," +
                                "\"duration\": \"01:00:00\"," +
                                "\"cup\": \"0\"," +
                                "\"tasks\": [" +
                                "\"1\"," +
                                "\"2\"," +
                                "\"3\"," +
                                "\"4\"" +
                                "]" +
                                "}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"examId\":null,\"creator\":{\"userId\":1,\"username\":\"TestUser1\",\"password\":\"12345678\",\"email\":\"test@gmail.com\",\"name\":\"Test\",\"surname\":\"Tester\",\"photo\":null,\"enabled\":false,\"accounttype\":2,\"verificationCode\":\"1234\"},\"title\":\"TestComp\",\"description\":\"New Test Competition\",\"trophy\":\"AA==\",\"maxScore\":20,\"dateStart\":\"2022-01-09T08:00:00.000+00:00\",\"duration\":\"01:00:00\",\"virtualFlag\":false,\"originalExamFromVirtualExam\":null}"));
    }

    @Test
    @WithMockUser(roles = "VODITELJ")
    void getCompetition() throws Exception {
        when(examService.findById(1L)).thenReturn(new ExamDto(1L, "Test","Test for Test",new byte[]{0}, 10, Timestamp.valueOf(LocalDateTime.of(Date.valueOf("2022-01-09").toLocalDate(), Time.valueOf("9:00:00").toLocalTime())),Time.valueOf("1:00:00"),false,4));

        mvc.perform(get("/exam/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"examId\":1,\"title\":\"Test\",\"description\":\"Test for Test\",\"trophy\":\"AA==\",\"maxScore\":10,\"date\":\"2022-01-09T09:00:00\",\"duration\":\"01:00:00\",\"virtualFlag\":false,\"totalTasks\":4}"));
    }

    @Test
    @WithMockUser(roles = "NATJECATELJ")
    void createVirtualCompetition() throws Exception {
        when(examService.createVirtualExamBasedOnExam(1L)).thenReturn(new ExamDto(1L, "Test","Test for Test",new byte[]{0}, 10, Timestamp.valueOf(LocalDateTime.of(Date.valueOf("2022-01-09").toLocalDate(), Time.valueOf("9:00:00").toLocalTime())),Time.valueOf("1:00:00"),true,4));

        mvc.perform(get("/exam/1/newVirtual")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"examId\":1,\"title\":\"Test\",\"description\":\"Test for Test\",\"trophy\":\"AA==\",\"maxScore\":10,\"date\":\"2022-01-09T09:00:00\",\"duration\":\"01:00:00\",\"virtualFlag\":true,\"totalTasks\":4}"));
    }

    @Test
    @WithMockUser(roles = "NATJECATELJ")
    void createVirtualCompetitionRandom() throws Exception {
        when(examService.createVirtualExamRandom(any(VirtualRandomDto.class))).thenReturn(new ExamDto(99L, "Generated exam","Generated exam",null, 0, Timestamp.valueOf(LocalDateTime.of(Date.valueOf("2022-01-09").toLocalDate(), Time.valueOf("9:00:00").toLocalTime())),Time.valueOf("1:00:00"),true,4));

        mvc.perform(post("/exam/newVirtual")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"duration\":\"1:00:00\", \"numOfTasks\": 4}")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"examId\":99,\"title\":\"Generated exam\",\"description\":\"Generated exam\",\"trophy\":null,\"maxScore\":0,\"date\":\"2022-01-09T09:00:00\",\"duration\":\"01:00:00\",\"virtualFlag\":true,\"totalTasks\":4}"));
    }



}
