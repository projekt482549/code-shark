package hr.fer.proinz.projekt.systemtests;

import hr.fer.proinz.projekt.dao.AppUserRepository;
import hr.fer.proinz.projekt.entity.AppUser;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import static hr.fer.proinz.projekt.utils.UserAssert.assertThatAppUser;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class RegisterSystemTest {

    @Autowired
    private AppUserRepository appUserRepository;

    WebDriver driver;

    @BeforeEach
    void setup() {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/registration");
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testRegister_GoodRegistration() throws InterruptedException {

        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("probniNatjecatelj");

        element = driver.findElement(By.name("name"));
        element.sendKeys("Probni");

        element = driver.findElement(By.name("surname"));
        element.sendKeys("Natjecatelj");

        element = driver.findElement(By.name("email"));
        element.sendKeys("proba@gmail.com");

        element = driver.findElement(By.name("password"));
        element.sendKeys("password");

        driver.findElement(By.cssSelector("input[value='natjecatelj']")).click();

        element = driver.findElement(By.name("picture"));
        element.sendKeys(System.getProperty("user.dir") + "\\src\\test\\java\\hr\\fer\\proinz\\projekt\\testPicture.png");

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        AppUser newUser = appUserRepository.findByUsername("probniNatjecatelj");

        assertThat(redirURL).isEqualTo("http://localhost:3000/registration");
        assertThatAppUser(newUser)
                .isNotNull()
                .hasUsername("probniNatjecatelj")
                .hasName("Probni")
                .hasSurname("Natjecatelj")
                .hasEmail("proba@gmail.com")
                .hasEnabled(false)
                .hasAccounttype(0);
    }

    @Test
    public void testRegister_BadRegistration() throws InterruptedException {

        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("natjecatelj");

        element = driver.findElement(By.name("name"));
        element.sendKeys("Dupli");

        element = driver.findElement(By.name("surname"));
        element.sendKeys("Natjecatelj");

        element = driver.findElement(By.name("email"));
        element.sendKeys("natjecatelj@gmail.com");

        element = driver.findElement(By.name("password"));
        element.sendKeys("password");

        driver.findElement(By.cssSelector("input[value='natjecatelj']")).click();

        element = driver.findElement(By.name("picture"));
        element.sendKeys(System.getProperty("user.dir") + "\\src\\test\\java\\hr\\fer\\proinz\\projekt\\testPicture.png");

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        AppUser newUser = appUserRepository.findByUsername("probniNatjecatelj");

        assertThat(redirURL).isEqualTo("http://localhost:3000/login");
        assertThatAppUser(newUser)
                .isNull();
    }
}
