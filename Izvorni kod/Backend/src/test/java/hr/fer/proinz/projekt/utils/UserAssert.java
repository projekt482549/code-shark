package hr.fer.proinz.projekt.utils;

import hr.fer.proinz.projekt.entity.AppUser;
import org.assertj.core.api.AbstractAssert;
import static org.assertj.core.api.Assertions.assertThat;

public class UserAssert extends AbstractAssert<UserAssert, AppUser> {

    public UserAssert(AppUser actual) {
        super(actual, UserAssert.class);
    }

    public static UserAssert assertThatAppUser(AppUser actual) {
        return new UserAssert(actual);
    }

    public UserAssert hasUserId(Long userId) {
        isNotNull();
        assertThat(actual.getUserId())
                .overridingErrorMessage("Expected user id to be <%s> but was <%s>", userId, actual.getUserId())
                .isEqualTo(userId);
        return this;
    }

    public UserAssert hasUsername(String username) {
        isNotNull();
        assertThat(actual.getUsername())
                .overridingErrorMessage("Expected username to be <%s> but was <%s>", username, actual.getUsername())
                .isEqualTo(username);
        return this;
    }

    public UserAssert hasName(String name) {
        isNotNull();
        assertThat(actual.getName())
                .overridingErrorMessage("Expected name to be <%s> but was <%s>", name, actual.getName())
                .isEqualTo(name);
        return this;
    }

    public UserAssert hasSurname(String surname) {
        isNotNull();
        assertThat(actual.getSurname())
                .overridingErrorMessage("Expected surname to be <%s> but was <%s>", surname, actual.getSurname())
                .isEqualTo(surname);
        return this;
    }

    public UserAssert hasAccounttype(Integer accounttype) {
        isNotNull();
        assertThat(actual.getAccounttype())
                .overridingErrorMessage("Expected account type to be <%s> but was <%s>", accounttype, actual.getAccounttype())
                .isEqualTo(accounttype);
        return this;
    }

    public UserAssert hasPassword(String password) {
        isNotNull();
        assertThat(actual.getPassword())
                .overridingErrorMessage("Expected password to be <%s> but was <%s>", password, actual.getPassword())
                .isEqualTo(password);
        return this;
    }

    public UserAssert hasEmail(String email) {
        isNotNull();
        assertThat(actual.getEmail())
                .overridingErrorMessage("Expected email to be <%s> but was <%s>", email, actual.getEmail())
                .isEqualTo(email);
        return this;
    }

    public UserAssert hasPhoto(byte[] photo) {
        isNotNull();
        assertThat(actual.getPhoto())
                .overridingErrorMessage("Expected photo to be <%s> but was <%s>", photo, actual.getPhoto())
                .isEqualTo(photo);
        return this;
    }

    public UserAssert hasVerificationCode(String verif) {
        isNotNull();
        assertThat(actual.getVerificationCode())
                .overridingErrorMessage("Expected verification code to be <%s> but was <%s>", verif, actual.getVerificationCode())
                .isEqualTo(verif);
        return this;
    }

    public UserAssert hasEnabled(Boolean enabled) {
        isNotNull();
        assertThat(actual.getEnabled())
                .overridingErrorMessage("Expected enabled to be <%s> but was <%s>", enabled, actual.getEnabled())
                .isEqualTo(enabled);
        return this;
    }

}
