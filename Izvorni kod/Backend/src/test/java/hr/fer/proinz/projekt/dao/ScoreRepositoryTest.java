package hr.fer.proinz.projekt.dao;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

@DataJpaTest
@ActiveProfiles("test")
@Sql(statements = {
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (11, 'Creator', '12345678', 'creator@gmail.com', 'Creator', 'One', null, true, 1, 'ayvfHrrfgrEG')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (12, 'Competitor1', '12345678', 'competitor1@gmail.com', 'Competitor', 'One', null, true, 0, 'yfbafafVdDyFb')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (13, 'Competitor2', '12345678', 'competitor2@gmail.com', 'Competitor', 'Two', null, true, 0, 'jxghsygsgnm652')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (14, 'Competitor3', '12345678', 'competitor3@gmail.com', 'Competitor', 'Three', null, true, 0, 'sh5s16sgn15sb')",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (11, 'Exam 1', 'Test exam', null, 10, current_timestamp - INTERVAL '1' HOUR, '02:00:00', 11, false, null)",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (12, 'Exam 2', 'Test exam 2', null, 10, current_timestamp - INTERVAL '1' DAY, '00:30:00', 11, false, null)",
        "insert into participating (userid, examid) values(12, 11)",
        "insert into participating (userid, examid) values(12, 12)",
        "insert into participating (userid, examid) values(13, 12)"
})
public class ScoreRepositoryTest {
}
