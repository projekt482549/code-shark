package hr.fer.proinz.projekt.systemtests;

import hr.fer.proinz.projekt.dao.TaskRepository;
import hr.fer.proinz.projekt.entity.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class OrganizerChangeTaskTest {

    @Autowired
    private TaskRepository taskRepository;

    WebDriver driver;

    @BeforeEach
    void setup() throws InterruptedException {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/login");

        //Login
        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("voditelj");
        element = driver.findElement(By.name("password"));
        element.sendKeys("voditelj");
        driver.findElement(By.cssSelector("button[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        //Provjera Logina
        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("voditelj");
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testChangeTask_allTasks() throws InterruptedException {
        Actions action = new Actions(driver);

        //Provjera da li smo na pravoj stranici
        action.moveToElement(driver.findElement(By.cssSelector("a[href*='/profil/']"))).perform();

        Thread.sleep(1000);

        driver.findElement(By.cssSelector("a[href*='/zadaci']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/zadaci");

        Thread.sleep(1000);

        driver.findElement(By.cssSelector("a[href='/uredivanjeZadatka/101']")).click();

        Thread.sleep(2000);

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/uredivanjeZadatka/101");

        WebElement element = driver.findElement(By.name("nameTask"));
        element.clear();
        element.sendKeys("Novi Novi zadatak");

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(2500);

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/zadaci/101");

        Task newTask = taskRepository.getById(101L);

        assertThat(newTask.getTitle()).isEqualTo("Novi Novi zadatak");
        assertThat(newTask.getDescription()).isEqualTo("T1 opis");
        assertThat(newTask.getMscore()).isEqualTo(3);
        assertThat(newTask.getTimeThreshold()).isEqualTo(5000);
    }

    @Test
    public void testChangeTask_profile() throws InterruptedException {

        //Provjera da li smo na pravoj stranici
        driver.findElement(By.cssSelector("a[href*='/profil/']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/profil/voditelj");

        Thread.sleep(1000);

        driver.findElement(By.cssSelector("a[href='/uredivanjeZadatka/101']")).click();

        Thread.sleep(2000);

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/uredivanjeZadatka/101");

        WebElement element = driver.findElement(By.name("nameTask"));
        element.clear();
        element.sendKeys("Novi Novi zadatak");

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(2500);

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/zadaci/101");

        Task newTask = taskRepository.getById(101L);

        assertThat(newTask.getTitle()).isEqualTo("Novi Novi zadatak");
        assertThat(newTask.getDescription()).isEqualTo("T1 opis");
        assertThat(newTask.getMscore()).isEqualTo(3);
        assertThat(newTask.getTimeThreshold()).isEqualTo(5000);
    }
}
