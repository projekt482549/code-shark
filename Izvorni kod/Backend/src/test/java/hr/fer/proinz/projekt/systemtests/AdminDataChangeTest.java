package hr.fer.proinz.projekt.systemtests;

import hr.fer.proinz.projekt.dao.AppUserRepository;
import hr.fer.proinz.projekt.entity.AppUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static hr.fer.proinz.projekt.utils.UserAssert.assertThatAppUser;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class AdminDataChangeTest {

    @Autowired
    private AppUserRepository appUserRepository;

    WebDriver driver;

    @BeforeEach
    void setup() throws InterruptedException {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/login");

        //Login
        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("admin1234");
        element = driver.findElement(By.name("password"));
        element.sendKeys("admin1234");
        driver.findElement(By.cssSelector("button[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        //Provjera Logina
        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("admin1234");
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testDataChange_GoodChange() throws InterruptedException {

        //Provjera da li smo na pravoj stranici
        driver.findElement(By.cssSelector("a[href='/korisnici']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/korisnici");

        Thread.sleep(1500);

        driver.findElement(By.cssSelector("input[value='Uredi podatke']")).click();

        Thread.sleep(1500);

        //Provjeri jesmo li ušli u uređivanje usera
        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/uredivanjePodataka/");

        //Provjeri odgovaraju li podaci onima u bazi
        String username = redirURL.split("/")[redirURL.split("/").length-1];
        AppUser checkUser = appUserRepository.findByUsername(username);

        WebElement elementUsername =  driver.findElement(By.name("username"));
        String oldUsername = elementUsername.getAttribute("value");
        WebElement elementName =  driver.findElement(By.name("name"));
        String oldName = elementName.getAttribute("value");
        WebElement elementSurname =  driver.findElement(By.name("surname"));
        String oldSurName = elementSurname.getAttribute("value");
        WebElement elementEmail =  driver.findElement(By.name("email"));
        String oldEmail = elementEmail.getAttribute("value");
        WebElement elementPassword =  driver.findElement(By.name("password"));

        assertThatAppUser(checkUser)
                .hasUsername(oldUsername)
                .hasName(oldName)
                .hasSurname(oldSurName)
                .hasEmail(oldEmail);

        //Provjera promjene podataka
        elementUsername.clear();
        elementUsername.sendKeys("natjecateljNovi");
        elementPassword.clear();
        elementPassword.sendKeys("natjecateljNovi");
        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(1500);

        checkUser = appUserRepository.findByUsername("natjecateljNovi");
        assertThatAppUser(checkUser)
                .hasUsername("natjecateljNovi")
                .hasName(oldName)
                .hasSurname(oldSurName)
                .hasEmail(oldEmail);

        //Provjera jesmo li preusmjereni
        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/korisnici");
    }

    @Test
    public void testDataChange_BadChange() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,2);

        //Provjera da li smo na pravoj stranici
        driver.findElement(By.cssSelector("a[href='/korisnici']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/korisnici");

        Thread.sleep(1500);

        driver.findElement(By.cssSelector("input[value='Uredi podatke']")).click();

        Thread.sleep(1500);

        //Provjeri jesmo li ušli u uređivanje usera
        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/uredivanjePodataka/");

        //Provjeri odgovaraju li podaci onima u bazi
        String username = redirURL.split("/")[redirURL.split("/").length-1];
        AppUser checkUser = appUserRepository.findByUsername(username);

        WebElement elementUsername =  driver.findElement(By.name("username"));
        String oldUsername = elementUsername.getAttribute("value");
        WebElement elementName =  driver.findElement(By.name("name"));
        String oldName = elementName.getAttribute("value");
        WebElement elementSurname =  driver.findElement(By.name("surname"));
        String oldSurName = elementSurname.getAttribute("value");
        WebElement elementEmail =  driver.findElement(By.name("email"));
        String oldEmail = elementEmail.getAttribute("value");
        WebElement elementPassword =  driver.findElement(By.name("password"));

        assertThatAppUser(checkUser)
                .hasUsername(oldUsername)
                .hasName(oldName)
                .hasSurname(oldSurName)
                .hasEmail(oldEmail);

        //Provjera promjene podataka (username)
        elementUsername.clear();
        elementUsername.sendKeys("natjecateljN");
        elementPassword.clear();
        elementPassword.sendKeys("natjecateljN");
        driver.findElement(By.cssSelector("input[type='submit']")).click();

        String alert = new String();

        try {
            wait.until(ExpectedConditions.alertIsPresent());
            alert = driver.switchTo().alert().getText();
            driver.switchTo().alert().dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertThat(alert).isEqualTo("Odabrani username je zauzet");

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/uredivanjePodataka/");

        //Provjera promjene podataka (email)
        elementUsername.clear();
        elementUsername.sendKeys("natjecateljNovi");
        elementPassword.clear();
        elementPassword.sendKeys("natjecateljNovi");
        elementEmail.clear();
        elementEmail.sendKeys("wantToBeCompetitor@yahoo.com");
        driver.findElement(By.cssSelector("input[type='submit']")).click();

        alert = new String();

        try {
            wait.until(ExpectedConditions.alertIsPresent());
            alert = driver.switchTo().alert().getText();
            driver.switchTo().alert().dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertThat(alert).isEqualTo("Odabrani email je zauzet");

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/uredivanjePodataka/");
    }
}
