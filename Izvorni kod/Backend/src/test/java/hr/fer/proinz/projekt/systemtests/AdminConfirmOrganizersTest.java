package hr.fer.proinz.projekt.systemtests;

import hr.fer.proinz.projekt.dao.AppUserRepository;
import hr.fer.proinz.projekt.entity.AppUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static hr.fer.proinz.projekt.utils.UserAssert.assertThatAppUser;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class AdminConfirmOrganizersTest {

    @Autowired
    private AppUserRepository appUserRepository;

    WebDriver driver;

    @BeforeEach
    void setup() throws InterruptedException {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/login");

        //Login
        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("admin1234");
        element = driver.findElement(By.name("password"));
        element.sendKeys("admin1234");
        driver.findElement(By.cssSelector("button[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        //Provjera Logina
        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("admin1234");
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testConfirmOrganizer() throws InterruptedException {

        //Provjera da li smo na pravoj stranici
        driver.findElement(By.cssSelector("a[href='/admin']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/admin");

        Thread.sleep(1500);

        AppUser checkUser = appUserRepository.findByUsername("voditeljC");

        assertThatAppUser(checkUser)
                .hasUsername("voditeljC")
                .hasName("Mogući")
                .hasSurname("Voditelj")
                .hasEmail("voditelj.moguci@gmail.com")
                .hasEnabled(true)
                .hasAccounttype(3);

        driver.findElement(By.className("acceptButton")).click();

        Thread.sleep(1500);

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/admin");

        //Provjeri da li je voditeljC potvrđen (promjena Accounttype iz 3 u 1)
        checkUser = appUserRepository.findByUsername("voditeljC");

        assertThatAppUser(checkUser)
                .hasUsername("voditeljC")
                .hasName("Mogući")
                .hasSurname("Voditelj")
                .hasEmail("voditelj.moguci@gmail.com")
                .hasEnabled(true)
                .hasAccounttype(1);

    }
}
