package hr.fer.proinz.projekt.controller;

import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.service.ExamService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ExamsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ExamService examService;


    @Test
    @WithMockUser(roles = "VODITELJ")
    void noCompetitions() throws Exception {
        when(examService.getExamsForYearAndMonth(2022, 1)).thenReturn(List.of());

        mvc.perform(get("/exams")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    @WithMockUser(roles = "VODITELJ")
    void twoCompetitions() throws Exception {
        AppUser user = new AppUser("TestUser1", "12345678", "test@gmail.com", "Test", "Tester", null, false, 2, "1234");
        user.setUserId(1L);

        ExamsDto e1 = new ExamsDto(1L, user, "Test","Test for Test", Timestamp.valueOf(LocalDateTime.of(Date.valueOf("2022-01-09").toLocalDate(), Time.valueOf("09:00:00").toLocalTime())),Time.valueOf("01:00:00"));
        ExamsDto e2 = new ExamsDto(2L, user, "Test2","Test for 2", Timestamp.valueOf(LocalDateTime.of(Date.valueOf("2022-01-09").toLocalDate(), Time.valueOf("10:00:00").toLocalTime())),Time.valueOf("00:30:00"));

        when(examService.getExamsForYearAndMonth(2022, 1)).thenReturn(List.of(e1,e2));

        mvc.perform(get("/exams/2022/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"examId\":1,\"creator\":{\"userId\":1,\"username\":\"TestUser1\",\"password\":\"12345678\",\"email\":\"test@gmail.com\",\"name\":\"Test\",\"surname\":\"Tester\",\"photo\":null,\"enabled\":false,\"accounttype\":2,\"verificationCode\":\"1234\"},\"title\":\"Test\",\"description\":\"Test for Test\",\"date\":\"2022-01-09T08:00:00.000+00:00\",\"duration\":\"01:00:00\"},{\"examId\":2,\"creator\":{\"userId\":1,\"username\":\"TestUser1\",\"password\":\"12345678\",\"email\":\"test@gmail.com\",\"name\":\"Test\",\"surname\":\"Tester\",\"photo\":null,\"enabled\":false,\"accounttype\":2,\"verificationCode\":\"1234\"},\"title\":\"Test2\",\"description\":\"Test for 2\",\"date\":\"2022-01-09T09:00:00.000+00:00\",\"duration\":\"00:30:00\"}]"));
    }
}
