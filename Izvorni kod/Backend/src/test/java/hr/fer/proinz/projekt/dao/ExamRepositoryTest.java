package hr.fer.proinz.projekt.dao;

import hr.fer.proinz.projekt.entity.Exam;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
@Sql(statements = {
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (11, 'Creator', '12345678', 'creator@gmail.com', 'Creator', 'One', null, true, 1, 'ayvfHrrfgrEG')",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (11, 'Exam 1', 'Test exam', null, 10, current_timestamp - INTERVAL '1' HOUR, '02:00:00', 11, false, null)",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (12, 'Exam 2', 'Test exam 2', null, 10, current_timestamp - INTERVAL '1' DAY, '00:30:00', 11, false, null)"
})
public class ExamRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ExamRepository repo;

    @Test
    void ExamRepositoryContext() {
        assertThat(repo.count()).isEqualTo(2);
        assertThat(repo.findAll()).hasSize(2);
    }

    @Test
    void deleteExamTest() {
        repo.deleteExam(11L);
        entityManager.flush();
        Optional<Exam> found = repo.findById(11L);

        assertThat(found).isNotPresent();
    }

    @Test
    void deleteExam_NoExamTest() {
        repo.deleteExam(99L);
        entityManager.flush();
        Optional<Exam> found = repo.findById(99L);

        assertThat(found).isNotPresent();
    }

}
