DROP TABLE IF EXISTS users cascade;
DROP TABLE IF EXISTS exam cascade;
DROP TABLE IF EXISTS task cascade;
DROP TABLE IF EXISTS score cascade;
DROP TABLE IF EXISTS participating cascade;
DROP TABLE IF EXISTS inexam cascade;
DROP TABLE IF EXISTS testcase cascade;

CREATE TABLE users
(
  userid INT NOT NULL,
  username VARCHAR(20) NOT NULL,
  password VARCHAR(100) NOT NULL ,
  email VARCHAR(320) NOT NULL,
  name VARCHAR(50) NOT NULL,
  surname VARCHAR(50) NOT NULL,
  photo BYTEA,
  enabled BOOLEAN NOT NULL,
  accounttype INT NOT NULL,
  verification_code VARCHAR(255),
  PRIMARY KEY (userid),
  UNIQUE (username),
  UNIQUE (email)
);

CREATE TABLE exam
(
  examid INT NOT NULL,
  title VARCHAR(100) NOT NULL,
  description VARCHAR(5000) NOT NULL,
  trophy BYTEA,
  maxscore INT NOT NULL,
  datestart TIMESTAMP NOT NULL,
  duration TIME NOT NULL,
  userid INT NOT NULL,
  virtualflag BOOLEAN,
  virtualexamid INT,
  PRIMARY KEY (examid),
  FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE,
  FOREIGN KEY (virtualexamid) REFERENCES exam(examid) ON DELETE CASCADE
);

CREATE TABLE task
(
  taskid INT NOT NULL,
  title VARCHAR(100) NOT NULL,
  description VARCHAR(5000) NOT NULL,
  mscore INT NOT NULL,
  userid INT NOT NULL,
  privatetask BOOLEAN NOT NULL,
  timethreshold INT NOT NULL,
  PRIMARY KEY (taskid),
  FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE
);

CREATE TABLE score
(
  scoreid INT NOT NULL,
  answer BYTEA NOT NULL,
  score INT NOT NULL,
  taskid INT NOT NULL,
  examid INT NOT NULL,
  userid INT NOT NULL,
  averagetime INT NOT NULL,
  PRIMARY KEY (scoreid),
  FOREIGN KEY (taskid) REFERENCES task(taskid) ON DELETE CASCADE,
  FOREIGN KEY (examid) REFERENCES exam(examid) ON DELETE CASCADE,
  FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE
);

CREATE TABLE inexam
(
  examid INT NOT NULL,
  taskid INT NOT NULL,
  taskno INT NOT NULL,
  PRIMARY KEY (examid, taskid),
  FOREIGN KEY (examid) REFERENCES exam(examid) ON DELETE CASCADE,
  FOREIGN KEY (taskid) REFERENCES task(taskid) ON DELETE CASCADE
);

CREATE TABLE participating
(
  userid INT NOT NULL,
  examid INT NOT NULL,
  PRIMARY KEY (userid, examid),
  FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE,
  FOREIGN KEY (examid) REFERENCES exam(examid) ON DELETE CASCADE
);

CREATE TABLE testcase
(
  testid INT NOT NULL,
  input VARCHAR(5000) NOT NULL,
  output VARCHAR(5000) NOT NULL,
  taskid INT NOT NULL,
  PRIMARY KEY (testid),
  FOREIGN KEY (taskid) REFERENCES task(taskid) ON DELETE CASCADE
);
