package hr.fer.proinz.projekt.service;

import hr.fer.proinz.projekt.dao.ScoreRepository;
import hr.fer.proinz.projekt.entity.Score;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql"},
    statements = {
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (11, 'Creator', '12345678', 'creator@gmail.com', 'Creator', 'One', null, true, 1, 'ayvfHrrfgrEG')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (12, 'Competitor1', '12345678', 'competitor1@gmail.com', 'Competitor', 'One', null, true, 0, 'yfbafafVdDyFb')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (13, 'Competitor2', '12345678', 'competitor2@gmail.com', 'Competitor', 'Two', null, true, 0, 'jxghsygsgnm652')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (14, 'Competitor3', '12345678', 'competitor3@gmail.com', 'Competitor', 'Three', null, true, 0, 'sh5s16sgn15sb')",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (11, 'Exam 1', 'Test exam', null, 10, current_timestamp - INTERVAL '1' HOUR, '02:00:00', 11, false, null)",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (12, 'Exam 2', 'Test exam 2', null, 10, current_timestamp - INTERVAL '1' DAY, '00:30:00', 11, false, null)",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (11, 'Task 1', 'Task 1 Description', 10, 11, false, 5000)",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (12, 'Task 2', 'Task 2 Description', 20, 11, false, 10000)",
        "insert into score (scoreid, answer, score, taskid, examid, userid, averagetime) values (11, 'A1', 10, 11, 11, 12, 10000)",
        "insert into score (scoreid, answer, score, taskid, examid, userid, averagetime) values (12, 'A2', 15, 12, 11, 12, 15000)",
        "insert into score (scoreid, answer, score, taskid, examid, userid, averagetime) values (13, 'A3', 15, 12, 12, 12, 12000)",
        "insert into score (scoreid, answer, score, taskid, examid, userid, averagetime) values (14, 'A4', 12, 12, 12, 13, 13200)"
})
public class ScoreServiceTest {

    @Autowired
    private ScoreRepository repo;

    @Autowired
    private ScoreService service;

    @Test
    @Transactional
    void getUserScoresOnExamTest() {
        List<Score> testScore = service.getUserScoresOnExam(12L, 11L);

        Optional<Score> sc1 = repo.findById(11L);
        Optional<Score> sc2 = repo.findById(12L);

        assertThat(testScore)
                .isNotNull()
                .hasSize(2)
                .contains(sc1.get())
                .contains(sc2.get());
    }

    @Test
    @Transactional
    void getUserScoresOnExam_NoExamTest() {
        List<Score> testScore = service.getUserScoresOnExam(12L, 99L);

        assertThat(testScore)
                .isEqualTo(List.of());
    }

    @Test
    @Transactional
    void getUserScoresOnExam_NoUserTest() {
        List<Score> testScore = service.getUserScoresOnExam(99L, 11L);

        assertThat(testScore)
                .isEqualTo(List.of());
    }

    @Test
    @Transactional
    void getByExamIdTest() {
        List<Score> testScore = service.getByExamid(12L);

        Optional<Score> sc1 = repo.findById(13L);
        Optional<Score> sc2 = repo.findById(14L);

        assertThat(testScore)
                .isNotNull()
                .hasSize(2)
                .contains(sc1.get())
                .contains(sc2.get());
    }

    @Test
    @Transactional
    void getByExamId_NoExamTest() {
        List<Score> testScore = service.getByExamid(99L);

        assertThat(testScore)
                .isEqualTo(List.of());
    }

    @Test
    @Transactional
    void getByUserIdTest() {
        List<Score> testScore = service.getByUserid(12L);

        Optional<Score> sc1 = repo.findById(11L);
        Optional<Score> sc2 = repo.findById(12L);
        Optional<Score> sc3 = repo.findById(13L);

        assertThat(testScore)
                .isNotNull()
                .hasSize(3)
                .contains(sc1.get())
                .contains(sc2.get())
                .contains(sc3.get());
    }

    @Test
    @Transactional
    void getByUserid_NoUserTest() {
        List<Score> testScore = service.getByUserid(99L);

        assertThat(testScore)
                .isEqualTo(List.of());
    }

}
