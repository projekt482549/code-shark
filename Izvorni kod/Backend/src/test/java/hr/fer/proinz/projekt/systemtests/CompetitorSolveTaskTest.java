package hr.fer.proinz.projekt.systemtests;

import hr.fer.proinz.projekt.dao.ParticipatingRepository;
import hr.fer.proinz.projekt.dao.TaskRepository;
import hr.fer.proinz.projekt.entity.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class CompetitorSolveTaskTest {

    @Autowired
    private TaskRepository taskRepository;

    WebDriver driver;

    @BeforeEach
    void setup() throws InterruptedException {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/login");

        //Login
        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("natjecatelj");
        element = driver.findElement(By.name("password"));
        element.sendKeys("natjecatelj");
        driver.findElement(By.cssSelector("button[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        //Provjera Logina
        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("natjecatelj");
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testSolveTask() throws InterruptedException {
        Actions action = new Actions(driver);

        //Provjera da li smo na pravoj stranici
        action.moveToElement(driver.findElement(By.cssSelector("a[href*='/profil/']"))).perform();

        Thread.sleep(1000);

        driver.findElement(By.cssSelector("a[href='/zadaci']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/zadaci");

        Thread.sleep(1500);

        //Odaberi prvi zadatak
        driver.findElement(By.cssSelector("a[href='/zadaci/101']")).click();

        Thread.sleep(1500);

        //Provjeri jesmo li ušli u uređivanje usera
        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/zadaci/101");

        //Provjeri odgovaraju li podaci onima u bazi
        Task checkTask = taskRepository.findByTaskId(101L);

        WebElement elementTitle =  driver.findElement(By.className("competitionTitle"));
        String taskTitle = elementTitle.getText();
        WebElement elementInfo =  driver.findElement(By.className("competitionInfo"));
        String taskInfo = elementInfo.getText();

        assertThat(checkTask).isNotNull();
        assertThat(taskTitle).isEqualTo(checkTask.getTitle());
        assertThat(taskInfo).contains(checkTask.getDescription());
        assertThat(taskInfo).contains(checkTask.getMscore().toString());

        //Upload
        WebElement elementUpload =  driver.findElement(By.id("file-input"));
        elementUpload.sendKeys(System.getProperty("user.dir") + "\\src\\test\\java\\hr\\fer\\proinz\\projekt\\testCode.java");

        Thread.sleep(2000);

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(15000);

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/zadaci/101");

        WebElement elementScore =  driver.findElement(By.id("test_score"));
        String score = elementScore.getText();

        assertThat(score).isEqualTo("1");
    }
}
