insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (100, 'admin1234', '$2a$10$zq8T7Sx0ANamRFi9/nYDUuYJ31305satsJvc2GfwafmS53GoLhnKu', 'glavni.admin@fer.hr', 'Admin', 'Adminović', null, true, 2, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (101, 'adminJunior', '$2a$10$Xd6HJLowupdxgD3xrprK2eP6tb8yCp3FuabkIVsIC2FXarT93NzG2', 'admin.junior@fer.hr', 'Admin', 'Junior', null, true, 2, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (102, 'natjecatelj', '$2a$10$8nfT26Mn1n6DrWWFh5XjReavgm.4gg7G3XpcfLzJCPimYgMxQXWyO', 'natjecatelj@gmail.com', 'Natjecatelj', 'Prvić', null, true, 0, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (103, 'natjecateljD', '$2a$10$vZDHopMVkn5XVWa56ylP5eQcD0atNfgPxlFQK3f6Sz1NBOh/G.EWy', 'natjecateljD@gmail.com', 'Natjecatelj', 'Drugić', null, true, 0, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (104, 'natjecateljN', '$2a$10$n9RHT6T9vhWPfDe.PCotjuS6NSZwVN2XxbD7/Nnn7I8ID43m7YI0K', 'wantToBeCompetitor@yahoo.com', 'Nije Još', 'Natjecatelj', null, false, 0, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (105, 'voditelj', '$2a$10$xpzQcWdbrgWpLbDcyAp7HuTpeKVFhjAXW38/wli4WOLDotqg4izsm', 'voditelj.glavni@fer.hr', 'Voditelj', 'Glavnić', null, true, 1, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (106, 'voditeljS', '$2a$10$wxliU5F3CWZErrTBdK8IB.tWhom46nfov9674NN748yjqBSVvWm6O', 'voditelj.sekundić@gmail.com', 'Voditelj', 'Sekundić', null, true, 1, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (107, 'voditeljN', '$2a$10$IZUd7ciLl8X9.pc5w64Ewu0IEysDU0yVuVE8LhdXk4AiPB3baUiCW', 'wantToBeLeader@yahoo.com', 'Voditelj', 'Potencialni', null, false, 3, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (108, 'voditeljC', '$2a$10$el25FrHd0Ch9g.bGG/W1uOwiNMO.zrKfmbvpuqepcKv6IEdWflCU2', 'voditelj.moguci@gmail.com', 'Mogući', 'Voditelj', null, true, 3, null);
insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (109, 'adminN123', '$2a$10$DsY2C00pDZX3sLIEEQs8ZufE8DqAE5J34PMNhelwSKvATunqDMMC6', 'nemoguci.admin@gmail.com', 'Admin', 'Nemogučko', null, false, 2, null);

insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (101, 'Exam1', 'Opis1', null, 2, current_timestamp - INTERVAL '1' DAY, '02:00:00', 105, false, null);
insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (102, 'Exam2', 'Opis2', null, 2, current_timestamp - INTERVAL '1' DAY, '02:00:00', 105, false, null);
insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (103, 'Exam3', 'Opis3', null, 2, current_timestamp + INTERVAL '1' DAY, '00:30:00', 105, false, null);

insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (101, 'T1', 'T1 opis', 3, 105, false, 5000);
insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (102, 'T2', 'T2 opis', 10, 105, false, 5000);
insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (103, 'T3', 'T3 opis', 10, 105, false, 5000);
insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (104, 'T4', 'T4 opis', 10, 105, true, 5000);
insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (105, 'T5', 'T5 opis', 10, 106, true, 5000);

insert into inexam (examid, taskid, taskno) values (101, 101, 1);
insert into inexam (examid, taskid, taskno) values (101, 102, 2);
insert into inexam (examid, taskid, taskno) values (101, 103, 3);
insert into inexam (examid, taskid, taskno) values (101, 104, 4);
insert into inexam (examid, taskid, taskno) values (102, 101, 1);
insert into inexam (examid, taskid, taskno) values (101, 105, 5);
insert into inexam (examid, taskid, taskno) values (103, 105, 1);
insert into inexam (examid, taskid, taskno) values (103, 101, 2);

insert into participating (userid, examid) values(102, 101);
insert into participating (userid, examid) values(102, 102);
insert into participating (userid, examid) values(103, 102);

insert into testcase (testid, input, output, taskid) values (101, 'Input1', 'Output1', 101);
insert into testcase (testid, input, output, taskid) values (102, 'Input2', 'Output2', 101);
insert into testcase (testid, input, output, taskid) values (103, 'Input2', 'Output2', 101);
