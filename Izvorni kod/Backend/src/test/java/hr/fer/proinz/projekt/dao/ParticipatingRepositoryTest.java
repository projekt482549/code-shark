package hr.fer.proinz.projekt.dao;

import hr.fer.proinz.projekt.entity.Exam;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
@Sql(statements = {
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (11, 'Creator', '12345678', 'creator@gmail.com', 'Creator', 'One', null, true, 1, 'ayvfHrrfgrEG')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (12, 'Competitor1', '12345678', 'competitor1@gmail.com', 'Competitor', 'One', null, true, 0, 'yfbafafVdDyFb')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (13, 'Competitor2', '12345678', 'competitor2@gmail.com', 'Competitor', 'Two', null, true, 0, 'jxghsygsgnm652')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (14, 'Competitor3', '12345678', 'competitor3@gmail.com', 'Competitor', 'Three', null, true, 0, 'sh5s16sgn15sb')",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (11, 'Exam 1', 'Test exam', null, 10, current_timestamp - INTERVAL '1' HOUR, '02:00:00', 11, false, null)",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (12, 'Exam 2', 'Test exam 2', null, 10, current_timestamp - INTERVAL '1' DAY, '00:30:00', 11, false, null)",
        "insert into participating (userid, examid) values(12, 11)",
        "insert into participating (userid, examid) values(12, 12)",
        "insert into participating (userid, examid) values(13, 12)"
})
public class ParticipatingRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ParticipatingRepository repo;

    @Autowired
    private ExamRepository examRepo;

    @Test
    void ParticipatingRepositoryContext() {
        assertThat(repo.count()).isEqualTo(3);
        assertThat(repo.findAll()).hasSize(3);
    }

    @Test
    void findExamsByUserIdTest() {
        List<Exam> testExams = repo.findExamsByUserId(12L);
        entityManager.flush();

        Optional<Exam> e1 = examRepo.findById(11L);
        Optional<Exam> e2 = examRepo.findById(12L);

        assertThat(testExams)
                .isNotNull()
                .hasSize(2)
                .contains(e1.get())
                .contains(e2.get());
    }

    @Test
    void findExamsByUserId_NoUserTest() {
        List<Exam> testExams = repo.findExamsByUserId(99L);
        entityManager.flush();

        assertThat(testExams)
                .isEqualTo(List.of());
    }

    @Test
    void findExamsByUserId_NoExamTest() {
        List<Exam> testExams = repo.findExamsByUserId(14L);
        entityManager.flush();

        assertThat(testExams)
                .isEqualTo(List.of());
    }


}
