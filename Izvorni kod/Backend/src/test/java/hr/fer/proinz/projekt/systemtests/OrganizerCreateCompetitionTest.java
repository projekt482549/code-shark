package hr.fer.proinz.projekt.systemtests;

import hr.fer.proinz.projekt.dao.ExamRepository;
import hr.fer.proinz.projekt.entity.Exam;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class OrganizerCreateCompetitionTest {

    @Autowired
    private ExamRepository examRepository;

    WebDriver driver;

    @BeforeEach
    void setup() throws InterruptedException {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/login");

        //Login
        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("voditelj");
        element = driver.findElement(By.name("password"));
        element.sendKeys("voditelj");
        driver.findElement(By.cssSelector("button[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        //Provjera Logina
        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("voditelj");
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testCreateCompatition() throws InterruptedException {
        Actions action = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver,2);

        //Provjera da li smo na pravoj stranici
        action.moveToElement(driver.findElement(By.cssSelector("a[href*='/profil/']"))).perform();

        Thread.sleep(1000);

        driver.findElement(By.cssSelector("a[href*='/novoNatjecanje/']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/novoNatjecanje/");

        Thread.sleep(1000);

        WebElement element = driver.findElement(By.name("name"));
        element.sendKeys("Novo Natjecanje");

        element = driver.findElement(By.id("description"));
        element.sendKeys("Tekst novog natjecanja");

        element = driver.findElement(By.name("date"));
        element.clear();
        element.sendKeys(java.time.LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));

        element = driver.findElement(By.name("startTime"));
        element.clear();
        element.sendKeys("10:00");

        element = driver.findElement(By.name("endTime"));
        element.clear();
        element.sendKeys("11:00");

        element = driver.findElement(By.cssSelector("input[type=file]"));
        element.sendKeys(System.getProperty("user.dir") + "\\src\\test\\java\\hr\\fer\\proinz\\projekt\\testPicture.png");

        element = driver.findElement(By.cssSelector("input[value='101']"));
        element.click();

        element = driver.findElement(By.cssSelector("input[value='102']"));
        element.click();

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(1000);

        String alert = new String();
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            alert = driver.switchTo().alert().getText();
            driver.switchTo().alert().dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Thread.sleep(1000);

        assertThat(alert).isEqualTo("Uspješno napravljeno natjecanje");

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/kalendar");

        //Workaround zato sto ne znamo koji ce natjecanje biti id (ovisi o redoslijedu testova)
        Long id = 0L;
        for (Exam e: examRepository.findAll()) {
            if(e.getTitle().equals("Novo Natjecanje")){
                id = e.getExamId();
                break;
            }
        }
        Exam newExam = examRepository.getById(id);

        assertThat(newExam.getTitle()).isEqualTo("Novo Natjecanje");
        assertThat(newExam.getDescription()).isEqualTo("Tekst novog natjecanja");
        assertThat(newExam.getDateStart()).hasSameTimeAs(java.time.LocalDate.now() + "T10:00:00");
        assertThat(newExam.getDuration()).hasSameTimeAs("1970-01-01T01:00:00");
        assertThat(newExam.getMaxScore()).isEqualTo(13);
    }
}
