package hr.fer.proinz.projekt.dao;

import hr.fer.proinz.projekt.entity.TestCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
@Sql(statements = {
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (11, 'Creator', '12345678', 'creator@gmail.com', 'Creator', 'One', null, true, 1, 'ayvfHrrfgrEG')",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (11, 'Task 1', 'Task 1 Description', 10, 11, false, 5000)",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (12, 'Task 2', 'Task 2 Description', 20, 11, false, 10000)",
        "insert into testcase (testid, input, output, taskid) values (11, 'Input 1', 'Output 1', 11);",
        "insert into testcase (testid, input, output, taskid) values (12, 'Input 2', 'Output 2', 11);",
        "insert into testcase (testid, input, output, taskid) values (13, 'Input 3', 'Output 3', 12);"
})
public class TestCaseRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TestcaseRepository repo;

    @Autowired
    private TaskRepository taskRepo;

    @Test
    void ParticipatingRepositoryContext() {
        assertThat(repo.count()).isEqualTo(3);
        assertThat(repo.findAll()).hasSize(3);
    }

    @Test
    void findByTaskIdTest() {
        List<TestCase> testTask = repo.findByTaskid(11L);
        entityManager.flush();

        Optional<TestCase> tc1 = repo.findById(11L);
        Optional<TestCase> tc2 = repo.findById(12L);

        assertThat(testTask)
                .isNotNull()
                .hasSize(2)
                .contains(tc1.get())
                .contains(tc2.get());
    }

    @Test
    void findByTaskId_NoTaskTest() {
        List<TestCase> testTask = repo.findByTaskid(99L);
        entityManager.flush();

        assertThat(testTask)
                .isEqualTo(List.of());
    }

    @Test
    void deleteByTaskIdTest() {
        repo.deleteByTaskId(11L);
        entityManager.flush();

        Optional<TestCase> tc1 = repo.findById(11L);
        Optional<TestCase> tc2 = repo.findById(12L);

        assertThat(repo.count())
                .isEqualTo(1);
        assertThat(tc1)
                .isNotPresent();
        assertThat(tc2)
                .isNotPresent();
    }

    @Test
    void deleteByTaskId_NoTaskTest() {
        repo.deleteByTaskId(99L);
        entityManager.flush();

        Optional<TestCase> tc1 = repo.findById(11L);
        Optional<TestCase> tc2 = repo.findById(12L);
        Optional<TestCase> tc3 = repo.findById(13L);

        assertThat(repo.count())
                .isEqualTo(3);
        assertThat(repo.findAll())
                .contains(tc1.get())
                .contains(tc2.get())
                .contains(tc3.get());

    }


}
