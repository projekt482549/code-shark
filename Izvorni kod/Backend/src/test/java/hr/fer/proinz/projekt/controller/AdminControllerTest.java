package hr.fer.proinz.projekt.controller;

import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.service.AppUserService;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class AdminControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AppUserService appUserService;

    @Test
    @WithMockUser(roles = "ADMIN")
    void allRequests_noRequests() throws Exception {
        given(appUserService.listAllNotEnabledByAdmin()).willReturn(List.of());

        mvc.perform(get("/admin")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void allRequests_oneRequests() throws Exception {
        AppUser user = new AppUser("TestUser1", "12345678", "test@gmail.com", "Test", "Tester", null, false, 2, "1234");
        user.setUserId(1L);
        given(appUserService.listAllNotEnabledByAdmin()).willReturn(List.of(user));

        mvc.perform(get("/admin")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"userId\":1,\"username\":\"TestUser1\",\"password\":\"12345678\",\"email\":\"test@gmail.com\",\"name\":\"Test\",\"surname\":\"Tester\",\"photo\":null,\"enabled\":false,\"accounttype\":2,\"verificationCode\":\"1234\"}]"));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void enableRequests() throws Exception {
        mvc.perform(get("/admin/enable/TestUser"))
                .andExpect(status().isOk());
    }

}
