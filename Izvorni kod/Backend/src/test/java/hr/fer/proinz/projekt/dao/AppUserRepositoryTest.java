package hr.fer.proinz.projekt.dao;

import hr.fer.proinz.projekt.entity.AppUser;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import javax.persistence.PersistenceException;
import java.util.List;

import static hr.fer.proinz.projekt.utils.UserAssert.assertThatAppUser;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@ActiveProfiles("test")
public class AppUserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AppUserRepository repo;

    @Nested
    @Sql(statements = {
            "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (11, 'User1', '12345678', 'user1@gmail.com', 'User', 'One', null, true, 1, 'ayvfHrrfgrEG')",
            "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (12, 'User2', '12345678', 'user2@gmail.com', 'User 2', 'Two', null, true, 1, 'st54shn6+st')",
            "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (13, 'User3', '12345678', 'user3@gmail.com', 'User 3', 'Three', null, true, 1, 'ag41rg+ahyf')",
            "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (14, 'User4', '12345678', 'user4@gmail.com', 'User 4', 'Four', null, true, 3, 'arga+g41gs')",
            "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (15, 'User5', '12345678', 'user5@gmail.com', 'User 5', 'Five', null, false, 3, 'hm4g656+ash')",
            "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (16, 'User6', '12345678', 'user6@gmail.com', 'User 6', 'Six', null, true, 2, 'sgjyygy6+4yy5f1h')"
    })
    class WithDataInMemory {

        @Test
        void AppUserRepositoryContext() {
            assertThat(repo.count()).isEqualTo(6);
            assertThat(repo.findAll()).hasSize(6);
        }

        @Test
        void findByUsernameTest() {
            AppUser found = repo.findByUsername("User1");

            entityManager.flush();

            assertThatAppUser(found)
                    .hasUsername("User1")
                    .hasName("User")
                    .hasSurname("One")
                    .hasPassword("12345678")
                    .hasEmail("user1@gmail.com")
                    .hasEnabled(true)
                    .hasAccounttype(1)
                    .hasVerificationCode("ayvfHrrfgrEG");
        }

        @Test
        void findByUsername_NoUserTest() {
            AppUser found = repo.findByUsername("User99");

            entityManager.flush();

            assertThat(found).isNull();
        }

        @Test
        void findByEmailTest() {
            AppUser found = repo.findByEmail("user2@gmail.com");

            entityManager.flush();

            assertThatAppUser(found)
                    .hasUsername("User2")
                    .hasName("User 2")
                    .hasSurname("Two")
                    .hasPassword("12345678")
                    .hasEmail("user2@gmail.com")
                    .hasEnabled(true)
                    .hasAccounttype(1)
                    .hasVerificationCode("st54shn6+st");
        }

        @Test
        void findByEmail_NoEmailTest() {
            AppUser found = repo.findByEmail("user99@gmail.com");

            entityManager.flush();

            assertThat(found).isNull();
        }

        @Test
        void listAllTest() {
            List<AppUser> found = repo.listAll();

            entityManager.flush();

            assertThat(found)
                    .isNotNull()
                    .hasSize(5);
        }

        @Test
        void findByVerificationCodeTest() {
            AppUser found = repo.findByVerificationCode("ag41rg+ahyf");

            entityManager.flush();

            assertThatAppUser(found)
                    .hasUsername("User3")
                    .hasName("User 3")
                    .hasSurname("Three")
                    .hasPassword("12345678")
                    .hasEmail("user3@gmail.com")
                    .hasEnabled(true)
                    .hasAccounttype(1)
                    .hasVerificationCode("ag41rg+ahyf");
        }

        @Test
        void findByVerificationCode_NoVerificationCodeTest() {
            AppUser found = repo.findByVerificationCode("aabbccddeeffgg");

            entityManager.flush();

            assertThat(found).isNull();
        }

        @Test
        void listAllNotEnabledByAdminTest() {
            List<AppUser> found = repo.listAllNotEnabledByAdmin();

            entityManager.flush();

            assertThat(found)
                    .isNotNull()
                    .hasSize(2);
        }

        @Test
        void enableAppUserAsAdminTest() {
            repo.enableAppUserAsAdmin("User4");
            entityManager.flush();
            AppUser found = repo.findByUsername("User4");

            assertThatAppUser(found)
                    .hasAccounttype(1);
        }

        @Test
        void enableAppUserAsAdmin_NoUserTest() {
            repo.enableAppUserAsAdmin("User99");
            entityManager.flush();
            AppUser found = repo.findByUsername("User99");

            assertThatAppUser(found).isNull();
        }

        @Test
        void saveUserTest() {
            AppUser found = repo.saveUser(new AppUser("User7", "12345678", "user7@gmail.com", "User 7", "Seven", null, false, 3, "xygjnssjsnbyy55"));
            //AppUser found = repo.saveUser(new AppUser("User1", "12345678", "user1@gmail.com", "User 8", "Eight", null, false, 3, "xygjnssjsnbyy55"));

            entityManager.flush();

            assertThatAppUser(found)
                    .hasUsername("User7")
                    .hasName("User 7")
                    .hasSurname("Seven")
                    .hasPassword("12345678")
                    .hasEmail("user7@gmail.com")
                    .hasEnabled(false)
                    .hasAccounttype(3)
                    .hasVerificationCode("xygjnssjsnbyy55");
        }

        @Test
        void saveUser_ExistingUserTest() {

            Exception exception = assertThrows(PersistenceException.class, () -> {
                AppUser found = repo.saveUser(new AppUser("User1", "12345678", "user1@gmail.com", "User 8", "Eight", null, false, 3, "xygjnssjsnbyy55"));
                entityManager.flush();
            });

            assertTrue(exception.getCause() instanceof ConstraintViolationException);
        }

    }

    @Nested
    class NoDataInMemory {

        @Test
        void listAllTest() {
            List<AppUser> found = repo.listAll();

            assertThat(found)
                    .isNotNull()
                    .hasSize(0);
        }

        @Test
        void listAllNotEnabledByAdminTest() {
            List<AppUser> found = repo.listAllNotEnabledByAdmin();

            assertThat(found)
                    .isNotNull()
                    .hasSize(0);
        }

        @Test
        void saveUser_NoUsersTest() {
            AppUser found = repo.saveUser(new AppUser("User1", "12345678", "user1@gmail.com", "User", "One", null, true, 2, "ayvfHrrfgrEG"));

            List<AppUser> userList = repo.listAll();

            assertThatAppUser(found)
                    .hasUsername("User1")
                    .hasName("User")
                    .hasSurname("One")
                    .hasPassword("12345678")
                    .hasEmail("user1@gmail.com")
                    .hasEnabled(true)
                    .hasAccounttype(2)
                    .hasVerificationCode("ayvfHrrfgrEG");
            assertThat(userList)
                    .isNotNull()
                    .hasSize(1);
        }

    }


    }
