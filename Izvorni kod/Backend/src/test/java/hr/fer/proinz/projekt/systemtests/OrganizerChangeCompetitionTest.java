package hr.fer.proinz.projekt.systemtests;

import hr.fer.proinz.projekt.dao.ExamRepository;
import hr.fer.proinz.projekt.entity.Exam;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class OrganizerChangeCompetitionTest {

    @Autowired
    private ExamRepository examRepository;

    WebDriver driver;

    @BeforeEach
    void setup() throws InterruptedException {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/login");

        //Login
        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("voditelj");
        element = driver.findElement(By.name("password"));
        element.sendKeys("voditelj");
        driver.findElement(By.cssSelector("button[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        //Provjera Logina
        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("voditelj");
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testChangeCompetition() throws InterruptedException {
        driver.findElement(By.cssSelector("a[href='/kalendar']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/kalendar");

        Thread.sleep(1500);

        if(java.time.LocalDate.now().plusDays(1).getMonth() != java.time.LocalDate.now().getMonth()) {
            driver.findElement(By.id("right_arrow")).click();
            Thread.sleep(1000);
        }

        driver.findElement(By.cssSelector("a[href='/natjecanje/103']")).click();

        Thread.sleep(2000);

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/natjecanje/103");

        WebElement competitionTitle = driver.findElement(By.className("competitionTitle"));
        WebElement competitionInfo = driver.findElement(By.className("competitionInfo"));

        assertThat(competitionTitle.getText()).isEqualTo("Exam3");
        assertThat(competitionInfo.getText())
                .contains("Opis3")
                .contains("Broj zadataka: 2")
                .contains("Maksimalno bodova: 2")
                .contains("00:30:00");

        driver.findElement(By.cssSelector("a[href='/uredivanjeNatjecanja/103']")).click();

        Thread.sleep(2000);

        WebElement element = driver.findElement(By.name("name"));
        element.clear();
        element.sendKeys("Novo Novo Natjecanje");

        element = driver.findElement(By.name("startTime"));
        element.clear();
        element.sendKeys("8:00");

        element = driver.findElement(By.name("endTime"));
        element.clear();
        element.sendKeys("16:00");

        element = driver.findElement(By.cssSelector("input[value='103']"));
        element.click();

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(2000);

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).isEqualTo("http://localhost:3000/natjecanje/103");

        Exam newExam = examRepository.getById(103L);

        assertThat(newExam.getTitle()).isEqualTo("Novo Novo Natjecanje");
        assertThat(newExam.getDescription()).isEqualTo("Opis3");
        assertThat(newExam.getDateStart()).hasSameTimeAs(java.time.LocalDate.now().plusDays(1) + "T08:00:00");
        assertThat(newExam.getDuration()).hasSameTimeAs("1970-01-01T08:00:00");
        assertThat(newExam.getMaxScore()).isEqualTo(13);
        assertThat(newExam.getTaskAssoc()).hasSize(2);
    }
}
