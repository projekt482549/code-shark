package hr.fer.proinz.projekt.systemtests;

import hr.fer.proinz.projekt.dao.TaskRepository;
import hr.fer.proinz.projekt.entity.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql", "file:src/test/java/hr/fer/proinz/projekt/testSqlData.sql"})
@ActiveProfiles("test")
public class OrganizerCreateTaskTest {

    @Autowired
    private TaskRepository taskRepository;

    WebDriver driver;

    @BeforeEach
    void setup() throws InterruptedException {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.get("http://localhost:3000/login");

        //Login
        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("voditelj");
        element = driver.findElement(By.name("password"));
        element.sendKeys("voditelj");
        driver.findElement(By.cssSelector("button[type='submit']")).click();

        Thread.sleep(1500);

        String redirURL = driver.getCurrentUrl();
        WebElement nav_user = driver.findElement(By.className("nav_user_text"));
        String nav_user_text = nav_user.getText();

        //Provjera Logina
        assertThat(redirURL).isEqualTo("http://localhost:3000/");
        assertThat(nav_user_text).isEqualTo("voditelj");
    }

    @AfterEach
    void teardown(){
        driver.quit();
    }

    @Test
    public void testCreateTask() throws InterruptedException {
        Actions action = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver,2);

        //Provjera da li smo na pravoj stranici
        action.moveToElement(driver.findElement(By.cssSelector("a[href*='/profil/']"))).perform();

        Thread.sleep(1000);

        driver.findElement(By.cssSelector("a[href*='/noviZadatak/']")).click();
        String redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/noviZadatak/");

        Thread.sleep(1000);

        WebElement element = driver.findElement(By.name("nameTask"));
        element.sendKeys("Novi zadatak");

        element = driver.findElement(By.name("textTask"));
        element.sendKeys("Tekst novog zadatka");

        element = driver.findElement(By.name("points"));
        element.sendKeys("1");

        element = driver.findElement(By.name("threadshold"));
        element.sendKeys("1000");

        element = driver.findElement(By.name("in"));
        element.sendKeys("1");

        element = driver.findElement(By.name("out"));
        element.sendKeys("2");

        //driver.findElement(By.name("private")).click();

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        Thread.sleep(1000);

        String alert = new String();
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            alert = driver.switchTo().alert().getText();
            driver.switchTo().alert().dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Thread.sleep(1000);

        assertThat(alert).isEqualTo("Uspješno napravljen zadatak");

        redirURL = driver.getCurrentUrl();
        assertThat(redirURL).contains("http://localhost:3000/noviZadatak/");

        //Workaround zato sto ne znamo koji ce zadatak biti id (ovisi o redoslijedu testova)
        Long id = 0L;
        for (Task t: taskRepository.findAll()) {
            if(t.getTitle().equals("Novi zadatak")){
                id = t.getTaskId();
                break;
            }
        }
        Task newTask = taskRepository.getById(id);

        assertThat(newTask.getTitle()).isEqualTo("Novi zadatak");
        assertThat(newTask.getDescription()).isEqualTo("Tekst novog zadatka");
        assertThat(newTask.getMscore()).isEqualTo(1);
        assertThat(newTask.getTimeThreshold()).isEqualTo(1000);
    }
}
