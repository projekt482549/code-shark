package hr.fer.proinz.projekt.service;

import hr.fer.proinz.projekt.controller.AppUserDto;
import hr.fer.proinz.projekt.controller.UserDataDto;
import hr.fer.proinz.projekt.dao.AppUserRepository;
import hr.fer.proinz.projekt.dao.TaskRepository;
import hr.fer.proinz.projekt.entity.AppUser;
import hr.fer.proinz.projekt.entity.Task;
import hr.fer.proinz.projekt.utils.MessageString;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import static hr.fer.proinz.projekt.utils.UserAssert.assertThatAppUser;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Sql(scripts = {"file:src/test/java/hr/fer/proinz/projekt/testSqlTables.sql"},
     statements = {
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (11, 'Creator', '12345678', 'creator@gmail.com', 'Creator', 'One', null, true, 1, 'ayvfHrrfgrEG')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (12, 'Competitor1', '12345678', 'competitor1@gmail.com', 'Competitor', 'One', null, true, 0, 'yfbafafVdDyFb')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (13, 'Competitor2', '12345678', 'competitor2@gmail.com', 'Competitor', 'Two', null, true, 0, 'jxghsygsgnm652')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (14, 'Competitor3', '12345678', 'competitor3@gmail.com', 'Competitor', 'Three', null, false, 0, 'sh5s16sgn15sb')",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (15, 'Admin1', '12345678', 'admin@gmail.com', 'Admin', 'One', null, true, 2, null)",
        "insert into users (userid, username, password, email, name, surname, photo, enabled, accounttype, verification_code) values (16, 'UC', '12345678', 'creatorUC@gmail.com', 'Creator', 'NotCom', null, true, 3, 'sfdbjkapnbpaga')",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (11, 'Exam 1', 'Test exam', null, 10, current_timestamp - INTERVAL '1' HOUR, '02:00:00', 11, false, null)",
        "insert into exam (examid, title, description, trophy, maxscore, datestart, duration, userid, virtualflag, virtualexamid) values (12, 'Exam 2', 'Test exam 2', null, 10, current_timestamp - INTERVAL '1' DAY, '00:30:00', 11, false, null)",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (11, 'Task 1', 'Task 1 Description', 10, 11, false, 5000)",
        "insert into task (taskid, title, description, mscore, userid, privatetask, timethreshold) values (12, 'Task 2', 'Task 2 Description', 20, 11, false, 10000)",
        "insert into score (scoreid, answer, score, taskid, examid, userid, averagetime) values (11, 'A1', 10, 11, 11, 12, 10000)",
        "insert into score (scoreid, answer, score, taskid, examid, userid, averagetime) values (12, 'A2', 15, 12, 11, 12, 15000)",
        "insert into score (scoreid, answer, score, taskid, examid, userid, averagetime) values (13, 'A3', 15, 12, 12, 12, 12000)",
        "insert into score (scoreid, answer, score, taskid, examid, userid, averagetime) values (14, 'A4', 12, 12, 12, 13, 13200)"
})
public class AppUserServiceTest {

    @Autowired
    private AppUserRepository repo;

    @Autowired
    private TaskRepository taskRepo;

    @Autowired
    private AppUserService service;

    @Test
    @Transactional
    void findByUsernameTest() {
        AppUser testUser = service.findByUsername("Creator");

        assertThatAppUser(testUser)
                .hasUsername("Creator")
                .hasPassword("12345678")
                .hasEmail("creator@gmail.com")
                .hasName("Creator")
                .hasSurname("One")
                .hasAccounttype(1)
                .hasEnabled(true);
    }

    @Test
    @Transactional
    void findByUsername_NoUserTest() {
        AppUser testUser = service.findByUsername("Test");

        assertThat(testUser)
                .isNull();
    }

    @Test
    @Transactional
    void findUserDataTest() {
        UserDataDto testData = service.findUserData("Creator");

        Optional<Task> t1 = taskRepo.findById(11L);
        Optional<Task> t2 = taskRepo.findById(12L);

        assertThatAppUser(testData.getAppUser())
                .hasUsername("Creator")
                .hasPassword("12345678")
                .hasEmail("creator@gmail.com")
                .hasName("Creator")
                .hasSurname("One")
                .hasAccounttype(1)
                .hasEnabled(true);
        assertThat(testData.getCorrectlySolvedTasks()).isEqualTo(0);
        assertThat(testData.getTriedTasks()).isEqualTo(0);
        assertThat(testData.getTrophyLinkList()).isNull();
        assertThat(testData.getTaskList())
                .contains(t1.get())
                .contains(t2.get());
    }

    /*@Test
    @Transactional
    void findUserData_NoUserTest() {
        UserDataDto testData = service.findUserData("Test");

        assertThat(testData).isNull();
    }*/

    @Test
    @Transactional
    void listAllTest() {
        List<AppUser> testUsers = service.listAll();

        Optional<AppUser> sc1 = repo.findById(11L);
        Optional<AppUser> sc2 = repo.findById(12L);
        Optional<AppUser> sc3 = repo.findById(13L);
        Optional<AppUser> sc4 = repo.findById(14L);

        assertThat(testUsers)
                .isNotNull()
                .hasSize(4)
                .contains(sc1.get())
                .contains(sc2.get())
                .contains(sc3.get())
                .contains(sc4.get());
    }

    @Test
    @Transactional
    void findByEmailTest() {
        AppUser testUser = service.findByEmail("creator@gmail.com");

        assertThatAppUser(testUser)
                .hasUsername("Creator")
                .hasPassword("12345678")
                .hasEmail("creator@gmail.com")
                .hasName("Creator")
                .hasSurname("One")
                .hasAccounttype(1)
                .hasEnabled(true);
    }

    @Test
    @Transactional
    void findByEmail_NoUserTest() {
        AppUser testUser = service.findByEmail("test@gmail.com");

        assertThat(testUser)
                .isNull();
    }

    //Potrebno onemoguciti antivirusni program
    @Test
    @Transactional
    void createUserTest() throws MessagingException, UnsupportedEncodingException {
        AppUserDto newUser = new AppUserDto();
        newUser.setUsername("NewUser");
        newUser.setName("New");
        newUser.setSurname("User");
        newUser.setAccounttype(1);
        newUser.setPassword("password");
        newUser.setEmail("newUser@gmail.com");
        String siteUrl = new String();
        AppUser testUser = service.createUser(newUser, siteUrl);

        assertThatAppUser(testUser)
                .hasName("New")
                .hasSurname("User")
                .hasUsername("NewUser")
                .hasEnabled(false)
                .hasAccounttype(3)
                .hasEmail("newUser@gmail.com");

        testUser = repo.findByUsername("NewUser");

        assertThatAppUser(testUser)
                .hasName("New")
                .hasSurname("User")
                .hasUsername("NewUser")
                .hasEnabled(false)
                .hasAccounttype(3)
                .hasEmail("newUser@gmail.com");
    }

    @Test
    @Transactional
    void verifyTest() {
        boolean testVerify = service.verify("sh5s16sgn15sb");

        assertThat(testVerify)
                .isTrue();

        AppUser testUser = repo.findByUsername("Competitor3");
        assertThatAppUser(testUser)
                .hasEnabled(true);
    }

    @Test
    @Transactional
    void verify_NoVerifyTest() {
        boolean testVerify = service.verify("dsdsbababaff");

        assertThat(testVerify)
                .isFalse();
    }

    @Test
    @Transactional
    void updateAppUserTest() {
        AppUserDto newUser = new AppUserDto();
        newUser.setUsername("NewUser");
        newUser.setName("New");
        newUser.setSurname("User");
        newUser.setAccounttype(1);
        newUser.setPassword("password");
        String oldUsername = "Competitor2";
        newUser.setEmail("newUser@gmail.com");
        MessageString test = service.updateAppUser(newUser, oldUsername);

        assertThat(test.getMessage())
                .isEqualTo("ok");

        AppUser testUser = repo.findByUsername("NewUser");
        assertThatAppUser(testUser)
                .hasName("New")
                .hasSurname("User")
                .hasUsername("NewUser")
                .hasEnabled(true)
                .hasAccounttype(1)
                .hasEmail("newUser@gmail.com");
    }

}
