--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5 (Ubuntu 13.5-2.pgdg20.04+1)
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: exam; Type: TABLE; Schema: public; Owner: hnplarhdqwdcdm
--

CREATE TABLE "public"."exam" (
    "examid" integer NOT NULL,
    "title" character varying(100) NOT NULL,
    "description" character varying(5000) NOT NULL,
    "trophy" "bytea",
    "maxscore" integer NOT NULL,
    "datestart" timestamp without time zone NOT NULL,
    "duration" time without time zone NOT NULL,
    "userid" integer NOT NULL,
    "virtualflag" boolean,
    "virtualexamid" integer
);


ALTER TABLE public.exam OWNER TO hnplarhdqwdcdm;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: hnplarhdqwdcdm
--

CREATE SEQUENCE "public"."hibernate_sequence"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO hnplarhdqwdcdm;

--
-- Name: inexam; Type: TABLE; Schema: public; Owner: hnplarhdqwdcdm
--

CREATE TABLE "public"."inexam" (
    "examid" integer NOT NULL,
    "taskid" integer NOT NULL,
    "taskno" integer NOT NULL
);


ALTER TABLE public.inexam OWNER TO hnplarhdqwdcdm;

--
-- Name: participating; Type: TABLE; Schema: public; Owner: hnplarhdqwdcdm
--

CREATE TABLE "public"."participating" (
    "userid" integer NOT NULL,
    "examid" integer NOT NULL
);


ALTER TABLE public.participating OWNER TO hnplarhdqwdcdm;

--
-- Name: score; Type: TABLE; Schema: public; Owner: hnplarhdqwdcdm
--

CREATE TABLE "public"."score" (
    "scoreid" integer NOT NULL,
    "answer" "bytea" NOT NULL,
    "score" integer NOT NULL,
    "taskid" integer NOT NULL,
    "examid" integer NOT NULL,
    "userid" integer NOT NULL,
    "averagetime" integer NOT NULL
);


ALTER TABLE public.score OWNER TO hnplarhdqwdcdm;

--
-- Name: task; Type: TABLE; Schema: public; Owner: hnplarhdqwdcdm
--

CREATE TABLE "public"."task" (
    "taskid" integer NOT NULL,
    "title" character varying(100) NOT NULL,
    "description" character varying(5000) NOT NULL,
    "mscore" integer NOT NULL,
    "userid" integer NOT NULL,
    "privatetask" boolean NOT NULL,
    "timethreshold" integer NOT NULL
);


ALTER TABLE public.task OWNER TO hnplarhdqwdcdm;

--
-- Name: testcase; Type: TABLE; Schema: public; Owner: hnplarhdqwdcdm
--

CREATE TABLE "public"."testcase" (
    "testid" integer NOT NULL,
    "input" character varying(5000) NOT NULL,
    "output" character varying(5000) NOT NULL,
    "taskid" integer NOT NULL
);


ALTER TABLE public.testcase OWNER TO hnplarhdqwdcdm;

--
-- Name: users; Type: TABLE; Schema: public; Owner: hnplarhdqwdcdm
--

CREATE TABLE "public"."users" (
    "userid" integer NOT NULL,
    "username" character varying(20) NOT NULL,
    "password" character varying(100) NOT NULL,
    "email" character varying(320) NOT NULL,
    "name" character varying(50) NOT NULL,
    "surname" character varying(50) NOT NULL,
    "photo" "bytea",
    "enabled" boolean NOT NULL,
    "accounttype" integer NOT NULL,
    "verification_code" character varying(255)
);


ALTER TABLE public.users OWNER TO hnplarhdqwdcdm;

--
-- Data for Name: exam; Type: TABLE DATA; Schema: public; Owner: hnplarhdqwdcdm
--

COPY "public"."exam" ("examid", "title", "description", "trophy", "maxscore", "datestart", "duration", "userid", "virtualflag", "virtualexamid") FROM stdin;
\.


--
-- Data for Name: inexam; Type: TABLE DATA; Schema: public; Owner: hnplarhdqwdcdm
--

COPY "public"."inexam" ("examid", "taskid", "taskno") FROM stdin;
\.


--
-- Data for Name: participating; Type: TABLE DATA; Schema: public; Owner: hnplarhdqwdcdm
--

COPY "public"."participating" ("userid", "examid") FROM stdin;
\.


--
-- Data for Name: score; Type: TABLE DATA; Schema: public; Owner: hnplarhdqwdcdm
--

COPY "public"."score" ("scoreid", "answer", "score", "taskid", "examid", "userid", "averagetime") FROM stdin;
\.


--
-- Data for Name: task; Type: TABLE DATA; Schema: public; Owner: hnplarhdqwdcdm
--

COPY "public"."task" ("taskid", "title", "description", "mscore", "userid", "privatetask", "timethreshold") FROM stdin;
367	Testni zadatak 1	Čitaj stdin i ispisi 'Bok\\n' za svaku pročitanu liniju.	1	8	f	5000
369	Testni zadatak 2	Za svaku liniju procitanu sa stdin, ispisi "Kob\\n".	2	8	f	5000
\.


--
-- Data for Name: testcase; Type: TABLE DATA; Schema: public; Owner: hnplarhdqwdcdm
--

COPY "public"."testcase" ("testid", "input", "output", "taskid") FROM stdin;
368	asdfasdf\nbadfsdaf\nsdafsdaf	Bok\nBok\nBok\n	367
370	dsčklafj lkasdjf askldfj \naslčdkgsad gsd jsadg asdkl	Kob\nKob\n	369
371	adsfasdf\nasdfa	Kob\nKob\n	369
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: hnplarhdqwdcdm
--

COPY "public"."users" ("userid", "username", "password", "email", "name", "surname", "photo", "enabled", "accounttype", "verification_code") FROM stdin;
1	adminlovro	$2a$12$EOFNxDGH0auz5dnActoKtetMpWUSUxX1Kdm7uDaO1oxbGgPjt6zMK	lovro.glogar@fer.hr	Lovro	Glogar	\N	t	2	\N
2	adminandrejl	$2a$12$PqVAeE.o0D6dV2p93E5ND.MlBmVFwWRTI/z6BKYxP0zj4f5U97Kva	andrej.lukic@fer.hr	Andrej	Lukic	\N	t	2	\N
3	admindomagoj	$2a$12$6M9BG3/VLrDWes9JnVRuQuZC1XWaZMu8rzIpFpWMnt4hg7ue4R.4O	domagoj.mutic@fer.hr	Domagoj	Mutic	\N	t	2	\N
4	adminandrejs	$2a$12$o1p1gIwrnWgaH1zsNf1RlOAWbQmDippMoYtWXsUjrxd/FCAr77mtm	andrej.simic@fer.hr	Andrej	Simic	\N	t	2	\N
5	admintin	$2a$12$2bdv11Sq9t84yYVtpAdX1eO5LOXUgIfSx1GLBbAYXsr8dZ1ludMg6	tin.manojlovic@fer.hr	Tin	Manojlovic	\N	t	2	\N
6	adminivan	$2a$12$cTbcUBpGjhUmJFZn952ltOz2SVepJHeU.AhOd0vVe5Cvn8xxe/dnC	ivan.filipovic@fer.hr	Ivan	Filipovic	\N	t	2	\N
7	adminantonia	$2a$12$acar0JjisiDKRboDtncO2.CU0ELmyK0ybZCbKrS27vS/saGBpZHE.	antonia.blazevic@fer.hr	Antonia	Blazevic	\N	t	2	\N
8	voditelj	$2a$12$8LPFyK/34TCI2Rtdv8sL2.q/glSHaUvm5UgTzOnFcl/uW2I52Sxx.	voditelj@gmail.com	ImeVoditelja	PrezimeVoditelja	\N	t	1	\N
9	natjecatelj1	$2a$12$KWz55s3udsEOw/DLBFWdyeBjK9OJdfnOkfMB6g.8fLuG90VlGxREq	natjecatelj1@gmail.com	ImeNatjecatelja1	PrezimeNatjecatelja1	\N	t	0	\N
10	natjecatelj2	$2a$12$KxXcRbFdKnNauCsRv.Ng..SGm9ybUufVDd51BCLgqhf85rWIbHHCS	natjecatelj2@gmail.com	ImeNatjecatelja2	PrezimeNatjecatelja2	\N	t	0	\N
11	natjecatelj3	$2a$12$F3uE4PZkkNYiGBWcPMnHjuk.ziQ0b//eYTiz21qIHw5Cy..eSPVXy	natjecatelj3@gmail.com	ImeNatjecatelja3	PrezimeNatjecatelja3	\N	t	0	\N
12	natjecatelj4	$2a$10$sBiVgyxf0AARYDZmg90GN.kkpjQyVSb7Lx0.T48ujcMzZSv8jv.ku	natjecatelj4@gmail.com	ImeNatjecatelja4	PrezimeNatjecatelja4	\N	t	0	\N
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: hnplarhdqwdcdm
--

SELECT pg_catalog.setval('"public"."hibernate_sequence"', 397, true);


--
-- Name: users app_user_uk; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."users"
    ADD CONSTRAINT "app_user_uk" UNIQUE ("username", "email");


--
-- Name: exam exam_pkey; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."exam"
    ADD CONSTRAINT "exam_pkey" PRIMARY KEY ("examid");


--
-- Name: inexam inexam_pkey; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."inexam"
    ADD CONSTRAINT "inexam_pkey" PRIMARY KEY ("examid", "taskid");


--
-- Name: participating participating_pkey; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."participating"
    ADD CONSTRAINT "participating_pkey" PRIMARY KEY ("userid", "examid");


--
-- Name: score score_pkey; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."score"
    ADD CONSTRAINT "score_pkey" PRIMARY KEY ("scoreid");


--
-- Name: task task_pkey; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."task"
    ADD CONSTRAINT "task_pkey" PRIMARY KEY ("taskid");


--
-- Name: testcase testcase_pkey; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."testcase"
    ADD CONSTRAINT "testcase_pkey" PRIMARY KEY ("testid");


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."users"
    ADD CONSTRAINT "users_email_key" UNIQUE ("email");


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."users"
    ADD CONSTRAINT "users_pkey" PRIMARY KEY ("userid");


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."users"
    ADD CONSTRAINT "users_username_key" UNIQUE ("username");


--
-- Name: exam exam_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."exam"
    ADD CONSTRAINT "exam_userid_fkey" FOREIGN KEY ("userid") REFERENCES "public"."users"("userid") ON DELETE CASCADE;


--
-- Name: exam exam_virtualexamid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."exam"
    ADD CONSTRAINT "exam_virtualexamid_fkey" FOREIGN KEY ("virtualexamid") REFERENCES "public"."exam"("examid") ON DELETE CASCADE;


--
-- Name: inexam inexam_examid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."inexam"
    ADD CONSTRAINT "inexam_examid_fkey" FOREIGN KEY ("examid") REFERENCES "public"."exam"("examid") ON DELETE CASCADE;


--
-- Name: inexam inexam_taskid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."inexam"
    ADD CONSTRAINT "inexam_taskid_fkey" FOREIGN KEY ("taskid") REFERENCES "public"."task"("taskid") ON DELETE CASCADE;


--
-- Name: participating participating_examid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."participating"
    ADD CONSTRAINT "participating_examid_fkey" FOREIGN KEY ("examid") REFERENCES "public"."exam"("examid") ON DELETE CASCADE;


--
-- Name: participating participating_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."participating"
    ADD CONSTRAINT "participating_userid_fkey" FOREIGN KEY ("userid") REFERENCES "public"."users"("userid") ON DELETE CASCADE;


--
-- Name: score score_examid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."score"
    ADD CONSTRAINT "score_examid_fkey" FOREIGN KEY ("examid") REFERENCES "public"."exam"("examid") ON DELETE CASCADE;


--
-- Name: score score_taskid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."score"
    ADD CONSTRAINT "score_taskid_fkey" FOREIGN KEY ("taskid") REFERENCES "public"."task"("taskid") ON DELETE CASCADE;


--
-- Name: score score_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."score"
    ADD CONSTRAINT "score_userid_fkey" FOREIGN KEY ("userid") REFERENCES "public"."users"("userid") ON DELETE CASCADE;


--
-- Name: task task_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."task"
    ADD CONSTRAINT "task_userid_fkey" FOREIGN KEY ("userid") REFERENCES "public"."users"("userid") ON DELETE CASCADE;


--
-- Name: testcase testcase_taskid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnplarhdqwdcdm
--

ALTER TABLE ONLY "public"."testcase"
    ADD CONSTRAINT "testcase_taskid_fkey" FOREIGN KEY ("taskid") REFERENCES "public"."task"("taskid") ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

